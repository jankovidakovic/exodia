import React from 'react';
import Axios from 'axios';
import { Link, useParams } from 'react-router-dom';
import { useAuthState } from '../Auth/AuthContext';
import { url } from '../../constants';
import { Card, Col, Button, Row, Container } from 'react-bootstrap';
import HikeMinimal from '../Hike/HikeMinimal';

function DetailedEvent() {
	const [detailedEvent, setDetailedEvent] = React.useState({
		firstName: '',
		lastName: '',
		hike: '',
		startDateTime: Date(null),
		endDateTime: Date(null),
		description: '',
		hikeId: '',
		sent: false
	});
	const [hike, setHike] = React.useState();
	const { id } = useParams();
	const auth = useAuthState();

	const [message, setMessage] = React.useState('');
	const [error, setError] = React.useState('');

	const [eventInvites, setEventInvites] = React.useState([]);

	const [creator, setCreator] = React.useState(false);
	//const [showFriends, setShowFriends] = React.useState(false);

	//let friends = [];
	const [friends, setFriends] = React.useState([]);

	function sendInvites() {
		Axios.get(`${url}/users/${auth.user.id}/friends`).then(
			(response) => {
				//setMessage('Pozivnice uspješno poslane');
				console.log(response.data);
				setFriends(response.data);
				//setShowFriends(true);

				//console.log(friends[0]);
				if (friends.length === 0) {
					setMessage(
						'Ako želite poslati još pozivnica, dodajte prijatelje'
					);
				}
			},
			(error) => {
				setError('Nismo dohvatili primatelje');
			}
		);
		/*
				Axios.post(`${url}/events/${id}`).then(
					(response) => {
						setMessage('Pozivnice uspješno poslane');
					},
					(error) => {
						setError('Pozivnice nisu poslane');
					}
				);
				*/
	}

	function inviteFriend(invUser) {
		const inviteDTO = {
			user: invUser,
			status: null
		};
		console.log('inviting ', invUser);
		Axios.post(`${url}/events/${id}/invites`, inviteDTO).then(
			(response) => {
				//setMessage('Pozivnice uspješno poslane');
				console.log(response);
				setMessage(
					'Poslana pozivnica korisniku ',
					invUser.firstName,
					invUser.lastName
				);
			},
			(error) => {
				setError('Nismo dohvatili primatelje');
			}
		);
	}

	function getInvites() {
		Axios.get(`${url}/events/${id}/invites`).then((response) => {
			setEventInvites(response.data);
		});
	}

	function formatDateTime(dateTime1) {
		if (!dateTime1) return;
		let dateTime = new Date(dateTime1);
		let date = dateTime.getDate() + "." + (dateTime.getMonth() + 1) + "." + dateTime.getFullYear() + ". ";
		let time = (dateTime.getHours() < 10 ? "0" + dateTime.getHours() : dateTime.getHours()) + ":"
			+ (dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes());
		return date + time;
	}


	React.useEffect(() => {
		Axios.get(`${url}/events/${id}`).then(
			(response) => {
				setDetailedEvent((old) => ({
					...old,
					hikeId: response.data.hike.id,
					firstName: response.data.creator.firstName,
					lastName: response.data.creator.lastName,
					hike: response.data.hike.name,
					startDateTime: response.data.startDateTime,
					endDateTime: response.data.endDateTime,
					description: response.data.description
				}));
				setHike(response.data.hike);
				console.log(response.data.hike);
				if (auth.user.id === response.data.creator.id) {
					setCreator(true);
				}
			},
			(error) => {
				setError('Događaj se ne može dohvatiti');
			}
		);
	}, []);

	return (
		<Container>
			<Row>
				<Col>
					Događaj je napravio <label className="important">{detailedEvent.firstName}{' '}
						{detailedEvent.lastName}</label>
				</Col>
			</Row>
			<div>
				<label>
					Održava se na stazi{' '}
					<HikeMinimal hike={hike} />
				</label>
			</div>
			<div>
				<label>
					Početak događaja je {formatDateTime(detailedEvent.startDateTime)}
					, a završit će {formatDateTime(detailedEvent.endDateTime)}
				</label>
			</div>
			<div>
				<label>Opis:</label>
			</div>
			<div>
				<text>{detailedEvent.description}</text>
			</div>
			<br></br>
			{friends.length > 0 ? (
				<Card>
					Prijatelji
					{friends.map((friend) => (
						<Row key={friend.id}>
							<Col lg="3">
								Korisnik: {friend.firstName} {friend.lastName}
							</Col>
							<Col>
								<Button
									onClick={() => {
										inviteFriend(friend);
									}}
								>
									Pošalji
							</Button>{' '}
							</Col>
						</Row>
					))}
				</Card>
			) : (
					<div></div>
				)}
			<br></br>
			{creator && (
				<Row>
					<Col lg="6" className="d-flex justify-content-end">
						<Button
							onClick={sendInvites}
							disabled={detailedEvent.sent && creator}
						>
							Pozovi prijatelje
						</Button>
					</Col>

					<Col lg="6" className="d-flex justify-content-start">
						<Button onClick={getInvites} disabled={!creator}>
							Prikaži pozivnice
						</Button>
					</Col>
				</Row>
			)}
			<br></br>
			<div>
				{eventInvites.map((invite, idx) => (
					<Row key={idx}>
						<Col className="d-flex justify-content-end">
							{invite.user.firstName} {invite.user.lastName}
						</Col>
						<Col>{invite.status}</Col>
					</Row>
				))}
			</div>
			{message}
			{error}
		</Container>
	);
}

export default DetailedEvent;
