import Axios from 'axios';
import React from 'react';
import { Link } from 'react-router-dom';
import { useAuthState } from '../Auth/AuthContext';
import { url } from '../../constants';
import { Card, Col, Button, Row, Container } from 'react-bootstrap';

const UserEvents = ({ userId }) => {
	const [userEventsForm, setUserEventsForm] = React.useState({
		events: [],
		loading: true
	});

	const [message, setMessage] = React.useState('');
	const [error, setError] = React.useState('');

	const auth = useAuthState();

	function formatDateTime(dateTime) {
		if (!dateTime) return;
		let date = dateTime.getDate() + "." + (dateTime.getMonth() + 1) + "." + dateTime.getFullYear() + ". ";
		let time = (dateTime.getHours() < 10 ? "0" + dateTime.getHours() : dateTime.getHours()) + ":"
			+ (dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes());
		return date + time;
	}

	function renderEvent(ev) {

		return (
			<div key={ev.id} className="eventContainer fluid">
				<br></br>

				<div>Događaj na izlet: {ev.hike.name}</div>
				<div>Lokacija: {ev.hike.location.name}</div>
				<div>Duljina staze: {ev.hike.length}</div>
				<div>Početak: {formatDateTime(new Date(ev.startDateTime))}</div>
				<div className="centered">
					<Button>
						<Link to={'/event/' + ev.id} className="link">
							Detaljnije o događaju
						</Link>
					</Button>
				</div>
			</div>
		);
	}

	React.useEffect(() => {
		Axios.get(`${url}/events?creatorId=${userId}`)
			.then((res) => {
				console.log('dogadaji ucitani', res);
				setUserEventsForm({
					events: res.data,
					loading: false
				});
				setMessage('Uspješno dohvaćeno');
			})
			.catch((error) => {
				setError('Neuspješno dohvaćanje događaja korisnika.');
			});
	}, []);
	if (userEventsForm.loading) {
		return <div>Učitavam događaje...</div>;
	} else if (userEventsForm.events.length === 0) {
		return <div>Korisnik nema svojih događaja.</div>;
	} else {
		return (
			<Container>
				<label>Događaji korisnika:</label>
				<Row>
					{userEventsForm.events.map((event, idx) => (
						<Col className="padding:5%" lg="4" key={idx}>
							{renderEvent(event)}
						</Col>
					))}
				</Row>
			</Container>
		);
	}
};
export default UserEvents;
