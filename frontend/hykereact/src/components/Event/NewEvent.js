import React from 'react';
import Axios from 'axios';
import { useAuthState } from '../Auth/AuthContext';
import { url } from '../../constants';
import { useLocation } from 'react-router-dom';
import { Row, Col, ToggleButton, Container, Button, Card } from "react-bootstrap";
import "./NewEvent.css"
import HikeMinimal from '../Hike/HikeMinimal';

function NewEvent() {
	const { state } = useLocation();
	console.log(state);

	const auth = useAuthState();

	const [eventForm, setEventForm] = React.useState({
		creator: auth.user.id,
		description: null,
		hike: state ? state.id : null,
		startDate: null,
		endDate: null,
		loading: true
	});
	const [hikes, setHikes] = React.useState([]);
	const [message, setMessage] = React.useState('');
	const [error, setError] = React.useState('');
	const [userHikes, setUserHikes] = React.useState();

	const [currentHike, setCurrentHike] = React.useState(null);

	React.useEffect(() => {
		Axios.get(`${url}/hikes?publicity=1`).then(
			(response) => {
				setHikes(response.data);
				if (state) {
					console.log(state.id);
					const filtered = response.data.filter((resphike) => {
						if (resphike.id == state.id)
							return true;
					});
					if (filtered.length > 0) {
						setCurrentHike(filtered[0]);
					}
				}

			},
			(error) => {
				setError('Izleti se ne mogu prikazati');
			}
		);
		Axios.get(`${url}/hikes?creatorId=${auth.user.id}`).then((response) => {
			setUserHikes(response.data);
		});
	}, []);
	function onSubmit(e) {
		e.preventDefault();
		setError('');
		if (!currentHike) {
			setError("Nije odabran izlet");
			return;
		}
		if (!eventForm.startDate || !eventForm.endDate) {
			setError("Odaberite vrijeme");
			return;
		}

		const eventCreds = {
			creator: { id: eventForm.creator },
			description: eventForm.description && eventForm.description != "" ? eventForm.description : "Opis događaja...",
			hike: { id: currentHike.id },
			startDateTime: eventForm.startDate,
			endDateTime: eventForm.endDate
		};
		console.log('Pokusaj stvaranja');
		console.log(eventCreds);

		if (
			eventForm.creator === '' ||
			eventForm.description === '' ||
			eventForm.hike === '' ||
			eventForm.startDate === '' ||
			eventForm.endDate === ''
		) {
			setError('Svi podatci moraju biti ispunjeni ili odabrani!');
			return;
		}
		setMessage("");
		Axios.post(`${url}/events`, eventCreds).then(
			(response) => {
				setMessage('Uspješno stvoren događaj!');
				console.log(response);
				setError("");
			},
			(error) => {
				setError('Stvaranje događaja nije uspjelo');
				setMessage("");
			}
		);

	}

	function onChange(event) {
		//console.log("changing");
		const { name, value } = event.target;
		//console.log(name, value);
		setEventForm((oldForm) => ({ ...oldForm, [name]: value }));
	}
	function onChoose(cHike) {
		setError("");
		setEventForm((oldForm) => ({
			...oldForm,
			hike: cHike
		}));
		setCurrentHike(cHike);
	}
	return (
		//TODO ne rade radiobuttoni
		<Container>
			<form id="newEventForm" onChange={onChange} onSubmit={onSubmit}>
				<div className="centered">
					<label className="important green">Odaberite izlet:</label>
				</div>
				<Row>
					{hikes.map((hike, idx) => (
						<Col lg="4" key={idx}>
							<div className="hikeContainer" key={hike.id}>
								<div>Naziv: {hike.name}</div>
								<div>Lokacija: {hike.location.name}</div>
								<div>Duljina staze: {hike.length}</div>
								<div>
									Stvorio korisnik:{' '}
									{hike.creator ? (
										<>
											{hike.creator.firstName} {hike.creator.lastName}
										</>
									) : (
											'IZBRISAN'
										)}
								</div>
								<div className="centered">
									<Button
										variant="primary"
										onClick={() => { onChoose(hike) }}
										name="izleti"
										value={hike.id}
									>Odaberi</Button>
								</div>
							</div>
						</Col>
					))}
				</Row>
				{currentHike ?
					<Card>
						Odabran je izlet:
					<HikeMinimal hike={currentHike}></HikeMinimal>
					</Card> :
					<Card>

					</Card>}
				<Row>
					<Col>
						<label>Unesite opis događaja:</label>
					</Col>
				</Row>
				<br></br>
				<textarea
					name="description"
					value={eventForm.description}
					rows="4"
					cols="50"
				>
					Opis događaja...
				</textarea>
				<br></br>
				<Row>
					<Col lg="8">
						<label>Odaberite datum i vrijeme početka događaja</label>
					</Col>
					<Col>
						<input
							type="datetime-local"
							id="startDate"
							name="startDate"
						></input>
					</Col>
				</Row>
				<Row>
					<Col lg="8">
						<label>Odaberite datum i vrijeme završetka događaja</label>
					</Col>
					<Col>
						<input
							type="datetime-local"
							id="endDate"
							name="endDate"
						></input>
					</Col>
				</Row>
				<br></br>
				<Row>
					<Col>
						<div className="centered">
							<Button type="submit">Stvori</Button>
						</div>
					</Col>
				</Row>
			</form>
			{error ? error : message}
		</Container>
	);
}
export default NewEvent;

/*<div key={idx} className="hikeCountainer">
						<div>Naziv: {hike.name} </div>
						<div>Lokacija: {hike.location.name} </div>
						<div>Duljina: {hike.lenght} </div>
				<div>Intenzitet: {hike.intensity} </div>*/
