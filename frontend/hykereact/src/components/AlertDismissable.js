import React, { useState } from 'react';
import { Alert } from 'react-bootstrap';

const AlertDismissable = ({ message }) => {
	const [show, setShow] = useState(true);

	return show ? (
		<Alert variant="primary" onClose={() => setShow(false)} dismissible>
			{message}
		</Alert>
	) : null;
};

export default AlertDismissable;
