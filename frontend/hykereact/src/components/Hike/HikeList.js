import React from 'react';
import { Alert, Col } from 'react-bootstrap';
import HikeMinimal from './HikeMinimal';
import "./HikeList.css"

const HikeList = ({ hikes, loading, error, conditions }) => {
	return error ? (
		<Alert variant="info">{error}</Alert>
	) : loading ? (
		<Alert variant="info">Dohvaćanje izleta...</Alert>
	) : !hikes || hikes.length === 0 ? (
		<Alert variant="info">Nema izleta koji zadovoljavaju kriterije.</Alert>
	) : (hikes
			.filter((hike) => {
				if (conditions) {
					for (let i = 0; i < conditions.length; i++) {
						if (!conditions[i](hike)) return false;
					}
				}
				return true;
			})
			.map((hike) =>
			<Col key={hike.id} lg="4">
			<HikeMinimal key={hike.id} hike={hike} />
			</Col>
			));
};

export default HikeList;
