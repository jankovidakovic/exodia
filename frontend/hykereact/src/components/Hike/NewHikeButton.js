import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const NewHikeButton = () => (
	<Link to="/new-hike">
		<Button variant="success">Novi izlet</Button>
	</Link>
);

export default NewHikeButton;
