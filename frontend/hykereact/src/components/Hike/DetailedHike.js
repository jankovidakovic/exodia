import React, { useEffect, useState } from 'react';
//import "./Fieldtrip.css";
import {
	Link,
	Redirect,
	useHistory,
	useParams,
	withRouter
} from 'react-router-dom';
import { Formik, Field, Form } from 'formik';

import { Card, Col, Button, Row } from 'react-bootstrap';

import axios from 'axios';
import Axios from 'axios';
import { useAuthState } from '../Auth/AuthContext';
import { url } from '../../constants';
import LoginPrompt from '../LoginPrompt';

const HikeDetailed = () => {
	//const deleteAllowed = true;
	//viewReportsAllowed = true;

	const [reports, setReports] = useState([]);

	const { id } = useParams();
	const history = useHistory();

	const auth = useAuthState();

	const [hike, setHike] = useState({});

	const [messages, setMessages] = useState({ addWishlistMessage: '' });

	const [averageRating, setAverageRating] = useState(0.0);

	const [archiveDesc, setArchiveDesc] = useState('');

	const [showArchiveForm, setShowArchiveForm] = useState(false);

	const getAverageRating = () => {
		Axios.get(`${url}/hikes/${id}/average-rating`)
			.then(
				(response) => {
					setAverageRating(response.data);
				},
				(error) => {
					setAverageRating(0.0);
				}
			)
			.catch((error) => {
				setAverageRating(0.0);
			});
	};

	useEffect(() => {
		axios.get(`${url}/hikes/${id}`).then(
			(response) => {
				//console.log(response);
				if (response.status === 200) {
					//TODO - change response status both on frontend and backend
					console.log('individual hike fetched');
					//showcase to user that the account has been deleted
					setHike(response.data);

					console.log(response.data);
				}
			},
			(err) => {
				console.log(err);
				//showcase error to user
				//refresh page to look normal again
			}
		);

		getAverageRating();
		const interval1 = setInterval(getAverageRating, 5000);
		getReports();
		const interval2 = setInterval(getReports, 5000);

		return () => {
			clearInterval(interval1);
			clearInterval(interval2);
		};
	}, []);

	const getReports = () => {
		if (auth.user.role === 'ADMIN') {
			axios
				.get(`${url}/hikes/${id}/reports`)
				.then((response) => {
					setReports(response.data);
				})
				.catch((e) => {
					console.log(e);
					return e;
				});
		}
	};

	const deleteHike = () => {
		console.log('Request to delete hike...');
		Axios.delete(`${url}/hikes/${id}`)
			.then(
				(response) => {
					console.log(response);
					history.push({
						pathname: '/hikes',
						state: {
							message: 'Izlet uspješno obrisan.' //TODO - set as dismissable alert
						}
					});
				},
				(error) => {
					console.log('Deleting hike failed with non-2xx');
					console.log(error);
				}
			)
			.catch((error) => {
				console.log('deleting hike failed unexpectedly');
				console.log(error);
			});
	};

	const addToWishlist = () => {
		console.log('adding to wishlist');
		Axios.post(`${url}/users/${auth.user.id}/wishlist`, { hikeId: id })
			.then(
				(response) => {
					console.log('GOT RESPONSE');
					if (response.status === 201) {
						// feedback for developer
						console.log('hike added to wishlist!');
						setMessages((oldMessages) => ({
							...oldMessages,
							wishlistMessage: 'Izlet dodan na vašu listu želja!'
						}));
					}
					console.log(response);
				},
				(error) => {
					console.log('wishlist add failed with non-2xx');
					console.log(error);

					if (error.response.status === 409) {
						setMessages((oldMessages) => ({
							...oldMessages,
							wishlistMessage:
								'Ovaj izlet je već na vašoj listi želja'
						}));
					} else {
						setMessages((oldMessages) => ({
							...oldMessages,
							wishlistMessage: 'Pogreška u dodavanju izleta'
						}));
					}
				}
			)
			.catch((e) => {
				console.log('wishlist adding failed unexpectedly');
				console.log(e);
				return e;
			});
	};
	const sendReport = (text) => {
		console.log('sending report');
		Axios.post(`${url}/hikes/${id}/reports`, {
			user: {
				id: auth.user.id,
				firstName: auth.user.firstName,
				lastName: auth.user.lastName
			},
			report: text
		})
			.then(
				(response) => {
					if (response.status === 201) {
						// feedback for developer
						console.log('Prijava uspjela');
						console.log(response.data);
						Axios.get(`/hikes/${id}/reports`).then((res) => {
							setReports(res.data);
						});
					} else {
						console.log('Dogodila se pogreska');
					}
				},
				(error) => {
					console.log('report failed non-2xx');
					console.log(error);
				}
			)
			.catch((e) => {
				console.log('report failed unexpectedly');
				console.log(e);
			});
	};

	/*
	const toggleExpand = () => {
		this.hutsExpanded = !(this.hutsExpanded);
		this.setState(oldState => ({ ...oldState, expandButtonText: this.hutsExpanded ? "Skrati" : "Pokazi sve" }));

	}

	*/

	const onChange = (event) => {
		setArchiveDesc(event.target.value);
	};

	const archive = () => {
		const archivedHikeDTO = {
			hikeMinimalDTO: {
				id: id,
				name: hike.name,
				location: hike.location,
				length: hike.hikeLength,
				intensity: hike.hikeIntensity,
				publicity: hike.publicity,
				creator: hike.creator
			},
			archiveDescription:
				archiveDesc === '' ? 'Standardni opis' : archiveDesc,
			archivedAt: Date.now(),
			doneAt: Date.now()
		};
		//console.log(archivedHikeDTO);
		Axios.post(
			`${url}/users/${auth.user.id}/archived-hikes/`,
			archivedHikeDTO
		)
			.then(
				(response) => {
					console.log(response);
				},
				(err) => {
					console.log(err);
				}
			)
			.catch((e) => {
				console.log(e);
				return e;
			});
	};

	const deleteReport = (reportId) => {
		console.log('Request to delete hike...');

		Axios.delete(`${url}/hikes/${id}/reports/${reportId}`)
			.then(
				(response) => {
					if (response.status === 200) {
						// feedback for developer
						console.log('Prijava izbrisana!');
						setReports((reports) =>
							reports.filter((report) => report.id !== reportId)
						);
					} else if (response.status === 404) {
						console.log('Prijava je vec izbrisana');
						this.reports = this.reports.filter(
							(rep) => rep.id !== reportId
						);
					} else {
						console.log('Dogodila se pogreska');
					}
				},
				(error) => {
					console.log('report deletion failed non 2xx');
					console.log(error);
					if (error.response && error.response.status === 404) {
						setReports((reports) =>
							reports.filter((report) => report.id !== reportId)
						);
					}
				}
			)
			.catch((e) => {
				console.log('report deletion failed unexpectedly');
				console.log(e);
				return e;
			});
	};
	console.log('Hike id: ', id);
	return (
		<div className="container">
			<div>
				<div className="row">
					<div className="col-sm-8">
						<div className="centered important">
							{hike.name} {/*(id: {id})*/}
						</div>
					</div>
					<div className="col-sm-4 float-right">
						<Link to="/hikes">Natrag na izlete</Link>
					</div>
				</div>
				{hike.publicity == false && (
					<div className="row">
						Ovaj izlet je privatan i nijedan drugi korisnik Hyke-a
						osim vas mu ne može pristupiti.
					</div>
				)}
				<br></br>
				{hike.location ? (
					<Row>
						<Col lg="3">
							<label className="important">Lokacija:</label>
						</Col>{' '}
						<Col>{hike.location.name}</Col>
					</Row>
				) : (
					<div />
				)}
				<Row>
					<Col lg="3">
						<label className="important">Duljina: </label>
					</Col>{' '}
					<Col>{hike.hikeLength} km</Col>
				</Row>
				<Row>
					<Col lg="3">
						<label className="important">Težina: </label>
					</Col>{' '}
					<Col>{hike.hikeIntensity}</Col>
				</Row>
				{hike.description ? (
					<Row>
						<Col lg="3">
							<label className="important">Opis: </label>
						</Col>{' '}
						<Col>{hike.description}</Col>
					</Row>
				) : (
					<div />
				)}
				{hike.hikeStart ? (
					<Row>
						<Col lg="3">
							<label className="important">Početak staze: </label>
						</Col>{' '}
						<Col>{hike.hikeStart}</Col>
					</Row>
				) : (
					<div />
				)}
				{hike.hikeEnd ? (
					<Row>
						<Col lg="3">
							<label className="important">Kraj staze: </label>
						</Col>{' '}
						<Col>{hike.hikeEnd}</Col>
					</Row>
				) : (
					<div />
				)}
				<div>
					<Row>
						<Col lg="3">
							<label className="important">
								Stvorio korisnik:{' '}
							</label>
						</Col>
						{hike.creator ? (
							<Col>
								{hike.creator.firstName +
									' ' +
									hike.creator.lastName}
							</Col>
						) : (
							<Col>IZBRISANO</Col>
						)}
					</Row>
				</div>
				<Row>
					<Col lg="3">
						<label className="important">Datum stvaranja: </label>
					</Col>{' '}
					<Col>{hike.createdAt}</Col>
				</Row>
				<Row>
					<Col>
						<label className="important">
							Sadrži planinarske domove:
						</label>
					</Col>
				</Row>
				{hike.mountainHuts && hike.mountainHuts.length > 0 ? (
					<Col>
						<Row>
							{hike.mountainHuts.map((mountainHut, idx) => (
								<Col key={idx} lg="4">
									<div className="hutContainer">
										<div>Naziv: {mountainHut.name}</div>
										<div>
											Lokacija:{' '}
											{mountainHut.location.name}
										</div>
										<div className="button">
											<Button variant="warning">
												<Link
													to={`/mountain-huts/${mountainHut.id}`}
													key={mountainHut.id}
													className="link"
												>
													Detalji
												</Link>
											</Button>
										</div>
									</div>
								</Col>
							))}
							{/*
                            {this.huts.length > this.glanceHutNum ?
                                <button type="button"
                                    onClick={this.toggleExpand}
                                    className="btn btn-primary mx-3">
                                    {this.state.expandButtonText}</button>
                                : <div />}
                        <Card>
                            <div style={{ paddingLeft: "10px" }}>
                                {(!this.hutsExpanded && this.huts.length > this.glanceHutNum ?
                                    this.huts.splice(0, this.glanceHutNum) : this.huts)
                                    .map(hut => <div key={hut.name}>{hut.name}</div>)}
							</div>
                        </Card>
							*/}
						</Row>
					</Col>
				) : (
					<div> Ne sadrži domove </div>
				)}
				<br></br>
				{hike.publicity && (
					<Row>
						<Col lg="3">
							<label className="important">
								Prosječna ocjena:{' '}
							</label>
						</Col>{' '}
						<Col>
							{(Math.round(averageRating * 100) / 100).toFixed(2)}
						</Col>
					</Row>
				)}
				<br></br>
				{auth.authenticated ? (
					<>
						<div>
							<Row>
								{hike.publicity && (
									<>
										<Col lg="4">
											<label>
												Želite li ići na ovaj izlet?
											</label>
										</Col>
										<Col>
											<Link
												to={{
													pathname: '/events/new',
													state: {
														hike: hike,
														id: id
													}
												}}
												hikeId={hike.id}
											>
												<Button>Stvori događaj</Button>
											</Link>
										</Col>
									</>
								)}
								<Col>
									<Button onClick={addToWishlist}>
										Dodaj na listu želja
									</Button>
								</Col>
							</Row>
							{messages.wishlistMessage ? (
								<div>
									<br></br>
									<Row>
										<Col className="centered">
											<label>
												{messages.wishlistMessage}
											</label>
										</Col>
									</Row>
									<br></br>
								</div>
							) : (
								<br></br>
							)}
							{hike.publicity == true && (
								<Row>
									<Col lg="3">Ocjena </Col>
									<Col>
										<RatingBar
											id={id}
											user={{
												id: auth.user.id,
												firstName: auth.user.firstName,
												lastName: auth.user.lastName
											}}
										></RatingBar>
									</Col>
								</Row>
							)}
							<Row>
								<Col lg="12">
									<label>
										Već ste odradili izlet?
										<Button
											onClick={() =>
												setShowArchiveForm(true)
											}
										>
											Dodajte ga u arhivu.
										</Button>
									</label>
								</Col>
							</Row>
							{showArchiveForm && (
								<Row>
									<Col lg="3">
										<label>Opis za arhivu:</label>
										<input
											name="archiveDesc"
											value={archiveDesc}
											onChange={onChange}
										></input>
										<Button onClick={archive}>
											Dodaj u arhivu
										</Button>
									</Col>
								</Row>
							)}
						</div>
						<br></br>
						{hike.publicity == true && (
							<div>
								Primjećujete pogrešku?
								<Formik
									initialValues={{ reportText: '' }}
									onSubmit={(values) => {
										sendReport(values.reportText);
									}}
								>
									<Form>
										<Field name="reportText" type="text" />
										<button
											type="submit"
											className="btn btn-danger"
										>
											Pošalji prijavu
										</button>
									</Form>
								</Formik>
							</div>
						)}

						{auth.user &&
							auth.user.role === 'ADMIN' &&
							reports &&
							reports.length > 0 && (
								<Card>
									<div>
										Postoje prijave o netočnim informacijama
										za ovaj izlet:
									</div>
									{reports.map((report) => (
										<div key={report.id} className="row">
											<div className="col-sm-10">
												{report.report}
											</div>
											<button
												type="button"
												onClick={() =>
													deleteReport(report.id)
												}
												className="btn btn-danger"
											>
												Izbriši prijavu
											</button>
										</div>
									))}
								</Card>
							)}
						<Col>
							{auth.user &&
							auth.user.role === 'ADMIN' ? (
								<button
									type="button"
									onClick={deleteHike}
									className="btn btn-danger"
								>
									Izbriši izlet
								</button>
							) : (
								<div></div>
							)}
						</Col>
					</>
				) : (
					<LoginPrompt />
				)}
			</div>
		</div>
	);
};

export default withRouter(HikeDetailed);

class RatingBar extends React.Component {
	constructor(props) {
		super();
		this.handleMouseEnter = this.handleMouseEnter.bind(this);
		this.handleMouseLeave = this.handleMouseLeave.bind(this);

		this.state = {
			i: 0,
			id: props.id,
			user: props.user
		};
	}

	postRating(rating, user) {
		console.log('rating:', rating);
		console.log('user: ', user);
		Axios.post(`${url}/hikes/${this.props.id}/ratings`, {
			rating: rating,
			user: user
		})
			.then(
				(response) => {
					if (response.status === 200) {
						console.log('Poslana ocjena: ' + rating);
					} else {
						console.log(response.status);
						console.log('Ocjena nije poslana');
					}
				},
				(error) => {
					console.log('rating non-2xx');
					console.log(error);
				}
			)
			.catch((e) => {
				console.log('rating unexp');
				console.log(e);
				return e;
			});
	}

	i = 0;

	handleMouseEnter(k) {
		this.setState({ i: k });
	}

	handleMouseLeave() {
		this.setState({ i: 0 });
	}

	render() {
		const elements = [1, 2, 3, 4, 5];
		return (
			<ul>
				{elements.map((value, index) => {
					return (
						<button
							key={value}
							className={
								value <= this.state.i
									? 'star starOnHover'
									: 'star'
							}
							onMouseEnter={() => {
								this.handleMouseEnter(value);
							}}
							onMouseLeave={this.handleMouseLeave}
							onClick={() => {
								console.log(
									'izlet ' +
										this.state.id +
										', ocjena: ' +
										this.state.i
								);
								this.postRating(this.state.i, this.state.user);
							}}
						></button>
					);
				})}
			</ul>
		);
	}
}
