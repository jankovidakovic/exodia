import React from 'react';
import { Link } from 'react-router-dom';
import './HikeList.css';
import { Button } from 'react-bootstrap';

const HikeMinimal = ({ hike }) =>
	!hike ? null : (
		<div className="hikeContainer" key={hike.id}>
			<div>Naziv: {hike.name}</div>
			<div>Lokacija: {hike.location.name}</div>
			<div>Duljina staze: {hike.length}</div>
			<div>
				Stvorio korisnik:{' '}
				{hike.creator ? (
					<>
						{hike.creator.firstName} {hike.creator.lastName}
					</>
				) : (
					'IZBRISAN'
				)}
			</div>
			<div className="centered">
				<Link to={'/hikes/' + hike.id} className="browserLink">
					<Button variant="success">Detalji</Button>
				</Link>
			</div>
		</div>
	);

export default HikeMinimal;
