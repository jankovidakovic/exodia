export const required = 'Ovo polje je obavezno';
export const emailValid = 'Molimo, unesite ispravan e-mail';
export const emailExists = 'Odabrani e-mail je zauzet';
export const passwordTooShort = 'Lozinka mora sadržavati minimalno 8 znakova';
export const passwordMatch = 'Lozinke se ne podudaraju';
export const usernameExists = 'Odabrano korisničko je zauzeto.';
