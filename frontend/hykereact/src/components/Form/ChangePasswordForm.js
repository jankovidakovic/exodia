import { Formik, Form, Field } from 'formik';
import { Col, Container, Row, Form as BSForm, Button } from 'react-bootstrap';
import * as yup from 'yup';
import * as feedback from './ErrorMessages';
import React, { useEffect } from 'react';
import Axios from 'axios';
import { url } from '../../constants';

const validationSchema = yup.object({
	oldPassword: yup.string().required(feedback.required),
	newPassword: yup
		.string()
		.required(feedback.required)
		.min(8, feedback.passwordTooShort),
	confirmNewPassword: yup
		.string()
		.required(feedback.required)
		.oneOf([yup.ref('newPassword'), null], feedback.passwordMatch)
});

const ChangePasswordForm = ({ id }) => {
	useEffect(() => {
		console.log(id);
	}, []);

	return (
		<Container>
			<Row>
				<Col>
					<Formik
						initialValues={{
							oldPassword: '',
							newPassword: '',
							confirmNewPassword: ''
						}}
						validationSchema={validationSchema}
						onSubmit={(data, { setSubmitting }) => {
							setSubmitting(true);
							//put request
							console.log('Submit initiated');

							Axios.put(`${url}/users/${id}/password`, data).then(
								(response) => {
									if (response.status === 200) {
										console.log(
											'Lozinka uspješno promijenjena'
										);
										//TODO - security stuff, update JWT i guess
									} else {
										console.log(response);
									}
								},
								(err) => {
									console.log(err);
								}
							);
							setSubmitting(false);
						}}
					>
						{({ values, errors, touched, isSubmitting }) => (
							<Form as={BSForm}>
								{' '}
								{/* TODO - dont pass values maybe?? */}
								<Container>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikOldPassword"
										>
											<Field
												placeholder="stara lozinka"
												name="oldPassword"
												type="password"
												as={BSForm.Control}
												isInvalid={
													touched.oldPassword &&
													!!errors.oldPassword
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.oldPassword}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikNewPassword"
										>
											<Field
												placeholder="nova lozinka"
												name="newPassword"
												type="password"
												as={BSForm.Control}
												isInvalid={
													touched.newPassword &&
													!!errors.newPassword
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.newPassword}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikConfirmNewPassword"
										>
											<Field
												placeholder="potvrda nove lozinke"
												name="confirmNewPassword"
												type="password"
												as={BSForm.Control}
												isInvalid={
													touched.confirmNewPassword &&
													!!errors.confirmNewPassword
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.confirmNewPassword}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<Col>
											<Button
												disabled={isSubmitting}
												variant="primary"
												type="submit"
											>
												Promjena lozinke
											</Button>
										</Col>
									</Row>
								</Container>
							</Form>
						)}
					</Formik>
				</Col>
			</Row>
		</Container>
	);
};

export default ChangePasswordForm;
