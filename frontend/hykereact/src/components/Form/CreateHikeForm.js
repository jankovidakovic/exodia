import React, { useEffect, useState } from 'react';
import { Field, FieldArray, Form, Formik } from 'formik';
import { Button, Col, Container, Form as BSForm, Row, Card } from 'react-bootstrap';
import * as yup from 'yup';
import { required } from './ErrorMessages';
import Axios from 'axios';
import { useAuthState } from '../Auth/AuthContext';
import { url } from '../../constants';
import "./CreateHikeForm.css"

const validationSchema = yup.object({
	name: yup.string().required(required),
	description: yup.string().required(required),
	hikeStart: yup.string().required(required),
	hikeEnd: yup.string().required(required),
	hikeLength: yup.number().required(required)
});

const CreateHikeForm = () => {
	const auth = useAuthState();
	const [error, setError] = useState(false);
	const [locations, setLocations] = useState([]);
	const [mountainHuts, setMountainHuts] = useState([]);
	const [allMountainHuts, setAllMountainHuts] = useState([]);

	const [message, setMessage] = React.useState("");


	const filterHuts = (locId) => {
		setMountainHuts(allMountainHuts.filter((h) => {
			return h.location.id + "" === locId
		}));
	}


	const fetchHuts = (locationId, setFieldValue, handleChange) => {
		handleChange();
		Axios.get(`${url}/mountain-huts?locationId=${locationId}`)
			.then(
				(response) => {
					setFieldValue('mountainHuts', response.data);
				},
				(error) => {
					console.log('mountain huts on location returned non-2xx');
					console.log(error);
					//TODO - do something in here
				}
			)
			.catch((error) => {
				console.log(
					'mountain huts on location caused an unexpected error'
				);
				console.log(error);
			});
	};

	useEffect(() => {
		Axios.get(`${url}/locations`).then((response) => {
			setLocations(response.data);
		});
		Axios.get(`${url}/mountain-huts`).then((response) => {
			//setMountainHuts(response.data);
			setAllMountainHuts(response.data);
		});
	}, []);

	const availableIntensities = ["Lagano", "Srednje", "Teško", "Jako teško"];

	return (
		<Formik
			initialValues={{
				creator: auth.user.id,
				name: '',
				description: '',
				location: '',
				hikeStart: '',
				hikeEnd: '',
				hikeLength: '',
				intensity: availableIntensities[0],
				publicity: false,
				mountainHuts: []
			}}
			validationSchema={validationSchema}
			onSubmit={(data, { setSubmitting }) => {
				console.log(data);
				data.intensity = ["LOW", "MODERATE", "HIGH", "VERY_HIGH"][availableIntensities.indexOf(data.intensity)];
				console.log(data);
				setMessage("Slanje...")
				Axios.post(`${url}/hikes`, data)
					.then(
						(response) => {
							if (response.status === 201) {
								setMessage("Izlet stvoren!");
								console.log('Hike succesfully created');
								//render dismissable notification

							}
						},
						(error) => {
							console.log(error);
							setError(true);
							setMessage("Pogreška")
							//TODO - add displaying error messages, check if all fields are valid
						}
					)
					.catch((error) => {
						console.log(error);
						setError(true);
					});
			}}
		>
			{({
				values,
				errors,
				touched,
				isSubmitting,
				setFieldValue,
				handleChange
			}) => (
				<Form as={BSForm}>
					<Container>
						<Row>
							<Col>
								<Field
									placeholder="naziv izleta"
									name="name"
									type="input"
									className="m-2"
								/>
							</Col>
							<Col>
								<Field
									placeholder="opis"
									name="description"
									type="input"
									className="m-2"
								/>
							</Col>
						</Row>
						<Row>
							<Col> Odabir lokacije
								<Field
									as="select"
									name="location"
									placeholder="Odabir lokacije"
									className="m-2"
									onChange={(e) => {
										values.mountainHuts = [];
										handleChange(e);
										//console.log(e.target.value);
										filterHuts(e.target.value);
									}}
								>
									<option
										name="location"
										className="m-2"
									></option>
									{locations.map((location) => (
										<option
											key={location.id}
											name="location"
											value={location.id}
											className="m-2"
										>
											{location.name}
										</option>
									))}
								</Field>
							</Col>
						</Row>
						{mountainHuts.length > 0 ?
							<div>
								<Row>
									<div className="centered">
										<label>Prolazi li staza kroz neke planinarske domove?</label>
									</div>
								</Row>
								<Row>
									{mountainHuts.map((mountainHut) => (
										<div key={mountainHut.id} className="locationFancy">
											<label>
												<Field
													type="checkbox"
													checked={values.mountainHuts.includes("" + mountainHut.id)}
													name="mountainHuts"
													value={mountainHut.id}
													className="m-2"
												/>
												{mountainHut.name}
											</label>
										</div>
									))}
								</Row>
							</div>
							: <div></div>}

						<Row>
							<Col>
								<Field
									placeholder="Početak staze"
									name="hikeStart"
									type="input"
									className="m-2"
								/>
							</Col>
							<Col>
								<Field
									placeholder="Kraj staze"
									name="hikeEnd"
									type="input"
									className="m-2"
								/>
							</Col>
						</Row>
						<Row>
							<Col>
								<Field
									placeholder="Duljina"
									name="hikeLength"
									type="input"
									className="m-2"
								/>
							</Col>

							<Col> Procjena težine
								<Field
									as="select"
									name="intensity"
									placeholder="Procjena težine"
									className="m-2"
								>

									{availableIntensities.map((intensity) => (
										<option
											key={intensity}
											name="intensity"
											value={intensity}
											className="m-2"
										>
											{intensity}
										</option>
									))}
								</Field>
							</Col>


						</Row>
						<Row>
							<Col>
								<label>Javni izlet</label>
								<Field
									type="checkbox"
									checked={values.publicity}
									name="publicity"
									value={values.publicity}
									className="m-2"
								/>
							</Col>
							<Col>
								<Button
									variant="primary"
									type="submit"
									className="m-2"
								>
									Stvori
								</Button>
							</Col>
						</Row>
					</Container>
					<Col>{message}</Col>
				</Form>
			)}

		</Formik>

	);
};

export default CreateHikeForm;
