import { Formik, Form, Field } from 'formik';
import { Col, Container, Row, Form as BSForm, Button } from 'react-bootstrap';
import * as yup from 'yup';
import * as feedback from './ErrorMessages';
import React, { useEffect } from 'react';
import Axios from 'axios';
import { url } from '../../constants';

const validationSchema = yup.object({
	firstName: yup.string().required(feedback.required),
	lastName: yup.string().required(feedback.required),
	username: yup.string().required(feedback.required),
	email: yup.string().required(feedback.required).email(feedback.emailValid)
});

const UpdateUserInfoForm = ({ id, firstName, lastName, username, email }) => {
	return (
		<Container>
			<Row>
				<Col>
					<Formik
						initialValues={{
							firstName: firstName,
							lastName: lastName,
							username: username,
							email: email
						}}
						enableReinitialize
						validationSchema={validationSchema}
						onSubmit={(data, { setSubmitting }) => {
							setSubmitting(true);
							//put request

							Axios.put(`${url}/users/${id}/info`, data).then(
								(response) => {
									if (response.status === 200) {
										console.log(
											'Podatci uspješno ažurirani.'
										);
									} else {
										console.log(response);
									}
								},
								(err) => {
									console.log(err);
								}
							);
							setSubmitting(false);
							console.log('API request finished.');
						}}
					>
						{({ values, errors, touched, isSubmitting }) => (
							<Form values={values} as={BSForm}>
								{' '}
								{/* TODO - dont pass values maybe?? */}
								<Container>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikFirstName"
										>
											<Field
												placeholder="ime"
												name="firstName"
												type="input"
												as={BSForm.Control}
												isInvalid={
													touched.firstName &&
													!!errors.firstName
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.firstName}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikLastName"
										>
											<Field
												placeholder="prezime"
												name="lastName"
												type="input"
												as={BSForm.Control}
												isInvalid={
													touched.lastName &&
													!!errors.lastName
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.lastName}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikUsername"
										>
											<Field
												placeholder="korisničko ime"
												name="username"
												type="input"
												as={BSForm.Control}
												isInvalid={
													touched.username &&
													!!errors.username
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.username}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<BSForm.Group
											as={Col}
											controlId="validationFormikEmail"
										>
											<Field
												placeholder="e-mail"
												name="email"
												type="email"
												as={BSForm.Control}
												isInvalid={
													touched.email &&
													!!errors.email
												}
											/>
											<BSForm.Control.Feedback type="invalid">
												{errors.email}
											</BSForm.Control.Feedback>
										</BSForm.Group>
									</Row>
									<Row>
										<Col>
											<Button
												disabled={isSubmitting}
												variant="primary"
												type="submit"
											>
												Ažuriraj podatke
											</Button>
										</Col>
									</Row>
								</Container>
							</Form>
						)}
					</Formik>
				</Col>
			</Row>
		</Container>
	);
};

export default UpdateUserInfoForm;
