import Axios from 'axios';
import { Field, Form, Formik } from 'formik';
import * as yup from 'yup';
import * as feedback from './ErrorMessages';
import React from 'react';
import { Button, Col, Container, Form as BSForm, Row } from 'react-bootstrap';
import { url } from '../../constants';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { withRouter } from 'react-router-dom';
const validationSchema = yup.object({
	firstName: yup.string().required(feedback.required),
	lastName: yup.string().required(feedback.required),
	username: yup.string().required(feedback.required),
	email: yup.string().required(feedback.required).email(feedback.emailValid),
	password: yup
		.string()
		.required(feedback.required)
		.min(8, feedback.passwordTooShort),
	confirmPassword: yup
		.string()
		.required(feedback.required)
		.min(8, feedback.passwordTooShort)
		.oneOf([yup.ref('password'), null], feedback.passwordMatch)
}); //TODO - add min/max constraints

const RegistrationForm = () => {
	const history = useHistory();

	return (
		<Formik
			initialValues={{
				firstName: '',
				lastName: '',
				username: '',
				email: '',
				password: '',
				confirmPassword: ''
			}}
			validationSchema={validationSchema}
			onSubmit={(data, { setErrors, setSubmitting }) => {
				//TODO - check
				//client-side validatae : DONE BY YUP
				//server-side validate
				setSubmitting(true);
				Axios.post(`${url}/register`, data)
					.then(
						(response) => {
							history.push({
								pathname: '/login',
								state: {
									info: 'Korisnički račun uspješno kreiran!'
								}
							});
						},
						(error) => {
							if (error.response) {
								if (error.response.status === 409) {
									console.log(error.response);
									//conflict
									if (error.response.data === 'email') {
										setErrors((errors) => ({
											...errors,
											email: 'Odabrani e-mail je zauzet.'
										}));
									} else if (
										error.response.data === 'username'
									) {
										setErrors((errors) => ({
											...errors,
											username:
												'Odabrano korisničko ime je zauzeto.'
										}));
									}
								}

								setSubmitting(false);
							} else {
								console.log('unexpected error: ', error);
								setSubmitting(false);
							}
						}
					)
					.catch((error) => {
						console.log('Error caught: ', error);
						setSubmitting(false);
					});
			}}
		>
			{({ values, errors, touched, isSubmitting }) => (
				<Form as={BSForm}>
					<Container className="inputsDiv">
						<h1 className="registracija">Registracija</h1>
						<Row className="inputsBox">
							<BSForm.Group
								as={Col}
								controlId="validationFormikFirstName"
							>
								<Field
									placeholder="ime"
									name="firstName"
									type="input"
									as={BSForm.Control}
									isInvalid={
										touched.firstName && !!errors.firstName
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.firstName}
								</BSForm.Control.Feedback>
							</BSForm.Group>
							<BSForm.Group
								as={Col}
								controlId="validationFormikLastName"
							>
								<Field
									placeholder="prezime"
									name="lastName"
									type="input"
									as={BSForm.Control}
									isInvalid={
										touched.lastName && !!errors.lastName
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.lastName}
								</BSForm.Control.Feedback>
							</BSForm.Group>
						</Row>
						<Row className="inputsBox">
							<BSForm.Group
								as={Col}
								controlId="validationFormikUsername"
							>
								<Field
									placeholder="korisničko ime"
									name="username"
									type="input"
									as={BSForm.Control}
									isInvalid={
										touched.username && !!errors.username
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.username}
								</BSForm.Control.Feedback>
							</BSForm.Group>
							<BSForm.Group
								as={Col}
								controlId="validationFormikEmail"
							>
								<Field
									placeholder="e-mail"
									name="email"
									type="email"
									as={BSForm.Control}
									isInvalid={touched.email && !!errors.email}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.email}
								</BSForm.Control.Feedback>
							</BSForm.Group>
						</Row>
						<Row className="inputsBox">
							<BSForm.Group
								as={Col}
								controlId="validationFormikPassword"
							>
								<Field
									placeholder="lozinka"
									name="password"
									type="password"
									as={BSForm.Control}
									isInvalid={
										touched.password && !!errors.password
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.password}
								</BSForm.Control.Feedback>
							</BSForm.Group>
							<BSForm.Group
								as={Col}
								controlId="validationFormikConfirmPassword"
							>
								<Field
									placeholder="potvrda lozinke"
									name="confirmPassword"
									type="password"
									as={BSForm.Control}
									isInvalid={
										touched.confirmPassword &&
										!!errors.confirmPassword
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.confirmPassword}
								</BSForm.Control.Feedback>
							</BSForm.Group>
						</Row>
						{
							//TODO - add confirmation checkbox
						}

						<Button
							variant="primary"
							type="submit"
							disabled={isSubmitting}
						>
							Registracija
						</Button>
					</Container>
				</Form>
			)}
		</Formik>
	);
};

export default withRouter(RegistrationForm);
