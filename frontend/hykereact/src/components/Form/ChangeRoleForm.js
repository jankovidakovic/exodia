import Axios from 'axios';
import { Field, Form, Formik } from 'formik';
import React, { useEffect } from 'react';
import { Button, Form as FormRB } from 'react-bootstrap';
import { url } from '../../constants';

const ChangeRoleForm = ({ id, role }) => {
	useEffect(() => {
		console.log(id);
		console.log(role);
	}, [id, role]);
	return (
		<div>
			<Formik
				initialValues={{
					role: String(role)
				}}
				enableReinitialize
				onSubmit={(data) => {
					Axios.put(`${url}/users/${id}/role`, {
						role: data.role
					})
						.then(
							(response) => {},
							(error) => {}
						)
						.catch((error) => {});
					//TODO - implement
				}}
			>
				{({ values }) => (
					<Form>
						<Field
							name="role"
							value="USER"
							type="radio"
							label="Korisnik"
							as={FormRB.Check}
						/>
						<Field
							name="role"
							value="ADMIN"
							type="radio"
							label="Administrator"
							as={FormRB.Check}
						/>
						<Field
							name="role"
							value="DUTY"
							type="radio"
							label="Dežurni planinar"
							as={FormRB.Check}
						/>
						<Button variant="primary" type="submit">
							Promjena prava pristupa korisnika
						</Button>
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default ChangeRoleForm;
