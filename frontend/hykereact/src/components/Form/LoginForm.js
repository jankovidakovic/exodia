import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as yup from 'yup';
import * as feedback from './ErrorMessages';
import React from 'react';
import { Button, Col, Container, Form as BSForm, Row } from 'react-bootstrap';
import { useAuthDispatch, useAuthState } from '../Auth/AuthContext';
import { loginUser } from '../Auth/AuthReducer/actions';
import './LoginForm.css';
import {
	useHistory,
	withRouter
} from 'react-router-dom/cjs/react-router-dom.min';

const validationSchema = yup.object({
	username: yup.string().required(feedback.required),
	password: yup.string().required(feedback.required)
});

const LoginForm = () => {
	const { loading, errorMessage } = useAuthState();
	const dispatch = useAuthDispatch();

	const [loginMessage, setLoginMessage] = React.useState("");

	const onSubmit = (data) => {
		loginUser(data, dispatch, (mess) => setLoginMessage(mess));
	};

	return (
		<Formik
			initialValues={{
				username: '',
				password: ''
			}}
			validationSchema={validationSchema}
			onSubmit={onSubmit}
		>
			{({ values, errors, touched, status }) => (
				<Form className="centered-container">
					<Container className="inputsDiv">
						<h1 className="login">Prijava</h1>
						<Row className="inputsBox">
							<BSForm.Group controlId="validationFormikUsername">
								<Field
									placeholder="korisničko ime"
									name="username"
									type="input"
									as={BSForm.Control}
									isInvalid={
										touched.username && !!errors.username
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.username}
								</BSForm.Control.Feedback>
							</BSForm.Group>
						</Row>
						<Row className="inputsBox">
							<BSForm.Group controlId="validationFormikPassword">
								<Field
									placeholder="lozinka"
									name="password"
									type="password"
									as={BSForm.Control}
									isInvalid={
										touched.password && !!errors.password
									}
								/>
								<BSForm.Control.Feedback type="invalid">
									{errors.password}
								</BSForm.Control.Feedback>
							</BSForm.Group>
						</Row>
						<Button
							variant="primary"
							type="submit"
							disabled={loading}
						>
							Prijava
						</Button>
						<BSForm.Control.Feedback type="invalid">
							{errorMessage}
						</BSForm.Control.Feedback>
						<div>{loginMessage}</div>
					</Container>

				</Form>
			)}
		</Formik>
	);
};

export default withRouter(LoginForm);
