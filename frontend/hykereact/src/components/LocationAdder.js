//import { render } from '@testing-library/react';
import React from 'react';
import './Registration.css';
import Axios from 'axios';

function LocationAdder(props) {
	//With this we define a setter for location form data
	const [locationForm, setLocationForm] = React.useState({
		locName: '',
		locDescription: ''
	});
	//Defines error and setter for errors
	const [error, setError] = React.useState('');
	//and for other messages
	const [message, setMessage] = React.useState('');

	//Defines what happens if there is a change on the site, it updates old location form data with new ones.
	function onChange(event) {
		//setError("");
		setMessage('');
		const { name, value } = event.target;
		setLocationForm((oldForm) => ({ ...oldForm, [name]: value }));
	}

	//const enableButton = false;

	//Defines what happends after we press the create button, when the submit happends and how we handle it
	function onSubmit(e) {
		//We prevent the default submit method that places the data in the http link
		e.preventDefault();
		setError('');
		//Sets data that we will send over to backend
		const locData = {
			name: locationForm.locName,
			description: locationForm.locDescription
		};
		//If somebody tries to create location without filling all inputs we send the coresponding error
		if (locationForm.locName === '' || locationForm.locDescription === '') {
			setError('Svi podatci moraju biti ispunjeni!');
			return;
		}

		//Sets the method that will be used in backend
		//Fetches the backend registration fucntion and sends the needed data
		Axios.post(`/locations`, locData).then(
			(response) => {
				console.log(response);
				if (response.status === 200) {
					//TODO - change to 201
					setMessage('Lokacija uspješno stvorena');
					setLocationForm({ locName: '', locDescription: '' });
				} else if (response.status === 409) {
					setError('Takva lokacija već postoji');
				} else {
					setError('Lokacija nije stvorena');
				}
			},
			(err) => {
				console.log(err);
			}
		);
	}

	//Returns the builded page for creating a location
	return (
		<div className="inputsDiv">
			<h1 className="login">Dodavanje lokacije</h1>
			<form className="centeredContainer" onSubmit={onSubmit}>
				<div className="inputsBox">
					<div>
						<label>Naziv lokacije</label>
					</div>
					<div>
						<input
							name="locName"
							onChange={onChange}
							value={locationForm.locName}
						></input>
					</div>
				</div>

				<div className="inputsBox">
					<div>
						<label>Opis</label>
					</div>
					<div>
						<input
							name="locDescription"
							onChange={onChange}
							value={locationForm.locDescription}
						></input>
					</div>
				</div>

				<div className="centeredContainer">
					{error === '' ? (
						message === '' ? (
							<div></div>
						) : (
							<div>{message}</div>
						)
					) : (
						<div className="error">{error}</div>
					)}
				</div>

				<div className="centeredContainer">
					<button className="centeredContainer" type="submit">
						Dodaj lokaciju
					</button>
				</div>
				{/*
                <div className="centeredContainer">

                    <label> Želite li pregledati postojeće lokacije? 
                        <Link to='/locations'> Sve lokacije </Link></label>
                </div>
                */}
			</form>
		</div>
	);
}

export default LocationAdder;
