import React from 'react';
import "./Browser.css";
import {Link} from "react-router-dom";


//With this we create the browser track that is shown under the header of the page, it contains links to different sites of our web application.
function Browser() {

    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }


    return (
        <table className="dhomepage-flex justify-content-start table table-bordered browserContainer">
            <tbody>
            <tr>
                <th scope="col" style={{borderColor:"#855723"}}>
                <Link to='/' className="browserLink">Naslovna</Link>
                </th>
                <th scope="col" style={{borderColor:"#855723"}}>
                <Link to='/hikes' className="browserLink">Planinarski izleti</Link>
                </th>
                <th scope="col" style={{borderColor:"#855723"}}>
                <Link to='/login' className="browserLink">Prijava</Link>
                </th>
                <th scope="col" style={{borderColor:"#855723"}}>
                <Link to='/registration' className="browserLink">Registracija</Link>
                </th>
				<th scope="col" style={{borderColor:"#855723"}}>
                <Link to='/locations' className="browserLink">Geografske lokacije</Link>
                </th>

                <th scope="col" style={{borderColor:"#855723"}}>
                <div className="dropdown">
                        <button onClick={myFunction} className="dropbtn">Upravljanje lokacijama</button>
                        <div id="myDropdown" className="dropdown-content">
                            <Link to='/locations/add' className="browserLink">Dodaj</Link>
                            <Link to='/locations/update' className="browserLink">Azuriraj</Link>
                            <Link to='/locations/delete' className="browserLink">Obrisi</Link>
                        </div>
                    </div>
                </th>
				
            </tr>
            </tbody>
        </table>
        
    )
}


export default Browser
