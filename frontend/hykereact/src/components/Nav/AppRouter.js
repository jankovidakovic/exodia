import React from 'react';
import Switch from 'react-bootstrap/esm/Switch';
import { BrowserRouter, Route } from 'react-router-dom';
import Browser from './Browser';
import Homepage from '../Homepage';
import UserDetailed from '../User/UserDetailed';
import AuthHeader from '../Auth/AuthHeader';
import LoginForm from '../Form/LoginForm';
import RegistrationForm from '../Form/RegistrationForm';
import ProtectedRoute from '../Auth/ProtectedRoute';
import MountainHuts from '../MountainHut/MountainHuts';
import MountainHut from '../MountainHut/MountainHut';
import MountainHutAdder from '../MountainHut/MountainHutAdder';
import UserEvents from '../Event/UserEvents';
import NewEvent from '../Event/NewEvent';
import DetailedEvent from '../Event/DetailedEvent';
import Archive from '../User/Archive';
import CreateHikeForm from '../Form/CreateHikeForm';
import DetailedHike from '../Hike/DetailedHike';
import GuestRoute from '../Auth/GuestRoute';
import PublicHikes from '../../pages/PublicHikes';
import Users from '../../pages/Users';
import Container from "react-bootstrap/Container"
import "./Approuter.css";

const AppRouter = () => {
	return (
		<BrowserRouter>
			{/*<AuthHeader /> */} 
			<Browser />
			<div className="appContainer">
			<Switch paddingLeft="0">
				<ProtectedRoute exact path="/" component={Homepage} />
				<Route exact path="/hikes" component={PublicHikes} />
				<Route exact path="/hikes/:id" component={DetailedHike} />
				<ProtectedRoute
					exact
					path="/new-hike"
					component={CreateHikeForm}
				/>
				{
					//<Route exact path="/mountain-huts" component={MountainHuts} />
					//<Route exact path="/" component={Homepage} />
				}
				<GuestRoute exact path="/login" component={LoginForm} />
				<GuestRoute
					exact
					path="/register"
					component={RegistrationForm}
				/>
				<ProtectedRoute exact path="/users" component={Users} />
				<ProtectedRoute
					exact
					path="/users/:id"
					component={UserDetailed}
				/>
				<ProtectedRoute
					exact
					path="/profile/:id"
					component={UserDetailed}
				/>
				<Route exact path="/mountain-huts" component={MountainHuts} />

				<ProtectedRoute
					exact
					path="/mountain-huts/new"
					component={MountainHutAdder}
				/>
				<Route
					exact
					path="/mountain-huts/:id"
					component={MountainHut}
				></Route>

				<ProtectedRoute exact path="/events" component={UserEvents} />

				<ProtectedRoute exact path="/events/new" component={NewEvent} />
				<ProtectedRoute
					exact
					path="/event/:id"
					component={DetailedEvent}
				/>

				<ProtectedRoute
					exact
					path="/users/:id/archived-hikes"
					component={Archive}
				/>
			</Switch>
			</div>
		</BrowserRouter>
	);
};

export default AppRouter;
