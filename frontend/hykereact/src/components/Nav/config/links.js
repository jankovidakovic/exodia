const links = [
	{
		to: '/',
		value: 'Novosti'
	},
	{
		to: '/hikes',
		value: 'Izleti'
	},
	{
		to: '/users',
		value: 'Korisnici'
	},
	{
		to: '/mountain-huts',
		value: 'Planinarski domovi'
	}
];

export default links;
