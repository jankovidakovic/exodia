import LoginForm from '../components/forms/LoginForm';
import RegistrationForm from '../components/forms/RegistrationForm';
import Homepage from '../../Homepage';
import Trip from '../components/Fieldtrip';
import Users from '../pages/Users';
import UserDetailed from '../components/UserDetailed';

//TODO - fix imports

const routes = {
	public: [
		{
			path: '/',
			component: Homepage
		},
		{
			path: '/login',
			component: LoginForm
		},
		{
			path: '/registration',
			component: RegistrationForm
		},
		{
			path: '/hikes',
			component: Trip
		},
		{
			path: '/users',
			component: Users
		}
	],

	protected: [
		{
			path: '/profile',
			component: UserDetailed
		}
		//TODO - figure this out
	]
};

export default routes;
