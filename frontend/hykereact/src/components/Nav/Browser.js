import React from 'react';
import './Browser.css';
import { Link } from 'react-router-dom';
import links from './config/links';
import  	Navbar from 'react-bootstrap/Navbar';
import Nav from "react-bootstrap/Nav";
import AuthHeader from '../Auth/AuthHeader';

//With this we create the browser track that is shown under the header of the page, it contains links to different sites of our web application.
function Browser() {

	const Navlinks = [];
	links.map((link,idx) => (
		Navlinks[idx] = link 
	));
	console.log(Navlinks[0]);

	return (
		<Navbar className="dhomepage-flex justify-content-end table table-bordered browserContainer" collapseOnSelect expand="lg">
			<Navbar.Toggle aria-controls = "basic-navbar-nav"/>
			<Navbar.Collapse id ="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link href={Navlinks[0].to}>
						{Navlinks[0].value}
					</Nav.Link>
					<Nav.Link href={Navlinks[1].to}>
						{Navlinks[1].value}
					</Nav.Link>
					<Nav.Link href={Navlinks[2].to}>
						{Navlinks[2].value}
					</Nav.Link>
					<Nav.Link href={Navlinks[3].to}>
						{Navlinks[3].value}
					</Nav.Link>
				</Nav>
				<Nav className="justify-content-end">
				<AuthHeader  />
				</Nav>
			</Navbar.Collapse>
			
			
			
			
			
			{/*<tbody>
				<tr>
					{links.map((link, idx) => (
						<th
							key={idx}
							scope="col"
							style={{ borderColor: '#855723' }}
						>
							<Link to={link.to} className="browserLink">
								{link.value}
							</Link>
						</th>
					))}
				</tr>
					</tbody>*/}
		</Navbar>
	);
}

export default Browser;
