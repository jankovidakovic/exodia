import React from 'react';
import { Alert, ListGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { UserMinimal } from './UserMinimal';

const UserList = ({ users, loading, error }) => {
	return error ? (
		<Alert variant="info">{error}</Alert>
	) : loading ? (
		<Alert variant="info">Dohvaćanje korisnika...</Alert>
	) : !users || users.length === 0 ? (
		<Alert variant="info">Nema drugih korisnika u sustavu.</Alert>
	) : (
		<ListGroup>
			{users.map((user) => (
				<Link to={`/users/${user.id}`} key={user.id}>
					<UserMinimal
						key={user.id}
						id={user.id}
						firstName={user.firstName}
						lastName={user.lastName}
					/>
				</Link>
			))}
		</ListGroup>
	);
};

export default UserList;
