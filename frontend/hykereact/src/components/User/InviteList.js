import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Alert } from 'react-bootstrap';
import { url } from '../../constants';
import { useAuthState } from '../Auth/AuthContext';
import Invite from '../Social/Invite';

const InviteList = () => {
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [events, setEvents] = useState([]);
	const { user } = useAuthState();

	useEffect(() => {
		getInvites();
		const interval = setInterval(getInvites, 5000);
		return () => clearInterval(interval);
	}, []);

	const getInvites = () => {
		axios
			.get(`${url}/users/${user.id}/invites`)
			.then(
				(response) => {
					setEvents(response.data);
					setLoading(false);
				},
				(error) => {
					setLoading(false);
					setError(true);
				}
			)
			.catch((error) => {
				setLoading(false);
				setError(true);
			});
	};

	return loading ? (
		<Alert variant="info">Dohvaćanje pozivnica...</Alert>
	) : error ? (
		<Alert variant="warning">Pozivnice trenutno nisu dostupne.</Alert>
	) : !events || events.length == 0 ? (
		<Alert variant="info">Nema pozivnica</Alert>
	) : (
		events.map((event, idx) => <Invite key={idx} event={event} />)
	);
};

export default InviteList;
