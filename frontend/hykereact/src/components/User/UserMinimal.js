import React from 'react';
import { ListGroup } from 'react-bootstrap';

export const UserMinimal = ({ id, firstName, lastName }) => {
	return (
		<ListGroup.Item key={id}>
			{firstName} {lastName}
		</ListGroup.Item>
	);
};
