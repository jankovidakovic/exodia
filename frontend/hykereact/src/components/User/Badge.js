import React, { useEffect } from 'react';
import { Card } from 'react-bootstrap';
import './User.css';

const Badge = ({ id, imageLocation, name, description }) => {
	if (id === undefined || imageLocation === undefined || name === undefined) {
		return null;
	}
	useEffect(() => {
		console.log('badge ', id, ' rendering!');
	}, []);

	return (
		<Card style={{ width: '18rem' }}>
				<img src={`https://hyke.ddns.net/assets/badges/${imageLocation}`} alt="Slika bedža" />
				<div className="centered">{name}</div>
				{description && <div className="centered">{description}</div>}
		</Card>
	);
};

export default Badge;
