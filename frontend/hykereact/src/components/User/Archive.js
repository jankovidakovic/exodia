import React, { useEffect, useState } from 'react';
import { Link, useParams, withRouter } from 'react-router-dom';
import axios from 'axios';
import { url } from '../../constants';
import { Button, Card, Col,Container,Row } from 'react-bootstrap';
import { useAuthState } from '../Auth/AuthContext';
import HikeMinimal from '../Hike/HikeMinimal';



const Archive = () => {
	const [state, setState] = useState({
		newEvent: '',
		archivedEvents: [],
		loading: true,
		error: '',
		message: '',
		hikes: [],
		addedHike: {}
	});
	const [loadingArchive, setLoadingArchive] = useState(true);
	const [archivedHikes, setArchivedHikes] = useState([]);
	const [error, setError] = useState(null);

	const auth = useAuthState();
	const [allHikes, setAllHikes] = useState([]);

	const { id } = useParams();

	const [whenChangedMount, setMount] = useState(0);

	function mountArchive() {
		axios.get(`${url}/users/${id}/archived-hikes`).then(
			(response) => {
				//console.log(response.data);
				if (response.status === 200) {
					console.log('archive fetched');
					//console.log(respons);
					setArchivedHikes(response.data);
					setLoadingArchive(false);
					setError("");
				}
			},
			(err) => {
				setError("Pogreška u arhivi, molimo pokušajte opet");
				console.log(err);
				//refresh page to look normal again
			}
		)
	}

	function mountHikes() {
		//var url1 = `${url}/hikes?publicity=1`;
		axios.get(`${url}/hikes?publicity=1`).then(
			(response) => {
				//console.log(response);
				if (response.status === 200) {
					console.log('archive fetched');
					//console.log(respons);
					setAllHikes(response.data);
				}
			},
			(err) => {
				setError("Pogreška u arhivi, molimo pokušajte opet");
				console.log(err);
				//showcase error to user
				//refresh page to look normal again
			}
		);
	}

	function addToArchive() {
		var url1 = `${url}/users/${id}/archived-hikes`;

		fetch(url1, {
			method: 'POST',
			body: state.addedHike
		}).then((res) => {
			if (res.status === 201) {
				setState((old) => ({
					...old,
					message: 'Uspjesno dodano'
				}));

			} else {
				setState((old) => ({
					...old,
					error: 'Nije dodano'
				}));
			}
		});
	}

	function addNew() {
		setState((old) => ({
			...old,
			newEvent: true
		}));
	}

	function doneAdding() {
		setState((old) => ({
			...old,
			newEvent: false
		}));
	}

	useEffect(() => {
		if (id == auth.user.id) { //
			mountArchive();
			//mountHikes();
		}

	}, [whenChangedMount]);

	function chosenHike(hike) {
		setState((old) => ({
			...old,
			addedHike: hike
		}));
	}


	if (id != auth.user.id) { // TODO refactor - remove id from url?
		return(
		<Container>
		 <Row>
			<Col lg = "6">Ne možete pregledati ovo</Col>
			<Col lg ="6">
			<Link to={`/users/${auth.user.id}/archived-hikes`}>
				<Button onClick={() => { setMount(whenChangedMount + 1); }}>Moja arhiva</Button>
			</Link>
			</Col>
			</Row>
			</Container>
		)
	}

	if (error && archivedHikes.length === 0) {
		return <Card><Col>{error}</Col></Card>
	}

	if (loadingArchive) {
		return <Card>Dohvaćanje arhive...</Card>
	}



	else return (
		<div>
			<Row> 
				<Col className="centered">{archivedHikes.length !== 0 ? "Vaši arhivirani izleti"
				: "Niste arhivirali nijedan izlet."}</Col>
				</Row>
				<Row>
				{archivedHikes.map((hike) => (
					<Col lg ="4" key={hike.hikeMinimalDTO.id + hike.archivedAt}>

						<HikeMinimal key={hike.hikeMinimalDTO.id} hike={hike.hikeMinimalDTO} />
					</Col>
				))}
				</Row>
		</div>
	);
};

export default Archive;
