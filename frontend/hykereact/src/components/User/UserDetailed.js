import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
	Accordion,
	Alert,
	Button,
	Card,
	Col,
	Container,
	Row,
	ListGroup
} from 'react-bootstrap';
import UpdateUserInfoForm from '../Form/UpdateUserInfoForm';
import ChangePasswordForm from '../Form/ChangePasswordForm';
import {
	Link,
	useHistory,
	useLocation,
	useParams,
	withRouter
} from 'react-router-dom';

import Badge from './Badge';
import ChangeRoleForm from '../Form/ChangeRoleForm';
import { useAuthDispatch, useAuthState } from '../Auth/AuthContext';
import { logout } from '../Auth/AuthReducer/actions';
import AlertDismissable from '../AlertDismissable';
import UserHikes from './UserHikes';
import Wishlist from '../Wishlist/Wishlist';
import { url } from '../../constants';
import { UserMinimal } from './UserMinimal';
import Request from '../Social/Request';
import UserEvents from '../Event/UserEvents';
import InviteList from './InviteList';
import "./User.css"

const UserDetailed = () => {
	const auth = useAuthState();
	const dispatch = useAuthDispatch();

	//TODO - fix stuff here if it doesn't work

	//TESTING THE REACT ROUTER PARAMS
	const { id } = useParams();

	const [data, setData] = useState({
		id: id, //id: auth.user.id
		firstName: '',
		lastName: '',
		username: '',
		email: '',
		role: null
	});

	const [friends, setFriends] = useState([]);
	const [friendRequests, setFriendRequests] = useState([]);
	const [renderAddFriend, setRenderAddFriend] = useState(false);
	const [errorAddFriend, setErrorAddFriend] = useState(false);
	const [added, setAdded] = useState(false);
	const [showAdded, setShowAdded] = useState(false);
	//TODO - implement a custom hook for fetchable resources
	//containing data, loading indicator and error indicator
	const [badges, setBadges] = useState([]);
	const [badgesLoading, setBadgesLoading] = useState(true);
	const [badgesError, setBadgesError] = useState(false);

	//PRIVATE STUFF
	const [wishlist, setWishlist] = useState([]);
	const [hikes, setHikes] = useState([]);

	const history = useHistory();

	//TODO - test on backend integration, seems to be working for now
	const deleteUser = () => {
		Axios.delete(`${url}/users/${data.id}`).then(
			(response) => {
				if (response.status === 204) {
					//TODO - change response status both on frontend and backend
					//console.log('Account deletion succesful');
					logout(dispatch);
					//showcase to user that the account has been deleted
					const location = {
						pathname: '/',
						state: {
							message: 'Korisnički račun uspješno obrisan.'
						}
					};
					history.push(location); //WORKS LMAO
				}
			},
			(err) => {
				console.log(err);
				//showcase error to user
				//refresh page to look normal again
			}
		);
	};

	const addFriend = () => {
		const FriendRequestDTO = {
			sender: auth.user.id,
			receiver: data.id
		};

		Axios.post(`${url}/users/${data.id}/add-friend`, FriendRequestDTO)
			.then(
				(response) => {
					//2xx responses only
					setAdded(true);
				},
				(error) => {
					setErrorAddFriend(true);
				}
			)
			.catch((error) => {
				setErrorAddFriend(true);
			});
	};

	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);

	useEffect(() => {
		setData((data) => ({ ...data, [id]: id }));
		//console.log('auth: ', auth);
		Axios.get(`${url}/users/${id}`).then(
			//was data.id or auth.user.id
			//was {auth.user.id}
			(response) => {
				setLoading(false);
				if (response.status === 200) {
					//ok
					setData({
						id: response.data.id,
						firstName: response.data.firstName,
						lastName: response.data.lastName,
						username: response.data.username,
						email: response.data.email,
						role: response.data.role
					});
					console.log(data);
					setError(false);
				} else {
					setError(true);
				}
			},
			(err) => {
				setError(true);
				setLoading(false);
				console.log(err);
			}
		);

		const friendsInterval = setInterval(() => {
			getFriends();
			getFriendRequests();
		}, 5000);

		//TODO - add requests for hikes, events, and all that
		//TODO - conditional requests depending on user context

		return () => {
			clearInterval(friendsInterval);
		};
	}, []);

	const getEvents = () => {
		//console.log('Fetching events...');
	};
	const getBadges = () => {
		//console.log('Fetching badges...');
		Axios.get(`${url}/users/${id}/badges`).then(
			(response) => {
				setBadgesLoading(false);
				if (response.status === 200) {
					setBadges(response.data);
					console.log('Badges fetches succesfully');
					setBadgesError(false);
				} else {
					console.log(response);
					setBadgesError(true);
					//TODO - more informative errors
				}
			},
			(err) => {
				setBadgesLoading(false);
				console.log(err);
				setBadgesError(true);
			}
		);
	};
	const getFriends = () => {
		//console.log('Fetching friends...');
		Axios.get(`${url}/users/${data.id}/friends`) //fetches friends of the profile owner
			.then(
				(response) => {
					setFriends(() => response.data);
					if (
						friends
							.map((friend) => friend.id)
							.indexOf(auth.user.id) === -1
					) {
						//user who is currently viewing the profile page is not friends with the profile owner
						setRenderAddFriend(() => true);
					}
				},
				(error) => {
					setErrorAddFriend(() => true);
				}
			)
			.catch((error) => {
				console.log(
					'Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovno kasnije'
				);
			});
	};
	const getFriendRequests = () => {
		//console.log('Fetching friend requests...');
		Axios.get(`${url}/users/${data.id}/friend-requests`)
			.then((response) => {
				//console.log('Fetching friend requests OK');
				setFriendRequests(() => response.data);
			})
			.catch((error) => {
				console.log('Fetching friend requests resulted in an error');
				console.log(error);
			});
	};
	const getArchive = () => {
		console.log('Fetching archive...');
	};

	useEffect(() => {
		//fetch public hikes of user
		getBadges();
		getEvents();
		getFriends();
		//fetch user created events

		if (auth.user.id === data.id) {
			console.log('User is viewing its own profile');
			getFriendRequests();

			getArchive();
			//fetch friends
			//fetch friend requests
		} else {
			//something?
			console.log('Other user is viewing the profile');
		}

		/* 
		friendsInterval.current = setInterval(() => {
			getFriends();
			getFriendRequests();
		}, 5000);
		*/
	}, [data]);

	useEffect(() => {
		if (auth.user.id === data.id) {
			setRenderAddFriend(false);
		} else if (
			friends.map((friend) => friend.id).indexOf(auth.user.id) === -1
		) {
			setRenderAddFriend(true);
		} else {
			setRenderAddFriend(false);
		}
	}, [friends]);

	useEffect(() => {
		if (
			auth.user.id !== data.id &&
			friendRequests.map((friend) => friend.id).indexOf(auth.user.id) !==
				-1
		) {
			setRenderAddFriend(false);
		}
	}, [friendRequests]);

	if (loading) {
		return (
			<Container>
				<Row>
					<Col>
						<Alert variant="info">Dohvaćanje podataka...</Alert>
					</Col>
				</Row>
			</Container>
		);
	} else if (error) {
		return (
			<Container>
				<Row>
					<Col>
						<Alert variant="warning">
							Došlo je do pogreške. Molimo, pokušajte ponovno
							kasnije.
							{
								//TODO - prebaciti poruke u file sa konstantama
							}
						</Alert>
					</Col>
				</Row>
			</Container>
		);
	} else {
		/*
		<ListGroup variant="flush">
							<ListGroup.Item>Cras justo odio</ListGroup.Item>
							<ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
							<ListGroup.Item>Vestibulum at eros</ListGroup.Item>
						</ListGroup>
						*/

		return (
			<>
				<Container>
					<Card style={{ width: '30rem' }} className="mb-2">
						<Card.Header>Korisnički podaci</Card.Header>

						<ListGroup variant="flush">
							<ListGroup.Item>
								<Row>
									<Col>ime: </Col>
									<Col>{data.firstName}</Col>
								</Row>
							</ListGroup.Item>
							<ListGroup.Item>
								<Row>
									<Col>prezime: </Col>
									<Col>{data.lastName}</Col>
								</Row>
							</ListGroup.Item>
							<ListGroup.Item>
								<Row>
									<Col>e-mail: </Col>
									<Col>{data.email}</Col>
								</Row>
							</ListGroup.Item>
							{auth.user.id === data.id && (
								<ListGroup.Item>
									<Row>
										<Col>korisničko ime: </Col>
										<Col>{data.username}</Col>
									</Row>
								</ListGroup.Item>
							)}
							<ListGroup.Item>
								<Row>
									<Col>aplikativna rola: </Col>
									<Col>{data.role}</Col>
								</Row>
							</ListGroup.Item>
						</ListGroup>
					</Card>
					{auth.user.id === data.id && (
						<Card className="mb-2">
							<Accordion>
								<Card>
									<Accordion.Toggle
										as={Card.Header}
										eventKey="0"
									>
										Ažuriranje osobnih podataka
									</Accordion.Toggle>
									<Accordion.Collapse eventKey="0">
										<Card.Body>
											<UpdateUserInfoForm
												id={data.id}
												firstName={data.firstName}
												lastName={data.lastName}
												username={data.username}
												email={data.email}
											/>
										</Card.Body>
									</Accordion.Collapse>
								</Card>
								<Card>
									<Accordion.Toggle
										as={Card.Header}
										eventKey="1"
									>
										Promjena lozinke
									</Accordion.Toggle>
									<Accordion.Collapse eventKey="1">
										<Card.Body>
											<ChangePasswordForm
												id={auth.user.id}
											/>
										</Card.Body>
									</Accordion.Collapse>
								</Card>
							</Accordion>
						</Card>
					)}
					{
						auth.user.role === 'ADMIN' &&
							auth.user.id !== data.id && ( //USER FOR TESTING ONLY, SHOULD BE ADMIN
								<ChangeRoleForm id={data.id} role={data.role} />
							) //only change roles to other users
					}

					{/*auth.user.id === data.id && data.role === 'DUTY' && (
						<DutyMountainHuts />
					)*/}
					{
						auth.user.id === data.id && (
							<>
								<>
									Lista želja:
									<Wishlist wishlist={wishlist} />
								</>
								<Link
									to={{
										pathname: `/users/${data.id}/archived-hikes`
									}}
								>
									<Button>Arhiva izleta</Button>
								</Link>
							</>
						) //TODO - add Archive
					}

					<Row className="pb-3">
						{errorAddFriend ? (
							<Alert variant="primary">
								Planinarska zajednica trenutno nije dostupna.
							</Alert>
						) : added ? (
							<AlertDismissable
								message={
									'Zahtjev za dodavanje uspješno poslan.'
								}
							/>
						) : (
							renderAddFriend && (
								<Col>
									<Button
										variant="primary"
										onClick={addFriend}
									>
										Dodaj na popis planinarske zajednice
									</Button>
								</Col>
							)
						)}
						{auth.user.id === data.id && (
							<Col>
								<Button variant="danger" onClick={deleteUser}>
									Obriši korisnički račun
								</Button>
							</Col>
						)}
					</Row>
				</Container>
				Bedževi:
				{badgesLoading ? (
					<Alert variant="primary">Dohvaćanje bedževa...</Alert>
				) : badgesError ? (
					<Alert variant="error">
						Došlo je do pogreške u dohvaćanju bedževa.
					</Alert>
				) : !badges || badges.length === 0 ? (
					<Alert variant="info">
						{data.firstName} {data.lastName} nema stečenih bedževa.
					</Alert>
				) : (
					badges.map((badge, idx) => (
						<Badge
							key={idx}
							id={idx}
							imageLocation={badge.fileSystemLocation}
							name={badge.name}
						/>
					))
				)}
				{auth.user.id === data.id && (
					<ListGroup>
						<br></br>
						<div>
						Popis vlastite planinarske zajednice:
						{friends.map((user) => (
							<Link
								to={{
									pathname: `/users/${user.id}`,
									state: {
										key: user.id
									}
								}}
								key={user.id}
							>
								<UserMinimal
									key={user.id}
									id={user.id}
									firstName={user.firstName}
									lastName={user.lastName}
								/>
							</Link>
						))}
						</div>
						<br></br>
						<div>
						Zahtjevi za prijateljstvom:
						{friendRequests.map((sender) => (
							<Request
								key={sender.id}
								sender={sender}
								replyEndpoint={`${url}/users/${auth.user.id}/friend-requests/reply`}
							/>
						))}
						</div>
					</ListGroup>
				)}
				<br></br>
				{auth.user.id === data.id && (
					<>
						Pozivnice za događaje:
						<InviteList />
					</>
				)}
				<br></br>
				Stvoreni izleti:
				{auth.user.id === data.id ? (
					<div className="userHikes">
					<UserHikes id={auth.user.id} />
					</div>
				) : (
					<div className="userHikes">
					<UserHikes
						id={data.id}
						conditions={[(hike) => hike.publicity]}
					/>
					</div>
				)}
				{auth.user.id === data.id ? (
					<UserEvents userId={auth.user.id} />
				) : (
					<UserEvents
						userId={data.id}
						//conditions={[(hike) => hike.publicity]}
					/>
				)}
			</>
		);
	}
};

export default withRouter(UserDetailed);
