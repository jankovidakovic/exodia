import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { url } from '../../constants';
import { useAuthState } from '../Auth/AuthContext';
import HikeList from '../Hike/HikeList';
import NewHikeButton from '../Hike/NewHikeButton';
import {Row} from "react-bootstrap";
import "./User.css"

const UserHikes = ({ id, conditions }) => {
	const [hikes, setHikes] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState('');

	useEffect(() => {
		axios
			.get(`${url}/hikes?creatorId=${id}`)
			.then(
				(response) => {
					setHikes(response.data);
					setLoading(false);
				},
				(error) => {
					setError(
						'Izleti trenutno nisu dostupni. Molimo, pokušajte ponovno kasnije.'
					);
				}
			)
			.catch((error) => {
				setError(
					'Izleti trenutno nisu dostupni. Molimo, pokušajte ponovno kasnije.'
				);
			});
	}, [id]);

	//TODO - ADD SOME STYLE HERE
	return (
		<div>
		<Row>
			<div className="centered">
			<NewHikeButton />
			</div>
			</Row>
			<Row>
			<HikeList
				hikes={hikes}
				loading={loading}
				error={error}
				conditions={conditions}
			/>
			</Row>
		</div>
	);
};

export default UserHikes;
