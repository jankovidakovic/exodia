import { Button } from 'bootstrap';
import { Card, Col } from 'react-bootstrap';
import React from 'react';
import Axios from 'axios';
import { url } from '../constants';

//					user id
const VisitRequest = ({
	mountainHutId, //TODO - use context for this
	userId,
	firstName,
	lastName, //TODO - use context for all of these
	status
}) => {
	//TODO - return UserMinimal which is clickable and takes ID from props

	const accept = () => {
		Axios.post(`${url}/mountain-huts/${mountainHutId}/visits`, {
			user: {
				id: userId,
				firstName: firstName,
				lastName: lastName
			},
			status: 'ACCEPTED' //TODO - check if this is correct
		})
			.then(
				(response) => {
					//remove oneself from the visit request list
				},
				(err) => {
					console.log(err);
				}
			)
			.catch((err) => {
				console.log(err);
			});
	};

	const reject = () => {
		Axios.post(`${url}/mountain-huts/${mountainHutId}/visits`, {
			user: {
				id: userId,
				firstName: firstName,
				lastName: lastName
			},
			status: 'REJECTED' //TODO - check if this is correct
		})
			.then(
				(response) => {
					//remove oneself from the visit request list
				},
				(err) => {
					console.log(err);
				}
			)
			.catch((err) => {
				console.log(err);
			});
	};
	return (
		<Card>
			<Card.Body>
				<Card.Text>
					<Col xs={2}>
						{firstName} {lastName}
					</Col>
				</Card.Text>
				{status === 'PENDING' && (
					//TODO - check if those Col tags are working correctly
					<>
						<Col>
							<Button variant="success" onClick={accept}>
								Potvrdi
							</Button>
						</Col>
						<Col>
							<Button variant="danger" onClick={reject}>
								Odbij
							</Button>
						</Col>
					</>
				)}
			</Card.Body>
		</Card>
	);
};

export default VisitRequest;
