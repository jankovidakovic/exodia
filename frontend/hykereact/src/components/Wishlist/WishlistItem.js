import axios from 'axios';
import React, { useState } from 'react';
import { Alert, Button, Col, Container, Row } from 'react-bootstrap';
import { url } from '../../constants';
import { useAuthState } from '../Auth/AuthContext';
import { Link } from 'react-router-dom';

const WishlistItem = ({ item, type, dto, removeItem }) => {
	const { user } = useAuthState();
	const [show, setShow] = useState(true);
	const [error, setError] = useState('');

	const deleteItem = () => {
		console.log(item.id);
		axios
			.delete(`${url}/users/${user.id}/wishlist`, { data: dto })
			.then(
				(response) => {
					setShow(false);
					removeItem();
				},
				(error) => {
					setError(
						'Došlo je do pogreške. Molimo, pokušajte ponovno kasnije.'
					);
				}
			)
			.catch((error) => {
				setError(
					'Došlo je do pogreške. Molimo, pokušajte ponovno kasnije.'
				);
			});
	};
	if (error) {
		return (
			<Alert variant="info">{error}</Alert>
		)
	} else if (type === "hike") {
		return (
			<Col lg="4">
				<div className="hikeContainer" key={item.id}>
					<div>Naziv: {item.name}</div>
					<div>Lokacija: {item.location.name}</div>
					<div>Duljina staze: {item.length}</div>
					<div>
						Stvorio korisnik:{' '}
						{item.creator ? (
							<>
								{item.creator.firstName} {item.creator.lastName}
							</>
						) : (
								'IZBRISAN'
							)}
					</div>
					<div className="centered">
						<Row>
							<Col>
								<Link to={'/hikes/' + item.id} className="browserLink">
									<Button variant="success">
										Detalji
							</Button>
								</Link>
							</Col>
							<Col>
								<Button variant="danger" onClick={deleteItem}>
									Izbriši
							</Button>
							</Col>
						</Row>
					</div>
				</div>
			</Col>
		)
	} else {
		return (
			<Col lg="4">
				<div className="hutContainer" key={item.id}>
					<div>Naziv: {item.name}</div>
					<div>Lokacija: {item.location.name}</div>
					<div className="button">
						<Row>
							<Col>
								<Link to={`/mountain-huts/${item.id}`} key={item.id} className="link">
									<Button className="link" variant="success">
										Detalji
								</Button>
								</Link>
							</Col>
							<Col>
								<Button variant="danger" onClick={deleteItem}>
									Izbriši
							</Button>
							</Col>
						</Row>
					</div>
				</div>
			</Col>
		)
	}
	/*
	return error ? (
		<Alert variant="info">{error}</Alert>
	) : (
		show && item && (
			<Alert variant="secondary" show={show}>
				<Container>
					<Row>
						<Col className="d-flex justify-content-start">
							{item.label}
						</Col>
						<Col className="d-flex justify-content-end">
							<Button variant="danger" onClick={deleteItem}>
								Izbriši
							</Button>
						</Col>
					</Row>
				</Container>
			</Alert>
		)
	);
	*/
};

export default WishlistItem;
