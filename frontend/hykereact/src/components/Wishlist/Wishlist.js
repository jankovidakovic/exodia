import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container, Alert } from 'react-bootstrap';
import { errorMessage, url } from '../../constants';
import { useAuthState } from '../Auth/AuthContext';
import WishlistItem from './WishlistItem';
import {Row} from "react-bootstrap";

const Wishlist = () => {
	const { user } = useAuthState();
	const [wishlist, setWishlist] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);

	const removeItem = (item) => {
		setWishlist((oldWishlist) =>
			oldWishlist.filter((wishlistItem) => wishlistItem !== item)
		);
	};

	useEffect(() => {
		console.log('wishlist items: ', wishlist.length);
	}, [wishlist.length]);

	useEffect(() => {
		axios
			.get(`${url}/users/${user.id}/wishlist`)
			.then(
				(response) => {
					setWishlist(response.data);
					setLoading(false);
				},
				(error) => {
					setError(true);
				}
			)
			.catch((error) => {
				setError(true);
			});
	}, []);

	return error ? (
		<Alert variant="info">{errorMessage}</Alert>
	) : loading ? (
		<Alert variant="info">Učitavanje liste želja...</Alert>
	) : !wishlist || wishlist.length === 0 ? (
		<Alert variant="info">Nema stavki na listi želja.</Alert>
	) : (
		<Container>
			<Row>
			{wishlist.map((item) =>
			
				<WishlistItem 
				key={item.hike ? (item.hike.id) : (item.mountainHut.id)} 
				item = {item.hike ? (item.hike) : (item.mountainHut)}
				type = {item.hike ? ("hike") : ("hut")}
				dto = {item.hike ? ({hikeId:item.hike.id,
									mountainHutId:null}) 
									: ({hikeId:null,
										mountainHutId:item.mountainHut.id})} 
				removeItem={() => removeItem(item)}/>
				
			)}
			</Row>
		</Container>
	);
};

export default Wishlist;
