import React from 'react';
import "./Browser.css";
import { Link } from "react-router-dom";


//With this we create the browser track that is shown under the header of the page, it contains links to different sites of our web application.
function Browser() {
    return (
        <table className="dhomepage-flex justify-content-start table table-bordered browserContainer">
            <tbody>
                <tr>
                    <th scope="col" style={{ borderColor: "#855723" }}>
                        <Link to='/' className="browserLink">Naslovna</Link>
                    </th>
                    <th scope="col" style={{ borderColor: "#855723" }}>
                        <Link to='/hikes' className="browserLink">Planinarski izleti</Link>
                    </th>
                    <th scope="col" style={{ borderColor: "#855723" }}>
                        <Link to='/login' className="browserLink">Prijava</Link>
                    </th>
                    <th scope="col" style={{ borderColor: "#855723" }}>
                        <Link to='/registration' className="browserLink">Registracija</Link>
                    </th>
                    <th scope="col" style={{ borderColor: "#855723" }}>
                        <Link to='/wishlist' className="browserLink">Lista zelja</Link>
                    </th>
                </tr>
            </tbody>
        </table>

    )
}


export default Browser