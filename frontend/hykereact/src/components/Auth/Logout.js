import React from 'react';
import { Button } from 'react-bootstrap';
import { useAuthDispatch } from './AuthContext';
import { logout } from './AuthReducer/actions';

const Logout = () => {
	const dispatch = useAuthDispatch();
	return <Button variant="danger" onClick={() => logout(dispatch)}>Odjava</Button>;
};

export default Logout;
