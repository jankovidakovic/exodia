import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useAuthState } from './AuthContext';

const GuestRoute = ({ component: Component, path, ...rest }) => {
	const { authenticated } = useAuthState();
	return (
		<Route
			path={path}
			exact
			render={(props) =>
				authenticated ? (
					<Redirect to={{ pathname: '/' }} />
				) : (
					<Component {...props} />
				)
			}
		></Route>
	);
};

export default GuestRoute;
