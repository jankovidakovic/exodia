import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useAuthState } from './AuthContext';

const ProtectedRoute = ({ component: Component, path, ...rest }) => {
	const { authenticated } = useAuthState();
	return (
		<Route
			path={path}
			exact
			render={(props) =>
				authenticated ? (
					<Component {...props} />
				) : (
					<Redirect to={{ pathname: '/login' }} />
				)
			}
		></Route>
	);
};

export default ProtectedRoute;
