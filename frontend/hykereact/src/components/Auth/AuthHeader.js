import React from 'react';
import { Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useAuthState } from './AuthContext';
import Logout from './Logout';

import './AuthHeader.css';

const AuthHeader = () => {
	const auth = useAuthState();
	return (
		<Container>
			{auth.authenticated ? (
				<>
					<div className="headerRow">
						<Row>
							<Col>
								<Link to={`/profile/${auth.user.id}`}>
									<Button variant="success">
										{auth.user.firstName}{' '}
										{/*auth.user.lastName*/}
									</Button>
								</Link>
							</Col>
							<Col>
								<Logout />
							</Col>
						</Row>
					</div>
				</>
			) : (
				<>
					<div className="headerRow">
						<Row>
							<Col>
								<Link to="/register">
									<Button>Registracija</Button>
								</Link>
							</Col>
							<Col>
								<Link to="/login">
									<Button>Prijava</Button>
								</Link>
							</Col>
						</Row>
					</div>
				</>
			)}
		</Container>
	);
};

export default AuthHeader;
