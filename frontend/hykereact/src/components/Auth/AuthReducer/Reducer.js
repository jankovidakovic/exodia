import {
	useHistory,
	withRouter
} from 'react-router-dom/cjs/react-router-dom.min';
import { AuthProvider } from '../AuthContext';

let user = localStorage.getItem('currentUser')
	? JSON.parse(localStorage.getItem('currentUser')).user
	: '';
let token = localStorage.getItem('currentUser')
	? JSON.parse(localStorage.getItem('currentUser')).token
	: '';

let authenticated = localStorage.getItem('authenticated')
	? JSON.parse(localStorage.getItem('authenticated'))
	: false;

export const initialState = {
	authenticated: authenticated,
	user: user,
	token: token,
	loading: false, //TODO - maybe remove
	errorMessage: null //TODO - maybe remove
};

export const AuthReducer = (initialState, action) => {
	switch (action.type) {
		case 'REQUEST_LOGIN':
			console.log('REQUEST_LOGIN');
			return {
				...initialState,
				loading: true
			};
		case 'LOGIN_SUCCESS':
			console.log('LOGIN_SUCCESS');
			return {
				...initialState,
				authenticated: true,
				user: action.payload.user,
				token: action.payload.token,
				loading: false
			};
		case 'LOGOUT':
			console.log('LOGOUT');
			return {
				...initialState,
				authenticated: false,
				user: '',
				token: ''
			};
		case 'LOGIN_ERROR':
			console.log('LOGIN_ERROR');
			return {
				...initialState,
				loading: false,
				errorMessage: action.error
			};
		default:
			throw new Error(`Unhandled action type: ${action.type}`);
	}
};
