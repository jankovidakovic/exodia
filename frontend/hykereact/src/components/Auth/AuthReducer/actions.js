import Axios from 'axios';
import jwt_decode from 'jwt-decode';
import { url } from '../../../constants';

export const loginUser = (loginPayload, dispatch, loginFeedback) => {
	dispatch({
		type: 'REQUEST_LOGIN'
	});
	Axios.post(`${url}/login`, loginPayload)
		.then(
			(response) => {
				//only corresponds to 2xx responses
				console.log('200 ree');
				console.log(response);
				//decode jwt
				const jwt = response.headers.authorization.split(' ')[1];
				loginFeedback("");
				const jwt_decoded = jwt_decode(jwt);
				console.log(jwt); //WORKS
				const authedUser = {
					firstName: jwt_decoded.firstName,
					lastName: jwt_decoded.lastName,
					username: jwt_decoded.username,
					email: jwt_decoded.email,
					role: jwt_decoded.role,
					id: jwt_decoded.id
				};

				dispatch({
					type: 'LOGIN_SUCCESS',
					payload: {
						user: authedUser,
						token: jwt
					}
				}); //updates the context state
				localStorage.setItem(
					//TODO - fix when integrated with backend
					'currentUser',
					JSON.stringify({
						user: authedUser,
						token: jwt
					})
				);
				localStorage.setItem('authenticated', true);
			},
			(error) => {
				//corresponds to non-2xx responses and client side errors
				//handle client-side error
				console.log(error);
				if (error.response) {
					console.log(error.response);
					if (error.response.status === 401) {
						//unauthorized
						dispatch({
							type: 'LOGIN_ERROR',
							error: 'Pogrešna lozinka!'
						});
						loginFeedback('Pogrešna lozinka!');
					} else if (error.response.status === 404) {
						//non-existing user
						dispatch({
							type: 'LOGIN_ERROR',
							error:
								'Ne postoji korisnički račun s tim korisničkim imenom.'
						});
						loginFeedback('Ne postoji korisnički račun s tim korisničkim imenom.');
					} else {
						//some other error
						dispatch({
							type: 'LOGIN_ERROR',
							error:
								'Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.'
						});
						loginFeedback('Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.');
					}
				} else if (error.request) {
					//client side error, request didnt go through
					dispatch({
						type: 'LOGIN_ERROR',
						error:
							'Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.'
					});
					loginFeedback('Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.');
				} else {
					//some other unexpected error
					dispatch({
						type: 'LOGIN_ERROR',
						error:
							'Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.'
					});
					loginFeedback('Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.');
				}
			}
		)
		.catch((error) => {
			console.log('bruh');
			console.log(error);
			dispatch({
				type: 'LOGIN_ERROR',
				error: 'Došlo je do pogreške. Molimo, pokušajte ponovo kasnije.'
			});
			loginFeedback('Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovo kasnije.');
			return error;
		});
};

export const logout = (dispatch) => {
	dispatch({ type: 'LOGOUT' });
	localStorage.removeItem('currentUser');
	localStorage.removeItem('authenticated');
};
