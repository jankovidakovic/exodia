import { AuthReducer, initialState } from './AuthReducer/Reducer';

import { React, createContext, useContext, useReducer } from 'react';
import "./AuthContext.css"

const AuthStateContext = createContext();
const AuthDispatchContext = createContext();

/**
 * Custom hook for using the authentication state context, from which one has access to the information regarding
 * the user who is currently signed-in.
 */
export function useAuthState() {
	const context = useContext(AuthStateContext);
	if (context === undefined) {
		throw new Error('useAuthState must be used within a AuthProvider');
	}
	return context;
}

export function useAuthDispatch() {
	const context = useContext(AuthDispatchContext);
	if (context === undefined) {
		throw new Error('useAuthDispatch must be used within a AuthProvider');
	}
	return context;
}

export const AuthProvider = ({ children }) => {
	const [auth, dispatch] = useReducer(AuthReducer, initialState);

	//context will have the reducer state as the value
	return (
		<AuthStateContext.Provider value={auth}>
			<AuthDispatchContext.Provider value={dispatch}>
				<div className="app">
					{children}
				</div>
			</AuthDispatchContext.Provider>
		</AuthStateContext.Provider>
	);
};
