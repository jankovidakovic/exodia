import React from 'react';
import './Header.css';
import headerImage from './Images/headerImage.jpg';

console.log(headerImage);

//This function creates the header of the web application and for now is used only in estetic purposes.
function Header() {
	return <img src={headerImage} className="headerImage"></img>;
}

export default Header;
