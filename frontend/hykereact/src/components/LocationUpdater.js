//import { render } from '@testing-library/react';
import React from 'react';
import "./Registration.css"
import { Link } from 'react-router-dom';

function LocationUpdater(props) {
    //With this we define a setter for location form data
    const [locationForm, setLocationForm] = React.useState({
        locName: "",
        locDescription: "",
    });
    //Defines error and setter for errors
    const [error, setError] = React.useState("");
    //and for other messages
    const [message, setMessage] = React.useState("");

    //Defines what happens if there is a change on the site, it updates old location form data with new ones.
    function onChange(event) {
        //setError("");
        setMessage("");
        const { name, value } = event.target;
        setLocationForm(oldForm => ({ ...oldForm, [name]: value }));
    }


    //const enableButton = false;

    //Defines what happends after we press the create button, when the submit happens and how we handle it
    function onSubmit(e) {
        //We prevent the default submit method that places the data in the http link
        e.preventDefault();
        setError("");
		setMessage("");
        //Sets data that we will send over to backend
        const locData = {
            name: locationForm.locName,
            description: locationForm.locDescription
        }
        //If somebody tries to u location without filling all inputs we send the coresponding error
        if (locationForm.locName === "" || locationForm.locDescription === "") {
            setError("Svi podatci moraju biti ispunjeni!");
            return;
        }
        
        
        //Sets the method that will be used in backend
        const options = {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            //Data converts to JSON format for easier backend usage
            body: JSON.stringify(locData)
        };
		setMessage("Pokušavam ažuriranje...");

        //Fetches the backend registration fucntion and sends the needed data
        fetch('https://hyke.ddns.net/api/locations', options)
            .then(response => {
				setMessage("");
                if (response.status === 200) {
                    setMessage("Lokacija uspješno ažurirana");
                    setLocationForm({locName:"",locDescription:""});
                } else if (response.status === 404) {
                    setError("Takva lokacija ne postoji");
                }
                else {
                    setError("Lokacija nije stvorena");
                }
            }
            )
    }

    //Returns the builded page for creating a location
    return (
        <div className="inputsDiv">
            <h1 className="login">Ažuriranje lokacije</h1>
            <form className="centeredContainer" onSubmit={onSubmit}>


                <div className="inputsBox">
                    <div>
                        <label>
                            Naziv lokacije koju treba ažurirati
                            </label>
                    </div>
                    <div>
                        <input name="locName" onChange={onChange} value={locationForm.locName}></input>
                    </div>
                </div>

                <div className="inputsBox">
                    <div>
                        <label>
                            Opis
                            </label>
                    </div>
                    <div>
                        <input name="locDescription" onChange={onChange} value={locationForm.locDescription}></input>
                    </div>
                </div>
                
                
                <div className="centeredContainer">
                    
                    {error === "" ?
                        message === "" ? <div></div> : <div>{message}</div>
                        :
                        <div className="error">{error}</div>
                    }
                </div>
                

                <div className="centeredContainer">
                    <button className="centeredContainer">Ažuriraj lokaciju
                    </button>
                </div>
                
                <div className="centeredContainer">

                    <label> Želite li pregledati postojeće lokacije? 
                        <Link to='/locations'> Sve lokacije </Link></label>
                </div>
                <div className="centeredContainer">

                    <label> Želite li dodati novu lokaciju? 
                        <Link to='/locations/add'> Stvaranje lokacija </Link></label>
                </div>
                
            </form>
        </div>
    );
}

export default LocationUpdater;
