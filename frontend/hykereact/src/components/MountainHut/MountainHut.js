import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams, withRouter } from 'react-router-dom';
import { useAuthState } from '../Auth/AuthContext';
import Axios from 'axios';
import './MountainHut.css';
import AppRouter from '../Nav/AppRouter';
import { url } from '../../constants';

import { Card, Col, Button, Row, Container } from 'react-bootstrap';
import Request from '../Social/Request';
import LoginPrompt from '../LoginPrompt';

const MountainHut = () => {
	// TODO maybe option to update

	const auth = useAuthState();
	const { id } = useParams();

	const [state, setState] = useState({
		locations: [],
		loading: true,
		error: '',
		message: '',
		visitReqs: []
	});

	const [mountainHut, setMountainHut] = useState({
		id: '',
		name: '',
		description: '',
		location: {
			id: '',
			name: '',
			description: ''
		},
		food: false,
		water: false,
		sleepover: false,
		electricity: false
	});

	const [message, setMessage] = React.useState('');
	const [error, setError] = React.useState('');

	const [update, setUpdate] = useState(false);
	const [updateForm, setUpdateForm] = React.useState({
		updName: '',
		updLoc: '',
		updDesc: ''
	});
	useEffect(() => {
		Axios.get(`${url}/mountain-huts/${id}`)
			.then((response) => {
				setMountainHut(response.data);
			})
			.catch((e) => {
				console.log(e);
				setState((oldState) => ({
					...oldState,
					error: 'Ucitavanje doma nije uspjelo'
				}));
				return e;
			});

		let interval;
		if (auth.user && auth.user.role === 'DUTY') {
			getVisitRequests();
			interval = setInterval(getVisitRequests, 5000);
		}
		return () => {
			if (interval) clearInterval(interval);
		};
	}, []);

	const addToWishlist = () => {
		console.log('adding to wishlist');
		Axios.post(`${url}/users/${auth.user.id}/wishlist`, {
			mountainHutId: id
		})
			.then(
				(response) => {
					console.log('GOT RESPONSE');
					if (response.status === 201) {
						// feedback for developer
						console.log('hut added to wishlist!');

						setMessage('Dom dodan na vašu listu želja!');
					}
					console.log(response);
				},
				(error) => {
					console.log('wishlist add failed with non-2xx');
					console.log(error);

					if (error.response.status === 409) {
						setMessage(
							'Ovaj planinarski dom je već na vašoj listi želja'
						);
					} else {
						setError('Pogreška u dodavanju izleta');
					}
				}
			)
			.catch((e) => {
				console.log('wishlist adding failed unexpectedly');
				console.log(e);
				return e;
			});
	};

	const getVisitRequests = () => {
		Axios.get(`${url}/mountain-huts/${id}/visits`)
			.then((response) => {
				console.log(response.data);
				setState((oldState) => ({
					...oldState,
					visitReqs: response.data
				}));
			})
			.catch((e) => {
				console.log(e);
				/*
					setState((oldState) => ({
						...oldState,
						error: 'Ucitavanje doma nije uspjelo'
					}));
					*/
				return e;
			});
	};

	function onChange(event) {
		const { name, value } = event.target;

		setState((oldState) => ({
			...oldState,
			[name]: value
		}));
	}

	function onSubmit(e) {
		e.preventDefault();
		console.log('Pokusavam azurirati dom');
		if (
			state.updName === '' ||
			state.updDesc === '' ||
			state.updLoc === ''
		) {
			setState((oldState) => ({
				...oldState,
				error: 'Unesite sve podatke'
			}));
			return;
		}
		const hutForm = {
			name: state.updName,
			locName: state.updLoc,
			desc: state.updDesc
		};
		const options = {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(hutForm)
		};
		setState((oldState) => ({
			...oldState,
			message: 'Pokušavam ažurirati dom...'
		}));

		fetch(`/mountain-huts/${id}`, options).then((response) => {
			if (response.status === 200) {
				alert('Dom uspješno ažuriran');
				setUpdateForm({
					updName: '',
					updDesc: '',
					updLoc: ''
				});
				setUpdate(false);
				//TODO - implement reload ??
			} else {
				setState((oldState) => ({
					...oldState,
					error: 'Dom nije azuriran'
				}));
			}
		});
	}

	function onActivate(event) {
		setUpdate(true);
	}

	function deleteHut() {
		const url = `/mountain-huts/${id}`;
		const options = {
			method: 'DELETE'
		};
		setState((oldState) => ({
			...oldState,
			message: 'Pokušavam izbrisati dom...'
		}));
		//Fetches the backend registration fucntion and sends the needed data
		fetch(url, options).then((response) => {
			if (response.status === 200) {
				//TODO - check this
				setState((oldState) => ({
					...oldState,
					message: 'Dom uspješno izbrisan!'
				}));
			} else {
				setState((oldState) => ({
					...oldState,
					error: 'Dom nije izbrisan!'
				}));
			}
		});
	}

	function askForVisit() {
		setMessage('Zatražujem potvrdu...');
		Axios.post(`${url}/mountain-huts/${id}/request-visit`, {
			id: auth.user.id
		}).then(
			(res) => {
				setMessage('Potvrda zatražena.');
			},
			(error) => {
				setError('Traženje potvrde nije uspjelo.');
			}
		);
	}

	const requests =
		auth.user && auth.user.role === 'DUTY' ? (
			state.visitReqs.length ? (
				state.visitReqs.map((visitRequest) => (
					<Request
						sender={visitRequest.user}
						key={visitRequest.user.id}
						replyEndpoint={`${url}/mountain-huts/${id}/visits`}
					/>
				))
			) : (
				<Card>Za ovaj dom nema zahtjeva o potvrdi posjeta</Card>
			)
		) : (
			<div></div>
		);

	if (update) {
		return (
			<Card>
				<label>{state.hutName}</label>
				<input onChange={onChange} value={updateForm.updName}></input>
				<label>{state.hutDescription}</label>
				<input onChange={onChange} value={updateForm.updDesc}></input>
				<label>{state.hutLocation.name}</label>

				<div>
					{state.locations.map((location, idx) => (
						<div className="" key={idx}>
							{' '}
							<label>{location.name}</label>
							<input
								type="radio"
								onChange={onChange}
								value={updateForm.updLoc}
								name="locations"
							></input>
						</div>
					))}
				</div>
				<button onClick={onSubmit}>Ažuriraj</button>
				<button onClick={deleteHut}>Izbrisi planinarski dom</button>
				<div className="centeredContainer">
					{state.error === '' ? (
						state.message === '' ? (
							<div></div>
						) : (
							<div>{state.message}</div>
						)
					) : (
						<div className="error">{state.error}</div>
					)}
				</div>
			</Card>
		);
	} else {
		return (
			<div className="container">
				{auth.authenticated ? (
					<Row>
						<Col className="row" lg="6">
							<div className="gore">
								{' '}
								Posjetili ste dom?
								<div className="space"></div>
								<br></br>
								<Button variant="success" onClick={askForVisit}>
									Zatraži potvrdu dolaska
								</Button>
							</div>
						</Col>

						<Col className="row" lg="6">
							<div className="gore">
								{' '}
								Planirate posjetiti?
								<div className="space"></div>
								<br></br>
								<Button
									variant="primary"
									onClick={addToWishlist}
								>
									Dodaj na listu želja
								</Button>
							</div>
						</Col>
					</Row>
				) : (
					<LoginPrompt />
				)}
				<div className="centeredContainer">
					{error === '' ? (
						message === '' ? (
							<div></div>
						) : (
							<div>{message}</div>
						)
					) : (
						<div className="error">{error}</div>
					)}
				</div>
				<br></br>
				<Row>
					<Col lg="3">
						<label className="important">Ime: </label>
					</Col>{' '}
					<Col>{mountainHut.name}</Col>
				</Row>
				<br></br>
				<Row>
					<Col lg="3">
						<label className="important">Lokacija:</label>
					</Col>
					<Col>{mountainHut.location.name}</Col>
				</Row>
				<Row>
					<Col lg="3">
						<label className="important">O lokaciji:</label>
					</Col>
					{mountainHut.location.description ? (
						<Col>{mountainHut.location.description}</Col>
					) : (
						<div></div>
					)}
				</Row>
				<br></br>
				<Row>
					<Col lg="3">
						<label className="important">Opis:</label>{' '}
					</Col>{' '}
					<Col>{mountainHut.description}</Col>
				</Row>
				<br></br>
				<Row>
					<Col lg="3">
						<label className="important">Pitka voda: </label>
					</Col>{' '}
					<Col>
						{mountainHut.water ? (
							<label className="yes">DA</label>
						) : (
							<label className="no">NE</label>
						)}
					</Col>
				</Row>
				<Row>
					<Col lg="3">
						<label className="important">Hrana: </label>
					</Col>{' '}
					<Col>
						{mountainHut.food ? (
							<label className="yes">DA</label>
						) : (
							<label className="no">NE</label>
						)}
					</Col>
				</Row>
				<Row>
					<Col lg="3">
						<label className="important">Struja: </label>
					</Col>{' '}
					<Col>
						{mountainHut.electricity ? (
							<label className="yes">DA</label>
						) : (
							<label className="no">NE</label>
						)}
					</Col>
				</Row>
				<Row>
					<Col lg="3">
						<label className="important">Mogućnost noćenja:</label>
					</Col>{' '}
					<Col>
						{mountainHut.sleepover ? (
							<label className="yes">DA</label>
						) : (
							<label className="no">NE</label>
						)}
					</Col>
				</Row>

				{requests}
			</div>
		);
	}
};

export default MountainHut;
