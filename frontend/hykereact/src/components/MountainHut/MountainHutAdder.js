import React, { useEffect, useState } from 'react';
import '../Auth/Registration.css';
import { Link } from 'react-router-dom';
import Axios from 'axios';




const MountainHutAdder = () => {
	const [state, setState] = useState({
		hutName: '',
		hutDescription: '',
		hutLocation: '',
	});

	const [message, setMessage] = useState('');
	const [error, setError] = useState('');
	const [locations, setLocations] = useState({
		locations:[],
		loading:true
	});
	function onChange(event) {
		const { name, value } = event.target;
		setState((oldForm) => ({ ...oldForm, [name]: value }));
	}

	function onChoose(location) {
		setState((oldForm) => ({ ...oldForm, hutLocation:location }));

	}

	function onSubmit(e) {
		e.preventDefault();
		setError("")
		setMessage("")
		if (
			state.hutName === '' ||
			state.hutDescription === '' ||
			state.hutLocation === ''
		) {
			setError('Svi podatci moraju biti ispunjeni!' );
			return;
		}

		const newHut = {
			name: state.hutName,
			desc: state.hutDescription,
			loc: state.hutLocation
		};
		setMessage('Pokusavam unijeti planinarski dom u sustav...');

		Axios.post("/mountain-huts",newHut).then((res) =>  {
			setMessage("Uspješno stvoren dom");
			setState({
				hutName: '',
				hutDescription: '',
				hutLocation:null
			});
		},(error) => {
			console.log(error);
			setError("Dom se nije stvorio.")
		});
	}
	useEffect(() => {
		Axios.get('/locations')
			.then((res) => {
					setLocations({locations:res.data});
				})
			.catch((err) => {
				console.log(err);
			});
	}, []);


	if (locations.loading) {
		return (
			<div>Loading locations...</div>
		)
	} else {
		return (
			<div className="inputsDiv">
				<h1 className="login">Dodavanje planinarskog doma</h1>
				<form className="centeredContainer" onSubmit={onSubmit}>
					<div className="inputsBox">
						<div>
							<label>Naziv planinarskog doma</label>
						</div>
						<div>
							<input
								name="hutName"
								onChange={onChange}
								value={state.hutName}
							></input>
						</div>
					</div>
					<div className="inputsBox">
						<div>
							<label>Opis</label>
						</div>
						<div>
							<input
								name="hutDescription"
								onChange={onChange}
								value={state.hutDescription}
							></input>
						</div>
					</div>
					<div>
						Lokacija:
					{locations.locations.map((location, idx) => (
						<div className="" key={idx}>
						<label>{location.name}</label>
							<input
								type="radio"
								onChange={onChange}
								value={state.hutLocation}
								name="locations"
								onClick={onChoose(location)}
							>
							</input>
						</div>
					))}
					</div>
					<div className="centeredContainer">
						{/* {error === "" ?
                                message === "" ? <div></div> : <div>{message}</div>
                                :
                                <div className="error">{error}</div>
                            } */}
					</div>{' '}
				<div className="centeredContainer">
						<button className="centeredContainer">
							Dodaj planinatski dom
					</button>
					</div>
					<div className="centeredContainer">
						<label>
							{' '}
						Želite li pregledati postojeće planinarske domove?
						<Link to="/mountain-huts">Svi domovi</Link>
						</label>
					</div>
				</form>
			</div>
		)
						}
};

export default MountainHutAdder;
