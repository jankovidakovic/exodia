import React, { useEffect, useState } from 'react';
import './MountainHuts.css';
import { Link, withRouter } from 'react-router-dom';
import Axios from 'axios';
import {
	Button,
	Col,
	Card,
	Container,
	Form as BSForm,
	Row
} from 'react-bootstrap';
import { url } from '../../constants';
import { Field, Form, Formik } from 'formik';

const MountainHuts = () => {
	const [state, setState] = useState({
		loading: true,
		huts: []
	});

	//const [filteredHuts, setFilteredHuts] = useState([]);
	const [allHuts, setAllHuts] = useState([]);
	const [message, setMessage] = useState('');

	useEffect(() => {
		Axios.get(`${url}/mountain-huts`)
			.then((res) => {
				//console.log(res);
				setState({
					loading: false,
					huts: res.data
				});
				setAllHuts(res.data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);
	//console.log(state.huts);

	const filterArea = (
		<Col>
			<Card>
				Filtriranje
				<Formik
					initialValues={{
						toggle: false,
						requirements: []
					}}
					onSubmit={async (values) => {
						if (values.requirements.length === 0) {
							setMessage(
								'Ništa nije označeno. Prikazuju se svi domovi.'
							);
							setState({ huts: allHuts });
							return;
						}
						setState((old) => ({ ...old, loading: true }));
						console.log(JSON.stringify(values, null, 2));
						console.log(values.requirements);
						const filtered = allHuts.filter((hut) => {
							if (
								values.requirements.includes('food') &&
								!hut.food
							) {
								return false;
							}
							if (
								values.requirements.includes('electricity') &&
								!hut.electricity
							) {
								return false;
							}
							if (
								values.requirements.includes('bedding') &&
								!hut.sleepover
							) {
								return false;
							}
							if (
								values.requirements.includes('water') &&
								!hut.water
							) {
								return false;
							}
							return true;
						});
						if (filtered.length === 0) {
							setMessage(
								'Nema takvih domova. Prikazujemo sve...'
							);
							return;
						}
						setMessage(
							'Prikazuju se domovi (' +
								filtered.length +
								') sa željenim stvarima'
						);
						console.log(filtered.length);
						setState({
							loading: false,
							huts: filtered
						});

						/*
						let params = "";
						values.requirements.forEach((req) => params += req.charAt(0));
						
						Axios.get(`${url}/mountain-hut?reqs=${params}`)
							.then((res) => {
								console.log(res);

								if (res.data.length == 0) {
									setMessage("Nema domova koji zadovoljavaju sve uvjete. Prikazuju se svi domovi.");
									setState((old) => ({ ...old, loading: false }));
								}
								else {
									setMessage("Dohvaćeni domovi koji zadovoljavaju kriterije.");
									setState({ huts: res.data, loading: false });
								}




								setMessage("");
							})
							.catch((err) => {
								console.log(err);
								setState({
									loading: false,
									huts: state.huts
								});
								setMessage("Filtriranje nije upalilo. Molimo pokušajte kasnije. Prikazuju se svi domovi.");
							});
							*/
					}}
				>
					{({ values }) => (
						<Form>
							<Col id="checkbox-group">
								Prikaži samo domove gdje su dostupni:
							</Col>
							<Col role="group" aria-labelledby="checkbox-group">
								<label className="mx-3">
									<Field
										type="checkbox"
										name="requirements"
										value="electricity"
									/>
									Struja
								</label>
								<label className="mx-3">
									<Field
										type="checkbox"
										name="requirements"
										value="food"
									/>
									Hrana
								</label>
								<label className="mx-3">
									<Field
										type="checkbox"
										name="requirements"
										value="bedding"
									/>
									Prenoćište
								</label>
								<label className="mx-3">
									<Field
										type="checkbox"
										name="requirements"
										value="water"
									/>
									Pitka voda
								</label>
							</Col>

							<Button
								type="submit"
								variant="success"
								className="mr-2"
							>
								Primijeni filter
							</Button>

							<Button
								type="reset"
								onClick={() => {
									setState({ huts: allHuts, loading: false });
									setMessage(
										'Prikazuju se svi planinarski domovi'
									);
								}}
							>
								Makni filtere
							</Button>
						</Form>
					)}
				</Formik>
				{message}
			</Card>
		</Col>
	);

	if (state.loading) {
		return (
			<Container>
				{filterArea}
				Ucitavam domove...
			</Container>
		);
	} else {
		return (
			<Container>
				{filterArea}
				<div className="hutsDiv">
					<Row>
						{state.huts.map((hut, idx) => (
							<Col key={idx} lg="4">
								<div className="hutContainer">
									<div>Naziv: {hut.name}</div>
									<div>Lokacija: {hut.location.name}</div>
									<div className="button">
										<Link
											to={`/mountain-huts/${hut.id}`}
											key={hut.id}
											className="link"
										>
											<Button
												className="link"
												variant="warning"
											>
												Detalji
											</Button>
										</Link>
									</div>
								</div>
							</Col>
						))}
					</Row>
				</div>
			</Container>
		);
	}
};

export default MountainHuts;
