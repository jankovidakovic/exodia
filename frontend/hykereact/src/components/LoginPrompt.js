import React from 'react';
import { Link } from 'react-router-dom';

const LoginPrompt = () => (
	<>
		<Link to="/login">Prijavite se </Link>
		za dodatne funkcionalnosti
	</>
);

export default LoginPrompt;
