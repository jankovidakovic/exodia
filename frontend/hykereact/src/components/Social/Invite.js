import axios from 'axios';
import React, { useState } from 'react';
import { Alert, Button, Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { url } from '../../constants';
import AlertDismissable from '../AlertDismissable';
import { useAuthState } from '../Auth/AuthContext';

const Invite = ({ event }) => {
	const { user } = useAuthState();
	const [show, setShow] = useState(true);
	const [infoMessage, setInfoMessage] = useState('');
	const [error, setError] = useState(false);

	const respond = (status) => {
		axios
			.post(`${url}/events/${event.id}/reply`, {
				user: {
					id: user.id,
					firstName: user.firstName,
					lastName: user.lastName
				},
				status: status
			})
			.then(
				(response) => {
					setShow(false);
					setError(false);
					if (status === 'ACCEPTED')
						setInfoMessage('Pozivnica uspješno prihvaćena.');
					else setInfoMessage('Pozivnica uspješno odbijena.');
				},
				(error) => {
					setError(true);
				}
			)
			.catch((error) => {
				setError(true);
			});
	};

	return error ? (
		<AlertDismissable message="Došlo je do pogreške. Molimo, pokušajte ponovno kasnije." />
	) : show ? (
		<Alert variant="info">
			<div>
				<Row>
					<Col className="d-flex justify-content-start" lg="7">
						{event.creator.firstName} {event.creator.lastName} vas
						poziva na {event.hike.name}
						</Col>
						<Col lg ="2">
						<Link to={`/event/${event.id}`}>
							<Button variant="info">detalji</Button>
						</Link>
					</Col>
					<Col className="d-flex justify-content-end" lg="3">
						<Button
							variant="success"
							onClick={() => respond('ACCEPTED')}
						>
							Prihvati
						</Button>
						<Button
							variant="danger"
							onClick={() => respond('DECLINED')}
						>
							Odbij
						</Button>
					</Col>
				</Row>
			</div>
		</Alert>
	) : (
		<AlertDismissable message={infoMessage} />
	);
};

export default Invite;
