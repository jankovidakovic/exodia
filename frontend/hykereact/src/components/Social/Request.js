import axios from 'axios';
import React, { useState } from 'react';
import { Alert, Button, Col, Container, Row } from 'react-bootstrap';
import { useAuthState } from '../Auth/AuthContext';

const Request = ({ sender, replyEndpoint }) => {
	const [show, setShow] = useState(true);
	const [error, setError] = useState('');

	const reply = (status) => {
		axios
			.post(replyEndpoint, {
				user: sender,
				status: status
			})
			.then(
				(response) => {
					setShow(false);
					console.log('response succesfully sent');
				},
				(error) => {
					console.log('Replying to friend request returned non-2xx');
					console.log(error);
				}
			)
			.catch((error) => {
				console.log('Friend request reply unexpected error');
				setError(
					'Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovno kasnije.'
				);
			});
	};

	return (
		show &&
		sender &&
		sender.id && (
			<Alert variant="secondary" show={show} >
				<div>
					<Row>
						<Col className="d-flex justify-content-start" lg="6">
							{sender.firstName} {sender.lastName}
						</Col>
						{error ? (
							{ error }
						) : (
							<Col className="d-flex justify-content-end">
								<Button
									onClick={() => reply('ACCEPTED')}
									variant="success"
								>
									Prihvati
								</Button>
								<Button
									onClick={() => reply('DECLINED')}
									variant="danger"
								>
									Odbij
								</Button>
							</Col>
						)}
					</Row>
				</div>
			</Alert>
		)
	);
};

export default Request;
