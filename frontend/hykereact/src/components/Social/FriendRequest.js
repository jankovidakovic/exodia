import axios from 'axios';
import React from 'react';
import { useState } from 'react';
import { Col, Container, Row, Alert, Button } from 'react-bootstrap';
import { url } from '../../constants';
import { useAuthState } from '../Auth/AuthContext';

const FriendRequest = ({ sender }) => {
	const [show, setShow] = useState(true);
	const { user } = useAuthState();
	const [error, setError] = useState('');

	const reply = (status) => {
		const FriendRequestDTO = {
			user: sender,
			status: status
		};

		axios
			.post(
				`${url}/users/${user.id}/friend-requests/reply`,
				FriendRequestDTO
			)
			.then(
				(response) => {
					setShow(false);
					console.log('response succesfully sent');
				},
				(error) => {
					console.log('Replying to friend request returned non-2xx');
					console.log(error);
				}
			)
			.catch((error) => {
				console.log('Friend request reply unexpected error');
				setError(
					'Došlo je do nepredviđene pogreške. Molimo, pokušajte ponovno kasnije.'
				);
			});
	};

	return (
		show &&
		sender &&
		sender.id && (
			<Alert variant="secondary" show={show}>
				<Container>
					<Row>
						<Col className="d-flex justify-content-start">
							{sender.firstName} {sender.lastName}
						</Col>
						{error ? (
							{ error }
						) : (
							<Col className="d-flex justify-content-end">
								<Button
									onClick={() => reply('ACCEPTED')}
									variant="success"
								>
									Prihvati
								</Button>
								<Button
									onClick={() => reply('DECLINED')}
									variant="danger"
								>
									Odbij
								</Button>
							</Col>
						)}
					</Row>
				</Container>
			</Alert>
		)
	);
};

export default FriendRequest;
