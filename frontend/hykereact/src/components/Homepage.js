import { React, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
//import { Formik, Field, Form } from "formik";

import { Card, Col, Button, Container, InputGroup, FormControl, Row } from 'react-bootstrap';
import { useAuthState } from './Auth/AuthContext';
import Axios from 'axios';
import { url } from '../constants';
import "./Homepage.css"

//This function creates the homepage of the web application and for now is used only in esthetic purposes.


let userId = 0;

function renderPost(post) {
	let commonData = (
		<div className="name">
			<div>{post.description}</div>
			<br></br>
			{userId !== post.creator.id ?
				<div>
					Stvorio korisnik:
			{post.creator.firstName} {post.creator.lastName}
				</div> : <div>Stvorili ste vi</div>}

		</div>
	);

	if (post.hike) {
		return (
			<div className="hikeContainer" key={post.id}>
				{commonData}
				<br></br>
				<div>Naziv izleta: {post.hike.name}</div>
				<div>Lokacija: {post.hike.location.name}</div>
				<div>Duljina staze: {post.hike.length}</div>
				<div className="centered">
					<Link to={'hikes/' + post.hike.id} className="link">
						<Button variant="success">Detaljnije o izletu</Button></Link>
				</div>
			</div>
		);
	}
	if (post.event) {
		let dateTime = new Date(post.event.startDateTime);
		let time = (dateTime.getHours() < 10 ? "0" + dateTime.getHours() : dateTime.getHours()) + ":"
			+ (dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes())
		return (
			<div key={post.id} className="eventContainer" fluid>
				{commonData}
				<br></br>

				<div>
					Događaj na izlet: {post.event.hike.name}
				</div>
				<div>
					Lokacija: {post.event.hike.location.name}
				</div>
				<div>
					Duljina staze: {post.event.hike.length}
				</div>
				<div>Početak: {dateTime.getDate()}.{dateTime.getMonth() + 1}.{dateTime.getFullYear()}
					. u {time}</div>
				<div className="centered">

					<Link to={'event/' + post.event.id} className="link"><Button>Detaljnije o događaju</Button></Link>
				</div>
			</div>
		);
	}

	if (post.photo) {
		return (
			<Card key={post.id}>
				{commonData}
				<Card>
					<img
						src={`https://hyke.ddns.net/assets/images/${post.photo.location}`}
						alt={post.photo.location}
						style={{"height": "auto"}}
					></img>
				</Card>
			</Card>
		);
	}

	if (post.badge) {
		return (
			<Card key={post.id}>
				{commonData}
				<Card>
						<img 
			src={`https://hyke.ddns.net/assets/badges/${post.badge.fileSystemLocation}`} 
				alt="Slika bedža" 
				style={{height: "auto"}}
				/>
				</Card>
			</Card>
		);
	}
}

function Homepage() {
	const auth = useAuthState();
	const [posts, setPosts] = useState([]);
	const [message, setMessage] = useState('Hvatanje novosti...');
	const [image, setImage] = useState(null);
	const [desc, setDesc] = useState('');
	userId = auth.user.id;
	function onImageChange(event) {
		if (event.target.files && event.target.files[0]) {
			let img = event.target.files[0];
			setImage(img);
			//URL.createObjectURL(img)
		}
	}

	function onDescChange(event) {
		setDesc(event.target.value);
	}

	function submitImage() {
		console.log(desc);
		if (!image) {
			alert('Molimo odaberite sliku');
			return;
		}
		let data = new FormData();
		data.append('image', image);
		//console.log(data);
		// fetch
		Axios.post(`${url}/photos/raw`, data).then(
			(response) => {
				console.log(response);
				// feedback for developer
				if (response.status === 201) {
					console.log('Slika poslana');
					const imageFilesystemLocation = response.data;

					//post image
					Axios.post(`${url}/photos/data`, {
						user: {
							firstName: auth.user.firstName,
							lastName: auth.user.lastName,
							id: auth.user.id
						},
						photo: {
							location: imageFilesystemLocation,
							description: desc
						}
					})
						.then(
							(response) => {
								console.log('Slika uspješno uploadana!');
							},
							(error) => {
								console.log(error);
							}
						)
						.catch((error) => {
							console.log(error);
						});
				}
			},
			(error) => {
				console.log('slika nije poslana');
			}
		);
	}

	const imagePrompt = (
		<Row className="justify-content-md-center">
		<Container className="imageShare">

			<Col>Želite li podijeliti sliku?</Col>
			<input
				type="file"
				name="myImage"
				onChange={onImageChange}
				accept="image/*"
			/>
			{image ? (
				<Col className="row m-">
					<img src={URL.createObjectURL(image)} alt="" />
				</Col>
			) : (
					<div></div>
				)}
			<Col className="m-2">
				Unijeti opis?
			</Col>
			{/*<Col>
			<input type="text" onChange={onDescChange} value={desc}></input>
			</Col>*/}
			<Col>
				<InputGroup>
					<FormControl as="textarea" aria-label="With textarea" onChange={onDescChange} />
				</InputGroup>
			</Col>
			<Col className="md-5">
				<Button variant="primary" onClick={submitImage}>
					Pošalji
				</Button>
			</Col>
			
			<br></br>
		</Container>
		</Row>
	);

	useEffect(() => {
		getNews();
		const interval = setInterval(getNews, 5000); //TODO - change to bigger timer
		return () => {
			clearInterval(interval);
		};
	}, []);

	const getNews = () => {
		Axios.get(`${url}/users/${auth.user.id}/news`)
			.then(
				(response) => {
					const data = response.data;
					if (data.length === 0) {
						setMessage('Izgleda da ste sve novo vidjeli!');
					}
					console.log(data);
					setPosts(data);
				},
				(error) => {
					setMessage('Nismo uspjeli dohvatiti novosti');
				}
			)
			.catch((error) => {
				//console.log(error);
				setMessage('Nismo uspjeli dohvatiti novosti');
			});
	};

	if (posts.length === 0) {
		//console.log(message);
		return (
			<Container className="homePage">
				{imagePrompt}
				<Col>{message}</Col>
			</Container>
		);
	}

	return (
		<Container className="homePage">
			{imagePrompt}
			<hr></hr>
			<Row className="padding:5%">
				{posts.map((post) =>
					<Col key={post.id} lg="4">
						{renderPost(post)}
					</Col>)}
			</Row>
		</Container>
	);
}

export default Homepage;
