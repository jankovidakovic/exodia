//import { render } from '@testing-library/react';
import React from 'react';
import "./Registration.css"
import { Link } from 'react-router-dom';

function LocationDeleter(props) {
    //With this we define a setter for the name of the location
    const [locName, setLocName] = React.useState("");
    //Defines error and setter for errors
    const [error, setError] = React.useState("");
    //and for other messages
    const [message, setMessage] = React.useState("");

    //Defines what happens if there is a change on the site, it updates old location name.
    function onChange(event) {
        //setError("");
        setMessage("");
        const { name, value } = event.target;
        setLocName(value);
    }


    //const enableButton = false;

    //Defines what happends after we press the create button, when the submit happens and how we handle it
    function onSubmit(e) {
        //We prevent the default submit method that places the data in the http link
        e.preventDefault();
        setError("");
		setMessage("");
        //Sets data that we will send over to backend
        const locData = {
            name: locName
        }
        //If somebody tries to create location without filling name we send the coresponding error
        if (locName === "") {
            setError("Trebam ime područja");
            return;
        }
        
        
        //Sets the method that will be used in backend
        const options = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            },
            //Data converts to JSON format for easier backend usage
            body: JSON.stringify(locData)
        };
		setMessage("Pokušavam izbrisati lokaciju...");
        //Fetches the backend registration fucntion and sends the needed data
        fetch('https://hyke.ddns.net/api/locations', options)
            .then(response => {

                if (response.status === 204) {
                    setMessage("Lokacija uspješno izbrisana iz sustava");
                    setLocName("");
                } else if (response.status === 404) {
                    setError("Lokacija s tim imenom ne postoji");
                }
                else {
                    setError("Lokacija nije stvorena");
                }
            }
            )
    }

    //Returns the builded page for creating a location
    return (
        <div className="inputsDiv">
            <h1 className="login">Brisanje lokacije</h1>
            <form className="centeredContainer" onSubmit={onSubmit}>


                <div className="inputsBox">
                    <div>
                        <label>
                            Naziv lokacije
                            </label>
                    </div>
                    <div>
                        <input name="locName" onChange={onChange} value={locName}></input>
                    </div>
                </div>

                
                
                
                <div className="centeredContainer">
                    
                    {error === "" ?
                        message === "" ? <div></div> : <div>{message}</div>
                        :
                        <div className="error">{error}</div>
                    }
                </div>
                

                <div className="centeredContainer">
                    <button className="centeredContainer">Izbrisi geografsko podrucje
                    </button>
                </div>
                
                <div className="centeredContainer">

                    <label> Želite li pregledati postojeće lokacije? 
                        <Link to='/locations'> Sve lokacije </Link></label>
                </div>
                
            </form>
        </div>
    );
}

export default LocationDeleter;
