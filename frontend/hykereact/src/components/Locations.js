import React from "react";
import "./Fieldtrip.css";
import { Link } from 'react-router-dom';

export default class Locations extends React.Component {

    // tracks data about locations
    state = {
        // is changed to false when locations have been fetched
        loading: true,
        // empty before fetching
        locations: []
    };

    checkRender = false; // true for viewing test locations for development purposes

    // handles fetching
    async componentDidMount() {

        const url = "https://hyke.ddns.net/api/locations";
		fetch(url)
		.then(res => res.json()) // turns fetched data into json format
		.then(data => this.setState({locations: data, loading: false})) // place data into array to be rendered
			.catch(e => {
				console.log(e);
				return e;
			})
    }

    // show loading when data has not yet been fetched
    // then if locations exist show each in its own <div> component
    render() {

        let prompts =
        <div>
            <label> Želite li dodati novu lokaciju? 
                <Link to='/locations/add'> Dodavanje lokacije </Link></label>
            <label> Želite li promijeniti neku lokaciju? 
                <Link to='/locations/update'> Ažuriranje lokacije </Link></label>
        </div>

        if (this.checkRender) {

            this.state.locations = [{name: "Zumberak", description: "Zeleno"}, {name: "Bilogora", description: "Mirisi"},
                {name: "Dinarsko gorje", description: "Ni orlovi"}, {name: "Velebit", description: "Bura"}];
            return (
                <div className="hikesDiv">
                    {this.state.locations.map(location => (
                        <div className="hikeDiv">
                            <div>Naziv: {location.name}</div>
                            <div>Opis: {location.description}</div>
                        </div>
                    ))}
                </div>
            );
        }

        if (this.state.loading) {
            return <div className="hikesDiv">
                {prompts}
                <div className = "hikeDiv">Ucitavam lokacije...</div></div>;
        }

        if (!this.state.locations.length) {
            return <div className="hikesDiv">
                {prompts}
                Nisu ucitane lokacije.</div>;
        }

        return (
            <div className="hikesDiv">
                {prompts}
                {this.state.locations.map(location => (
                    <div className="hikeDiv">
						<div>Naziv: {location.name}</div>
                        <div>Opis: {location.description}</div>
                    </div>
                ))}
            </div>
        );
    }
}

