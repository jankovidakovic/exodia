import React from 'react';
import './App.css';

import Header from './components/Header';
import Footer from './components/Footer';
import AppRouter from './components/Nav/AppRouter';
import { AuthProvider } from './components/Auth/AuthContext';
import Axios from 'axios';

/*
the main component that renders on the screen and defines the routes to all sub-components
*/
function App() {
	Axios.interceptors.request.use((request) => {
		console.log(request);
		let token = localStorage.getItem('currentUser')
			? JSON.parse(localStorage.getItem('currentUser')).token
			: '';
		if (token) {
			request.headers['Authorization'] = 'Bearer ' + token;
		}
		return request;
	});
	// to store username if user is logged in
	//const [loginName] = useState('helo');
	//const [auth] = useAuthState(); //CANNOT BE USED HERE SINCE ITS NOT WRAPPED IN AUTHPROVIDER DUMBASS
	//i guess i could wrap it on the outside tho
	// returns the essential components needed to render page
	/*
	return (
		<AuthProvider>
			<BrowserRouter>
				<Header />
				<AuthHeader />
				<Browser />
				<div className="body">
					<div className="userNameDiv">
						<label className="userName">{loginName}</label>
					</div>
					<Switch>
						{routes.map((route, idx) => (
							<Route
								key={idx}
								path={route.path}
								component={route.component}
								exact
							/>
						))}
					</Switch>
				</div>
				<Footer />
			</BrowserRouter>
		</AuthProvider>
	);
	*/
	return (
		<AuthProvider>
			<Header />
			<AppRouter />
			{/*
            <Route path="/mountain-huts" exact component={MountainHuts}></Route>
            <Route path="/mountain-huts/:id" exact component = {() => <MountainHut firstName={firstName} lastName={lastName} personID={id} /> }></Route>
			<Route path ="/mountain-huts/new" exact component = {MountainHutAdder}></Route>
            <Route path="/events" exact component={UserEvents} ></Route>
            <Route path="/events/new" exact component={NewEvent}></Route>
            <Route path="/events/:id" exact component={DetailedEvent}></Route>
			<Route path="/users/:id/archived-hikes" exact component={Archive}></Route>
            <Route path="/hikes" exact component = {Trip}></Route>
            <Route path='/locations/add' exact component={LocationAdder} />
            <Route path='/locations' exact component={Locations} />
            <Route path='/locations/delete' exact component={LocationDeleter} />
            <Route path='/locations/update' exact component={LocationUpdater} />
			<Route exact path="/hikes/:id" exact component={DetailedHike} />
          <Route path='/wishlist' exact component={Wishlist} />
				*/}
				<br></br>
			<Footer />
		</AuthProvider>
	);
}

export default App;
