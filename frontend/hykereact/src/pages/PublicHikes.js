import axios from 'axios';
import React, { useEffect, useState } from 'react';
import HikeList from '../components/Hike/HikeList';
import NewHikeButton from '../components/Hike/NewHikeButton';
import { url } from '../constants';
import "./PublicHikes.css"
import { Button, Col, Card, Container, Form as BSForm, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';

const PublicHikes = () => {
	const [hikes, setHikes] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState('');

	const [message, setMessage] = useState("");
	const [allHikes, setAllHikes] = useState([]);

	const [expandLength, setExpandLength] = useState(false);
	const [locations, setLocations] = useState([]);

	const [enableFiltering, setEnableFiltering] = useState(false);

	useEffect(() => {
		axios
			.get(`${url}/hikes?publicity=1`)
			.then(
				(response) => {
					setHikes(response.data);
					setAllHikes(response.data);
					setLoading(false);
				},
				(error) => {
					setError(
						'Izleti trenutno nisu dostupni. Molimo, pokušajte ponovno kasnije.'
					);
				}
			)
			.catch((error) => {
				setError(
					'Izleti trenutno nisu dostupni. Molimo, pokušajte ponovno kasnije.'
				);
			});

		axios
			.get(`${url}/locations`)
			.then(
				(response) => {
					console.log(response.data);
					setLocations(response.data);
				},
				(error) => {
					console.log(error);
				}
			)
			.catch((error) => {
				console.log(error);
			});
	}, []);


	const availableIntensities = ["LOW", "MODERATE", "HIGH", "VERY_HIGH"]
	const intensitiesGUI = ["Lagano", "Srednje", "Teško", "Jako teško"];


	const filterArea = (
		<Col>
			<Card>Filtriranje
						<Formik
					initialValues={{
						minIntensity: 0,
						maxIntensity: 3,
						minLength: 0,
						maxLength: 0,
						locationId: -1,
						locationUnspecified: false,
					}}
					onSubmit={async (values) => {
						let newMessage = "";
						console.log(message);
						values.minLength = Math.max(values.minLength, 0);
						values.maxLength = Math.max(values.maxLength, 0);

						const ignoreLength = !expandLength ||
							(values.maxLength <= 0 && values.minLength <= 0);
						const ignoreMaxLength = (values.minLength > values.maxLength
							|| values.maxLength === 0 || !values.maxLength);
						const ignoreIntensity = values.minIntensity === 0 && values.maxIntensity === 3
							|| values.minIntensity > values.maxIntensity;
						const ignoreLocation = (!values.locationId || values.locationId < 0 || values.locationUnspecified);
						console.log(values);
						if (ignoreLength && ignoreIntensity && ignoreLocation) {
							setMessage("Prikazuju se svi izleti");
							return;
						}
						if (expandLength && ignoreMaxLength) {
							newMessage += "Neodređena maksimalna duljina. ";
							//setMessage();
						}
						if (values.minIntensity > values.maxIntensity) {
							newMessage += "Neodređeni raspon težine. "
							//setMessage(message + "Neodređeni raspon težine. ")
						}

						//setLoading(true);
						console.log(JSON.stringify(values, null, 2));
						let filteredHikes = allHikes.filter((hike) => {
							//console.log(hike.location.id + "", values.locationId);
							//console.log(values.locationId);
							if (!ignoreLocation && hike.location.id + "" != values.locationId) {
								return false;
							}
							let intIndex = availableIntensities.indexOf(hike.intensity);
							if (values.minIntensity <= values.maxIntensity &&
								(intIndex < values.minIntensity || intIndex > values.maxIntensity)) {
								return false;
							}
							//console.log(hike.length, values.minLength, values.maxLength);

							if (!ignoreLength && (hike.length < values.minLength ||
								!ignoreMaxLength && hike.length > values.maxLength)) {
								return false;
							}
							return true;

						});
						//setLoading(false);
						if (filteredHikes.length > 0) {
							newMessage += " Prikazani izleti koji zadovoljavaju kriterije.";
							//setMessage(message + " Prikazani izleti koji zadovoljavaju kriterije.");

							console.log(filteredHikes);
							setHikes(filteredHikes);
						}
						else {
							newMessage += " Nema izleta koji zadovoljavaju kriterije. Prikazuju se svi izleti.";
							setHikes(allHikes);
							//setMessage(message + " Nema izleta koji zadovoljavaju kriterije. Prikazuju se svi izleti.");
						}
						setMessage(newMessage);
					}}
				>
					{({ values }) => (
						<Form>

							<Col>
								Odabir lokacije:
								<Field
									as="select"
									name="locationId"
									placeholder=""
									className="m-2"
								>
									<option
										key="-1"
										name=""
										value=""
										className="m-2"
									/>

									{locations.map((loc) => (
										<option
											key={loc.id}
											name="loc"
											value={loc.id}
											className="m-2"
										>
											{loc.name}
										</option>
									))}
								</Field>
								<Col >
									<Field
										type="checkbox"
										checked={values.locationUnspecified}
										name="locationUnspecified"
										value={values.locationUnspecified}
										className="m-2"
									/> Bilo koja lokacija
								</Col>



							</Col>


							<Col className="">
								Duljina <Button onClick={() => {
									setExpandLength(!expandLength);
									if (!expandLength) { values.minLength = 0; values.maxLength = 0; }

								}}>Odabir</Button>
								{expandLength ? <Col>
									Najkraći:
									<Field
										placeholder=""
										name="minLength"
										type="number"
										className="m-1"
									/>  Najduži: <Field
										placeholder=""
										name="maxLength"
										type="number"
										className="m-1"
									/>


								</Col>
									: <div></div>}
							</Col>
							<Col>

							</Col>
							<Col>
								Težina od:
								<Field
									as="select"
									name="minIntensity"
									placeholder=""
									className="m-2"
								>
									{[0, 1, 2, 3].map((i) => (
										<option
											key={i}
											name="intensity"
											value={i}
											className="m-2"
										>
											{intensitiesGUI[i]}
										</option>
									))}
								</Field></Col>
							<Col>
								Težina do:
								<Field
									as="select"
									name="maxIntensity"
									placeholder=""
									className="m-2"
								>
									{[0, 1, 2, 3].map((i) => (
										<option
											key={i}
											name="intensity"
											value={i}
											className="m-2"
										>
											{intensitiesGUI[i]}
										</option>
									))}
								</Field></Col>


							<Button type="submit" variant="outline-success" className="mr-1">Primijeni filter</Button>

							<Button type="reset" onClick={() => {
								setHikes(allHikes);
								setLoading(false);
								setMessage("Prikazuju se svi izleti")
							}}>
								Resetiraj filtere</Button>

						</Form>
					)}

				</Formik>

				{message}
			</Card>
		</Col >
	);


	return (
		<div>
			<div>
				{enableFiltering ? filterArea : <div></div>}
				<Button className="m-3" onClick={() => { setEnableFiltering(!enableFiltering) }}>Odabir kriterija</Button>

				<NewHikeButton />
			</div>
			<br></br>
			<Row>
				<HikeList hikes={hikes} loading={loading} error={error} />
			</Row>
		</div>

	);
};

export default PublicHikes;
