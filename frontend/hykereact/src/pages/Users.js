import axios from 'axios';
import React, { useEffect, useState } from 'react';
import UserList from '../components/User/UserList';
import { url } from '../constants';

const Users = () => {
	const [users, setUsers] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState('');

	useEffect(() => {
		axios
			.get(`${url}/users`)
			.then(
				(response) => {
					setUsers(response.data);
					setLoading(false);
				},
				(error) => {
					setError(
						'Došlo je do pogreške. Molimo, pokušajte ponovno kasnije.'
					);
					setLoading(false);
				}
			)
			.catch((error) => {
				setError(
					'Došlo je do pogreške. Molimo, pokušajte ponovno kasnije.'
				);
				setLoading(false);
			});
	}, []);

	return <UserList users={users} loading={loading} error={error} />;
};

export default Users;
