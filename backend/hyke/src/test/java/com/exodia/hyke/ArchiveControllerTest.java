package com.exodia.hyke;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.exodia.hyke.dto.ArchivedHikeDTO;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.ArchivedHike;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HikeIntensity;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Location;
import com.exodia.hyke.model.MountainHut;
import com.exodia.hyke.model.Role;
import com.exodia.hyke.rest.ArchiveController;
import com.exodia.hyke.service.ArchiveService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ArchiveController.class)
public class ArchiveControllerTest {

	@Autowired
	private MockMvc mock;

	@MockBean
	private ArchiveService service;

	private ArrayList<HykeUser> userList;
	private ArrayList<Hike> hikeList;
	private ArrayList<ArchivedHike> aHikeListUser1;

	@BeforeEach
	void setUp() {
		HykeUser user1 = new HykeUser();
		user1.setEmail("user1@email.com");
		user1.setFirstName("name1");
		user1.setLastName("lastName1");
		user1.setPassword("pass1");
		user1.setUsername("user1");
		user1.setRole(Role.USER);
		user1.setId(1L);

		HykeUser user2 = new HykeUser();
		user2.setEmail("user2@email.com");
		user2.setFirstName("name2");
		user2.setLastName("lastName2");
		user2.setPassword("pass2");
		user2.setUsername("user2");
		user2.setRole(Role.USER);
		user2.setId(2L);

		HykeUser user3 = new HykeUser();
		user3.setEmail("user3@email.com");
		user3.setFirstName("name3");
		user3.setLastName("lastName3");
		user3.setPassword("pass3");
		user3.setUsername("user3");
		user3.setRole(Role.USER);
		user3.setId(3L);

		this.userList = new ArrayList<>();
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);

		Location location1 = new Location();
		location1.setName("lokacija1");
		location1.setDescription("lokacija1");
		location1.setId(1L);

		Location location2 = new Location();
		location2.setName("lokacija2");
		location2.setDescription("lokacija2");
		location2.setId(2L);

		MountainHut mountainHut1 = new MountainHut();
		mountainHut1.setLocation(location1);
		mountainHut1.setName("dom1");
		mountainHut1.setDescription("Planinarski dom 1");
		mountainHut1.setFood(true);
		mountainHut1.setWater(true);
		mountainHut1.setElectricity(true);
		mountainHut1.setSleepover(false);
		mountainHut1.setId(1L);

		Hike hike1 = new Hike();
		hike1.setLocation(location1);
		hike1.setName("izlet1");
		hike1.setDescription("Izlet1");
		hike1.setHikeStart("Start1");
		hike1.setHikeEnd("End1");
		hike1.setHikeLength(1.0);
		hike1.setIntensity(HikeIntensity.LOW);
		hike1.setPublicity(true);
		hike1.setCreator(user1);
		hike1.setCreatedAt(new Date());
		Set<MountainHut> huts = new HashSet<>();
		huts.add(mountainHut1);
		hike1.setMountainHuts(huts);
		hike1.setId(1L);

		Hike hike2 = new Hike();
		hike2.setLocation(location1);
		hike2.setName("izlet2");
		hike2.setDescription("Izlet2");
		hike2.setHikeStart("Start2");
		hike2.setHikeEnd("End2");
		hike2.setHikeLength(1.0);
		hike2.setIntensity(HikeIntensity.LOW);
		hike2.setPublicity(true);
		hike2.setCreator(user2);
		hike2.setCreatedAt(new Date());
		Set<MountainHut> huts2 = new HashSet<>();
		huts2.add(mountainHut1);
		hike2.setMountainHuts(huts2);
		hike2.setId(2L);

		hikeList.add(hike1);
		hikeList.add(hike2);

		ArchivedHike aHike1 = new ArchivedHike();
		aHike1.setHike(hikeList.get(0));
		aHike1.setArchivedAt(new Date());
		aHike1.setDoneAt(new Date());
		aHike1.setArchiveDescription("desc1");

		ArchivedHike aHike2 = new ArchivedHike();
		aHike2.setHike(hikeList.get(1));
		aHike2.setArchivedAt(new Date());
		aHike2.setDoneAt(new Date());
		aHike2.setArchiveDescription("desc2");

		this.aHikeListUser1 = new ArrayList<>();
		aHikeListUser1.add(aHike1);
		aHikeListUser1.add(aHike2);
	}

	@Test
	void shouldReturnUserArchivedHikes() throws Exception {
		final Long userId = 1L;

		when(service.findByUserId(userId)).thenReturn(aHikeListUser1);

		this.mock.perform(get("users/{userId}/archived-hikes", userId)).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()", is(aHikeListUser1.size())));
	}

	@Test
	void shouldReturnNotFoundWhenFindByUserId() throws Exception {
		final Long userId = 4L;

		when(service.findByUserId(userId))
				.thenThrow(new UserDoesNotExistException("User with specified id does not exist."));
		this.mock.perform(get("users/{userId}/archived-hikes", userId)).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.size()", is(0)));
	}

	@Test
	void shouldAddHikeToArchive() throws Exception {
		final Long userId = 1L;

		ArchivedHike aHike = new ArchivedHike();
		aHike.setHike(hikeList.get(1));
		aHike.setArchivedAt(new Date());
		aHike.setDoneAt(new Date());
		aHike.setArchiveDescription("desc");

		ArchivedHikeDTO dto = ArchivedHikeDTO.getDTO(aHike);

		ObjectMapper mapper = new ObjectMapper();

		this.mock.perform(post("users/{userId}/archived-hikes", userId).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))).andExpect(status().isOk());
	}
}
