package com.exodia.hyke;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginUnsuccessfulTest {
	private WebDriver driver;
	private Map<String, Object> vars;
	JavascriptExecutor js;

	@BeforeEach
	public void setUp() {
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		vars = new HashMap<String, Object>();
	}

	@AfterEach
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void loginUnsuccessful() {
		driver.get("https://hyke.ddns.net/login");
		driver.manage().window().setSize(new Dimension(1552, 840));
		driver.findElement(By.id("validationFormikUsername")).click();
		// Name input
		driver.findElement(By.id("validationFormikUsername")).sendKeys("test");
		driver.findElement(By.id("validationFormikPassword")).click();
		// Invalid password input
		driver.findElement(By.id("validationFormikPassword")).sendKeys("testlozinka123");
		// Sending invalid login information
		driver.findElement(By.cssSelector(".btn:nth-child(4)")).click();
	}
}
