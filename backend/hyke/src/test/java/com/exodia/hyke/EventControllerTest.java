package com.exodia.hyke;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.exodia.hyke.dto.EventInviteDTO;
import com.exodia.hyke.dto.IdDTO;
import com.exodia.hyke.dto.form.EventFormDTO;
import com.exodia.hyke.exception.EventDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.Event;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HikeIntensity;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Invite;
import com.exodia.hyke.model.Location;
import com.exodia.hyke.model.MountainHut;
import com.exodia.hyke.model.Role;
import com.exodia.hyke.model.Status;
import com.exodia.hyke.rest.EventController;
import com.exodia.hyke.service.EventService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(EventController.class)
public class EventControllerTest {

	@Autowired
	private MockMvc mock;

	@MockBean
	private EventService service;

	private List<HykeUser> userList;
	private List<Event> eventList;

	@BeforeEach
	void setUp() {

		HykeUser user1 = new HykeUser();
		user1.setEmail("user1@email.com");
		user1.setFirstName("name1");
		user1.setLastName("lastName1");
		user1.setPassword("pass1");
		user1.setUsername("user1");
		user1.setRole(Role.USER);
		user1.setId(1L);

		HykeUser user2 = new HykeUser();
		user2.setEmail("user2@email.com");
		user2.setFirstName("name2");
		user2.setLastName("lastName2");
		user2.setPassword("pass2");
		user2.setUsername("user2");
		user2.setRole(Role.USER);
		user2.setId(2L);

		HykeUser user3 = new HykeUser();
		user3.setEmail("user3@email.com");
		user3.setFirstName("name3");
		user3.setLastName("lastName3");
		user3.setPassword("pass3");
		user3.setUsername("user3");
		user3.setRole(Role.USER);
		user3.setId(3L);

		this.userList = new ArrayList<>();
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);

		Location location1 = new Location();
		location1.setName("lokacija1");
		location1.setDescription("lokacija1");
		location1.setId(1L);

		Location location2 = new Location();
		location2.setName("lokacija2");
		location2.setDescription("lokacija2");
		location2.setId(2L);

		MountainHut mountainHut1 = new MountainHut();
		mountainHut1.setLocation(location1);
		mountainHut1.setName("dom1");
		mountainHut1.setDescription("Planinarski dom 1");
		mountainHut1.setFood(true);
		mountainHut1.setWater(true);
		mountainHut1.setElectricity(true);
		mountainHut1.setSleepover(false);
		mountainHut1.setId(1L);

		Hike hike1 = new Hike();
		hike1.setLocation(location1);
		hike1.setName("izlet1");
		hike1.setDescription("Izlet1");
		hike1.setHikeStart("Start1");
		hike1.setHikeEnd("End1");
		hike1.setHikeLength(1.0);
		hike1.setIntensity(HikeIntensity.LOW);
		hike1.setPublicity(true);
		hike1.setCreator(user1);
		hike1.setCreatedAt(new Date());
		Set<MountainHut> huts = new HashSet<>();
		huts.add(mountainHut1);
		hike1.setMountainHuts(huts);
		hike1.setId(1L);

		Event event1 = new Event();
		event1.setHike(hike1);
		event1.setStartDateTime(new Date());
		event1.setEndDateTime(new Date());
		event1.setDescription("događaj1");
		event1.setCreator(user1);
		event1.setCreatedAt(new Date());

		Event event2 = new Event();
		event2.setHike(hike1);
		event2.setStartDateTime(new Date());
		event2.setEndDateTime(new Date());
		event2.setDescription("događaj2");
		event2.setCreator(user1);
		event2.setCreatedAt(new Date());

		Event event3 = new Event();
		event2.setHike(hike1);
		event2.setStartDateTime(new Date());
		event2.setEndDateTime(new Date());
		event2.setDescription("događaj3");
		event2.setCreator(user2);
		event2.setCreatedAt(new Date());

		this.eventList = new ArrayList<>();
		eventList.add(event1);
		eventList.add(event2);
	}

	@Test
	void shouldListUser1Events() throws Exception {
		final Long userId = 1L;

		when(service.findByCreatorId(userId)).thenReturn(eventList);
		this.mock.perform(get("/events?creatorId=1")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()", is(eventList.size())));
	}

	@Test
	void shouldReturnNotFoundWhenFindByCreator() throws Exception {
		final Long userId = 4L;

		when(service.findByCreatorId(userId))
				.thenThrow(new UserDoesNotExistException("User with specified id does not exist."));
		this.mock.perform(get("/events?creatorId=3")).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.size()", is(0)));
	}

	@Test
	void shouldCreateNewEvent() throws Exception {
		final EventFormDTO dto = new EventFormDTO();

		final IdDTO hikeIdDTO = new IdDTO();
		hikeIdDTO.setId(1L);

		final IdDTO creatorIdDTO = new IdDTO();
		creatorIdDTO.setId(1L);

		dto.setCreator(hikeIdDTO);
		dto.setHike(hikeIdDTO);
		dto.setDescription("desc");
		dto.setEndDateTime(new Date());
		dto.setStartDateTime(new Date());

		ObjectMapper mapper = new ObjectMapper();

		this.mock.perform(post("/events")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isCreated());
	}

	@Test
	void shouldFetchOneEventById() throws Exception {
		final Long eventId = 1L;

		this.mock.perform(get("/events/{eventId}", eventId)).andExpect(status().isOk())
				.andExpect(jsonPath("$.creator.id", is(1L))).andExpect(jsonPath("$.hike.id", is(1L)))
				.andExpect(jsonPath("$.startDateTime", is(eventList.get(1).getStartDateTime())))
				.andExpect(jsonPath("$.endDateTime", is(eventList.get(1).getEndDateTime())))
				.andExpect(jsonPath("$.description", is(eventList.get(1).getDescription())));
	}

	@Test
	void shouldReturnNotFoundWhenFindByEvent() throws Exception {
		final Long eventId = 5L;
		when(service.findById(eventId))
				.thenThrow(new EventDoesNotExistException("Event with specified id does not exist."));

		this.mock.perform(get("/events/{eventId}", eventId)).andExpect(status().isNotFound());
	}

	@Test
	void shouldReturnEventInvitesById() throws Exception {
		final Long eventId = 1L;

		final HykeUser user1 = userList.get(0);
		final Invite invite1 = new Invite();
		invite1.setStatus(Status.PENDING);
		invite1.setInvitedUser(user1);

		final HykeUser user2 = userList.get(1);
		final Invite invite2 = new Invite();
		invite2.setStatus(Status.PENDING);
		invite2.setInvitedUser(user2);

		final List<Invite> inviteList = new ArrayList<>();
		inviteList.add(invite1);
		inviteList.add(invite2);

		when(service.findEventInvites(eventId)).thenReturn(inviteList);

		this.mock.perform(get("/event/{eventId}/invites", eventId)).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()", is(inviteList.size())));
	}

	@Test
	void shouldSendInvite() throws Exception {
		final Long eventId = 1L;

		final HykeUser user1 = userList.get(0);
		final Invite invite1 = new Invite();
		invite1.setStatus(Status.PENDING);
		invite1.setInvitedUser(user1);

		EventInviteDTO dto = EventInviteDTO.getDTO(invite1);

		ObjectMapper mapper = new ObjectMapper();

		this.mock.perform(post("/event/{eventId}/invites", eventId).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))).andExpect(status().isOk());
	}

}
