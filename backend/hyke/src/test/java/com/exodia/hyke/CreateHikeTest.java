package com.exodia.hyke;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CreateHikeTest {
	private WebDriver driver;
	private Map<String, Object> vars;
	JavascriptExecutor js;

	@BeforeEach
	public void setUp() {
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		vars = new HashMap<String, Object>();
	}

	@AfterEach
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void createHike() {
		driver.get("https://hyke.ddns.net/hikes");
		driver.manage().window().setSize(new Dimension(1552, 840));
		// Attempting to create a hike
		driver.findElement(By.cssSelector("a:nth-child(3) > .btn")).click();
		driver.findElement(By.name("name")).click();
		driver.findElement(By.name("name")).sendKeys("testni izlet");
		driver.findElement(By.cssSelector(".row:nth-child(1) > .col:nth-child(2) > .m-2")).click();
		driver.findElement(By.cssSelector(".row:nth-child(1) > .col:nth-child(2) > .m-2"))
				.sendKeys("test za potrebe selenium testiranja");
		driver.findElement(By.name("location")).click();
		{
			WebElement dropdown = driver.findElement(By.name("location"));
			dropdown.findElement(By.xpath("//option[. = 'Medvednica']")).click();
		}
		driver.findElement(By.name("location")).click();
		driver.findElement(By.cssSelector(".locationFancy:nth-child(1) > label")).click();
		driver.findElement(By.cssSelector(".locationFancy:nth-child(2) .m-2")).click();
		driver.findElement(By.name("hikeStart")).click();
		driver.findElement(By.name("hikeStart")).sendKeys("Zagreb");
		driver.findElement(By.name("hikeEnd")).click();
		driver.findElement(By.name("hikeEnd")).sendKeys("Grafičar");
		driver.findElement(By.name("hikeLength")).click();
		driver.findElement(By.name("hikeLength")).sendKeys("20.0");
		driver.findElement(By.name("intensity")).click();
		{
			WebElement dropdown = driver.findElement(By.name("intensity"));
			dropdown.findElement(By.xpath("//option[. = 'Srednje']")).click();
		}
		driver.findElement(By.name("intensity")).click();
		driver.findElement(By.name("publicity")).click();
		// Sending new hike information
		driver.findElement(By.cssSelector(".btn-primary")).click();
		// Opening user profile
		driver.findElement(By.cssSelector(".btn-success")).click();
		js.executeScript("window.scrollTo(0,0)");
		// Viewing new hike's information
		driver.findElement(By.cssSelector(".browserLink > .btn")).click();
		js.executeScript("window.scrollTo(0,640.7999877929688)");
	}
}
