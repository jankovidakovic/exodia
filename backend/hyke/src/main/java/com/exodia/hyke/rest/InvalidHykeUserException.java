package com.exodia.hyke.rest;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

public class InvalidHykeUserException extends RuntimeException {

    Map<String, String> errorsMap;

    public InvalidHykeUserException(BindingResult errors) {
        this.errorsMap = new HashMap<>();
                errors.getAllErrors()
                .stream()
                .filter(error -> error instanceof FieldError)
                        .forEach(error -> errorsMap.put(((FieldError) error).getField(), error.getDefaultMessage()));
    }

    public Map<String, String> getErrorsMap() {
        return errorsMap;
    }
}
