package com.exodia.hyke.service;

import java.util.List;

import com.exodia.hyke.dto.EventInviteDTO;
import com.exodia.hyke.dto.form.EventFormDTO;
import com.exodia.hyke.model.Event;
import com.exodia.hyke.model.Invite;

public interface EventService {
	
	Event findById(Long id);

	List<Event> findByCreatorId(Long creatorId);

	void createEvent(EventFormDTO dto);

	List<Invite> findEventInvites(Long eventId);

	String sendInvite(EventInviteDTO eventInviteDTO, Long eventId);

	void replyToInvite(Long eventId, EventInviteDTO eiDTO);
	
}
