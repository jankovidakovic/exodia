package com.exodia.hyke.rest;

import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.model.HykeUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class HykeUserDetailsService implements UserDetailsService {

    @Autowired
    HykeUserRepository hykeUserRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        HykeUser user = hykeUserRepo
                .findByUsername(username);
        if(user == null) throw new UsernameNotFoundException("Korisnik " + username + " nije pronađen.");

        return new HykeUserDetails(username, user.getPassword(), authorities(user));
    }

    private List<GrantedAuthority> authorities(HykeUser user){
        return commaSeparatedStringToAuthorityList("ROLE_"+user.getRole().toString());

    }
}
