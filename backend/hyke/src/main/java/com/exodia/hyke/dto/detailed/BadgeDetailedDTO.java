package com.exodia.hyke.dto.detailed;

import com.exodia.hyke.model.Badge;

public class BadgeDetailedDTO {

    private String location;
    private String name;
    private String description;

    public BadgeDetailedDTO() {

    }

    //for testing the frontend
    public BadgeDetailedDTO(String name, String description) {
        this.location = "NONE";
        this.name = name;
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
