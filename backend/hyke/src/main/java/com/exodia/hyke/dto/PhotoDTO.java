package com.exodia.hyke.dto;

import com.exodia.hyke.model.Photo;

public class PhotoDTO {

    private String location;
    private String description;

    public static PhotoDTO getDTO (Photo photo)
    {
        PhotoDTO photoDTO = new PhotoDTO ();

        photoDTO.setLocation (photo.getFilesystemLocation ());
        photoDTO.setDescription (photo.getDescription ());

        return photoDTO;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
