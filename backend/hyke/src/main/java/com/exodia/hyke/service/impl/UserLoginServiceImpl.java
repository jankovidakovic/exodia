package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.dto.form.UserLoginDTO;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.service.UserLoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserLoginServiceImpl implements UserLoginService
{
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private HykeUserRepository userRepo;

    @Override
    public boolean userLogin (UserLoginDTO userLoginDTO)
    {
        if (!userRepo.existsWithUsername (userLoginDTO.getUsername ())) {
            throw new UserDoesNotExistException ("User with username \"" + userLoginDTO.getUsername () + "\" does not exist");
        }

        return passwordEncoder.matches(userLoginDTO.getPassword (),
                userRepo.findByUsername (userLoginDTO.getUsername ()).getPassword ());
    }
}