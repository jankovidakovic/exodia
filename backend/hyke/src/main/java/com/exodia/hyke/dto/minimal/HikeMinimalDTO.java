package com.exodia.hyke.dto.minimal;

import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HikeIntensity;

import java.util.Optional;

/**
 * GET /hikes, dobiva se polje ovoga
 */
public class HikeMinimalDTO {

    private Long id;
    private String name;
    private LocationMinimalDTO location;
    private Double length;
    private HikeIntensity intensity;
    private UserMinimalDTO creator;
    private boolean publicity;

    public static HikeMinimalDTO getDTO(Hike hike) {
        HikeMinimalDTO dto = new HikeMinimalDTO();
        dto.setId(hike.getId());
        dto.setName(hike.getName());
        dto.setLocation(LocationMinimalDTO.getDTO(hike.getLocation()));
        dto.setLength(hike.getHikeLength());
        dto.setIntensity(hike.getIntensity());
        if (hike.getCreator() != null) {
            dto.setCreator(UserMinimalDTO.getDTO(hike.getCreator()));
        } else {
            dto.setCreator(null);
        }
        dto.setPublicity(hike.getPublicity());

        return dto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationMinimalDTO getLocation() {
        return location;
    }

    public void setLocation(LocationMinimalDTO location) {
        this.location = location;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public HikeIntensity getIntensity() {
        return intensity;
    }

    public void setIntensity(HikeIntensity intensity) {
        this.intensity = intensity;
    }

    public UserMinimalDTO getCreator() {
        return creator;
    }

    public void setCreator(UserMinimalDTO creator) {
        this.creator = creator;
    }

    public boolean isPublicity() {
        return publicity;
    }

    public void setPublicity(boolean publicity) {
        this.publicity = publicity;
    }
}
