package com.exodia.hyke.service;

import java.util.List;

import com.exodia.hyke.dto.form.UserRegistrationDTO;
import com.exodia.hyke.exception.UserAlreadyExistException;
import com.exodia.hyke.model.HykeUser;

/**
 * Manages new user registration
 * @author Roko Grbelja
 */
public interface UserRegistrationService {
	
	/**
     * Lists all registered users in the system.
     * @return list containing all HykeUsers
     */
	List<HykeUser> listAll();
	
	/**
     * Registers a new HykeUser
     * @param userDto user data provided by the registration form to be inserted into the database
     * @return created HykeUser with ID set by the system.
     * @throws UserAlreadyExistException if there already exists a registered user in the database with
     * either the email or the username provided by the form
     * @see HykeUser
     */
	HykeUser registerNewHykeUser(UserRegistrationDTO userDto);
}
