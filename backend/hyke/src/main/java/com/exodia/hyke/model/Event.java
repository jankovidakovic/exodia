package com.exodia.hyke.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "hyke_event")
public class Event extends Post { //TODO - hashCode, equals

    @ManyToOne
    private Hike hike;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDateTime;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDateTime;

    @Column(nullable = false)
    private String description; //TODO - getter, setter

    @OneToMany
    private Collection<Invite> invites;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return getHike().equals(event.getHike()) && getStartDateTime().equals(event.getStartDateTime()) && getEndDateTime().equals(event.getEndDateTime()) && getDescription().equals(event.getDescription()) && getInvites().equals(event.getInvites());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHike(), getStartDateTime(), getEndDateTime(), getDescription(), getInvites());
    }
//TODO - dohvat sudionika događaja?

    public Hike getHike() {
        return hike;
    }

    public void setHike(Hike hike) {
        this.hike = hike;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Invite> getInvites() {
        return invites;
    }

    public void setInvites(Collection<Invite> invites) {
        this.invites = invites;
    }
}
