package com.exodia.hyke.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Building located along some hiking trip or at the top of some mountain, that serves as
 * a place for hikers to stop by, rest, have some refreshments or lunch, or in some cases
 * stay the night. Uniquely identified by its ID.
 * @author jankovidakovic
 */
@Entity
public class MountainHut {

    @Id
    @GeneratedValue
    private Long id; //unique ID of mountain hut instance

    @ManyToOne
    @NotNull
    private Location location; //general location that the mountain hut belongs to

    @NotNull
    private String name; //mountain hut's name

    @NotNull
    private String description; //description

    @Column(nullable = false)
    private Boolean sleepover;

    @Column(nullable = false)
    private Boolean food;

    @Column(nullable = false)
    private Boolean water;

    @Column(nullable = false)
    private Boolean electricity;

    @ManyToMany
    private Collection<HykeUser> visitRequests; //users that request visit confirmation

    public Collection<HykeUser> getVisitRequests() {
        return visitRequests;
    }

    public void setVisitRequests(Collection<HykeUser> visitRequests) {
        this.visitRequests = visitRequests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSleepover() {
        return sleepover;
    }

    public void setSleepover(Boolean sleepover) {
        this.sleepover = sleepover;
    }

    public Boolean getFood() {
        return food;
    }

    public void setFood(Boolean food) {
        this.food = food;
    }

    public Boolean getWater() {
        return water;
    }

    public void setWater(Boolean water) {
        this.water = water;
    }

    public Boolean getElectricity() {
        return electricity;
    }

    public void setElectricity(Boolean electricity) {
        this.electricity = electricity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MountainHut)) return false;
        MountainHut that = (MountainHut) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(location, that.location) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, location, name);
    }
}
