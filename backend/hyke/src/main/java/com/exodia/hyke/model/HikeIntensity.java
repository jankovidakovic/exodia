package com.exodia.hyke.model;

import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Represents the intensity of the hiking trip.
 * @author jankovidakovic
 */

public enum HikeIntensity {

    LOW("LOW"), //easy, short hikes
    MODERATE("MODERATE"), //standard hikes, expected to be doable for the majority of amateur hikers
    HIGH("HIGH"), //more challenging hikes, which take more condition to complete
    VERY_HIGH("VERY_HIGH"); //extreme hikes, only for the fittest of hikers

    private final String label;

    private HikeIntensity (String label)
    {
        this.label = label;
    }

    public String label ()
    {
        return this.label;
    }
}
