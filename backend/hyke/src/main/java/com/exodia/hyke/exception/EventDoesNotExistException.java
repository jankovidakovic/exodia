package com.exodia.hyke.exception;

public class EventDoesNotExistException extends RuntimeException{
	
	public EventDoesNotExistException (String errorMessage)
	    {
	        super (errorMessage);
	    }
}	
