package com.exodia.hyke.dao;

import com.exodia.hyke.model.ArchivedHike;
import com.exodia.hyke.model.Hike;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages persistence of ArchivedHikes.
 * @author a_nakic
 */
public interface ArchivedHikeRepository extends JpaRepository <ArchivedHike, Long>
{
    /**
     * Delete ArchivedHike by Hike
     * @param hike
     */
    public void deleteByHike (Hike hike);
}
