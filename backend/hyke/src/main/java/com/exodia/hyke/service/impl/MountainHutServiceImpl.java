package com.exodia.hyke.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import com.exodia.hyke.dto.minimal.MountainHutMinimalDTO;
import com.exodia.hyke.service.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.dao.LocationRepository;
import com.exodia.hyke.dao.MountainHutRepository;
import com.exodia.hyke.dto.MountainHutDTO;
import com.exodia.hyke.dto.detailed.MountainHutDetailedDTO;
import com.exodia.hyke.exception.LocationDoesNotExistException;
import com.exodia.hyke.exception.MountainHutAlreadyExistException;
import com.exodia.hyke.exception.MountainHutDoesNotExistException;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Location;
import com.exodia.hyke.model.MountainHut;
import com.exodia.hyke.service.EntityMissingException;
import com.exodia.hyke.service.MountainHutService;

@Service
public class MountainHutServiceImpl implements MountainHutService {

    @Autowired
    MountainHutRepository mountainHutRepository;

    @Autowired
    HykeUserRepository hykeUserRepository;

    @Autowired
	private BadgeService badgeService;

    @Override
    public void approveVisitRequest(Long hutId, Long hykeUserId) {
        removeRequestor(hutId, hykeUserId);
        HykeUser user = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow();

        MountainHut hut =  mountainHutRepository
                .findById(hutId)
                .orElseThrow();
        user.getVisitedHuts().add(hut);

        badgeService.generateBadges(user);

        hykeUserRepository.saveAndFlush(user);
    }

	@Override
	public void addRequestor(Long hutId, Long hykeUserId) {
		MountainHut mountainHut = mountainHutRepository.findById(hutId).orElseThrow();
		mountainHut.getVisitRequests().add(hykeUserRepository.getOne(hykeUserId));
		mountainHutRepository.save(mountainHut);
		//TODO - implement defensively
	}

	@Override
    public void removeRequestor(Long hutId, Long hykeUserId) {
    	MountainHut hut;
    	try {
    		hut = mountainHutRepository
					.findById(hutId)
					.orElseThrow();
		}catch(Exception e){
    		throw new NoSuchElementException("Mountain hut does not exist.");
		}
        Iterator<HykeUser> i = hut.getVisitRequests().iterator();
        boolean found = false;
        while(i.hasNext()){
            HykeUser user = i.next();
            if(hykeUserId == user.getId()){
                i.remove();
                found = true;
            }
        }
        if(found == false) throw new NoSuchElementException("This user isn't requesting approval of visit.");
        mountainHutRepository.saveAndFlush(hut);
    }

    @Override
    public List<HykeUser> getVisitRequestors(Long hutId) {
        MountainHut hut = mountainHutRepository
                .findById(hutId)
                .orElseThrow();
        return (List<HykeUser>) hut.getVisitRequests();
    }


	@Autowired
	MountainHutRepository mountainHutRepo;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Override
	public List<MountainHut> listAll() {
		return mountainHutRepo.findAll();
	}

	@Override
	public Optional<MountainHut> findById(Long id) {
		return mountainHutRepo.findById(id);
	}
	
	@Override
	public MountainHut fetch(Long id) {
		return findById(id).orElseThrow(
                () -> new EntityMissingException(MountainHut.class, id)
			);
	}
	
	@Override
	public MountainHut createMountainHut(MountainHutDTO dto) {
		Location location = locationRepository.findByName(dto.getLocationName());
		MountainHut mountainHut = new MountainHut();
		mountainHut.setName(dto.getName());
		mountainHut.setLocation(location);
		mountainHut.setDescription(dto.getDescription());
		
		validate(mountainHut);
        return mountainHutRepo.save(mountainHut);
	}
	private void validate(MountainHut mountainHut) {
		if (mountainHutNameExist(mountainHut.getName())) {
			throw new MountainHutAlreadyExistException("Mountain hut with desired name already exists.");
		}
        Assert.notNull(mountainHut, "MountainHut object must be given.");
        Assert.notNull(mountainHut.getDescription(), "Mountain hut must have a description.");
        Assert.notNull(mountainHut.getLocation(), "Mountain hut must have a location");
        Assert.notNull(mountainHut.getName(), "Mountain hut must have a name.");
        Assert.isNull(mountainHut.getId(), "mountainHut ID must be null, not: " + mountainHut.getId());
    }
	private boolean mountainHutNameExist(String name) {
		return mountainHutRepo.findByName(name) != null;
	}
	
	@Override
	public void updateMountainHut(MountainHutDetailedDTO dto) {
		MountainHut mountainHut = mountainHutRepo.findByName(dto.getName());
		Location location = locationRepository.findByName(dto.getLocation().getName());
		
		if (mountainHut == null) {
			throw new MountainHutDoesNotExistException("Mountain hut with that name does not exist");
		} else {
			if (location != null) {
				mountainHut.setLocation(location);
				mountainHut.setDescription(dto.getDescription());
				mountainHut.setName(dto.getName());
				mountainHutRepo.save(mountainHut);
			} else {
				throw new LocationDoesNotExistException ("Location with that name does not exist");
			}
		}	
	}
	
	@Override
	public void deleteMountainHut(String name) {
		MountainHut mountainHut = mountainHutRepo.findByName(name);
		
		if (mountainHut != null) {
			mountainHutRepo.delete(mountainHut);
		} else {
			throw new MountainHutDoesNotExistException("Mountain hut with that name does not exist");
		}
	}


	@Override
	public List <MountainHutDetailedDTO> fetchMountainHutsByLocation (Long locationId)
	{
		if (locationRepository.findById (locationId).isPresent ()) {
			return mountainHutRepo.findByLocation (locationRepository.findById (locationId).get ())
					.stream ()
					.map (MountainHutDetailedDTO::getDTO)
					.collect(Collectors.toList());
		} else {
			throw new LocationDoesNotExistException ("Location with id does not exist.");
		}
	}
}
