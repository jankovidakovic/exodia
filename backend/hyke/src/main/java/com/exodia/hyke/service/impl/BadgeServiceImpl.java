package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.AcquiredBadgeRepository;
import com.exodia.hyke.dao.BadgeRepository;
import com.exodia.hyke.model.AcquiredBadge;
import com.exodia.hyke.model.Badge;
import com.exodia.hyke.model.BadgeThreshold;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.service.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BadgeServiceImpl implements BadgeService {

    @Autowired
    private BadgeRepository badgeRepository;

    @Autowired
    private AcquiredBadgeRepository acquiredBadgeRepository;

    private BadgeThreshold determineThreshold(int visits) {
        switch(visits) {
            case 1:
                return BadgeThreshold.NOVICE;
            case 5:
                return BadgeThreshold.BEGINNER;
            case 10:
                return BadgeThreshold.INTERMEDIATE;
            case 100:
                return BadgeThreshold.EXPIRIENCED;
            default:
                return null;
        }
    }

    @Override
    public void generateBadges(HykeUser hykeUser) {
        BadgeThreshold threshold = determineThreshold(hykeUser.getVisitedHuts().size());
        if (threshold == null) return;
        Badge badge = badgeRepository.findByThreshold(threshold);
        AcquiredBadge acquiredBadge = new AcquiredBadge();
        acquiredBadge.setBadge(badge);
        acquiredBadge.setCreatedAt(new Date());
        acquiredBadge.setCreator(hykeUser);
        acquiredBadgeRepository.save(acquiredBadge);

    }
}
