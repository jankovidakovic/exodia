package com.exodia.hyke.dto;

import com.exodia.hyke.dto.minimal.HikeMinimalDTO;
import com.exodia.hyke.dto.minimal.MountainHutMinimalDTO;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.MountainHut;

public class WishlistItemDTO {

    private HikeMinimalDTO hike;
    private MountainHutMinimalDTO mountainHut;

    public static WishlistItemDTO getDTO(Hike hike) {
        WishlistItemDTO dto = new WishlistItemDTO();
        dto.setHike(HikeMinimalDTO.getDTO(hike));
        return dto;
    }

    public static WishlistItemDTO getDTO(MountainHut mountainHut) {
        WishlistItemDTO dto = new WishlistItemDTO();
        dto.setMountainHut(MountainHutMinimalDTO.getDTO(mountainHut));
        return dto;
    }

    public HikeMinimalDTO getHike() {
        return hike;
    }

    public void setHike(HikeMinimalDTO hike) {
        this.hike = hike;
    }

    public MountainHutMinimalDTO getMountainHut() {
        return mountainHut;
    }

    public void setMountainHut(MountainHutMinimalDTO mountainHut) {
        this.mountainHut = mountainHut;
    }
}
