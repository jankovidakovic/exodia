package com.exodia.hyke.dao;

import com.exodia.hyke.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



@Repository //for exception translation
public interface HykeUserRepository extends JpaRepository<HykeUser, Long> {

    //all exceptions thrown by hibernate will be translated into
    //DataAccessException

    @Query("select u from HykeUser u where u.username = ?1 and u.id <> ?2")
    HykeUser findByUsernameAndIdNot(String username, Long id);

    @Query("select u from HykeUser u where u.email = ?1 and u.id <> ?2")
    HykeUser findByEmailAndIdNot(String email, Long id);

    @Query("SELECT ab FROM AcquiredBadge ab WHERE ab.creator = ?1")
    List<AcquiredBadge> getBadgesById(HykeUser hykeUser);

    @Modifying
    @Transactional
    @Query("Update Post p set creator=NULL where creator=?1")
    void setPostCreatorNull(HykeUser user);

    @Modifying
    @Transactional
    @Query("Update DutyRequest dr set dutyRequester=NULL where dutyRequester=?1")
    void setDutyRequesterNull(HykeUser user);

    @Modifying
    @Transactional
    @Query("Update UserReport ur set reporter=NULL where reporter=?1")
    void setReporterNull(HykeUser user);

    @Modifying
    @Transactional
    @Query("Update Invite i set invitedUser=NULL where invitedUser=?1")
    void setInvitedUserNull(HykeUser user);

    @Modifying
    @Transactional
    @Query("Update FriendRequest fr set sender=NULL where sender=?1")
    void setFriendRequestSenderNull(HykeUser user);

    @Modifying
    @Transactional
    @Query("Update FriendRequest fr set receiver=NULL where receiver=?1")
    void setFriendRequestReceiverNull(HykeUser user);

    @Query("SELECT u from HykeUser u where u.username = ?1")
    HykeUser findByUsername(String username);

    HykeUser findByEmail(String email);

    /**
     * Checks if a user with a given username exists
     * @return true or false
     */
    @Query(value = "SELECT count(username) > 0 FROM hyke_user WHERE username=:username_param", nativeQuery = true)
    boolean existsWithUsername (@Param("username_param") String username);

    @Transactional
    @Modifying
    @Query("update HykeUser hu set hu.role = ?2 where hu.id = ?1")
    void changeRole(Long hykeUserId, Role role);

    @Transactional
    @Modifying
    @Query(value="delete from hyke_user_friends where" +
            " hyke_user_id = :hyke_user_id", nativeQuery = true)
    void setFriendUserNull(@Param("hyke_user_id")Long hykeUserId);

    @Transactional
    @Modifying
    @Query(value = "delete from hyke_user_friends where friends_id" +
            " = :friends_id", nativeQuery = true)
    void setUserFriendNull(@Param("friends_id") Long hykeUserId);


    @Query("Select inv from Invite inv where invitedUser=?1 and status=?2")
    List<Invite> getInvitesByInvitedUserAndStatus(HykeUser hu, Status status);
}
