package com.exodia.hyke.service;

import com.exodia.hyke.dto.PostDTO;

import java.util.Date;
import java.util.List;

/**
 * Manages News Feed.
 * @author a_nakic
 */
public interface NewsFeedService
{
    /**
     * Fetches all Posts created by specified user after lastFetched
     * @param userId
     * @return List of Posts
     */
    public List <PostDTO> fetchNewsFeedByCreator (Long userId);
}