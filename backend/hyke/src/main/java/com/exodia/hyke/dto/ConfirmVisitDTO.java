package com.exodia.hyke.dto;

import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Status;

import java.lang.module.Configuration;

public class ConfirmVisitDTO {

    private UserMinimalDTO user;
    private Status status;

    public UserMinimalDTO getUser() {
        return user;
    }

    public void setUser(UserMinimalDTO user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static ConfirmVisitDTO getDTO(HykeUser user){
        ConfirmVisitDTO dto = new ConfirmVisitDTO();
        dto.setStatus(Status.PENDING);
        dto.setUser(UserMinimalDTO.getDTO(user));

        return dto;
    }
}
