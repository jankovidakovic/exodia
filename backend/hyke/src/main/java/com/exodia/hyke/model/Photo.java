package com.exodia.hyke.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
public class Photo extends Post {

    @Column(nullable = false)
    private String filesystemLocation;

    @Column(nullable = false)
    private String description;

    public String getFilesystemLocation() {
        return filesystemLocation;
    }

    public void setFilesystemLocation(String filesystemLocation) {
        this.filesystemLocation = filesystemLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Photo)) return false;
        Photo photo = (Photo) o;
        return Objects.equals(filesystemLocation, photo.filesystemLocation) &&
                Objects.equals(description, photo.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filesystemLocation, description);
    } //TODO - hashCode, equals
}
