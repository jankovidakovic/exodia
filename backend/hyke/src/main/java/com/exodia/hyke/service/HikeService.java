package com.exodia.hyke.service;

import com.exodia.hyke.dto.ReportDTO;
import com.exodia.hyke.dto.form.HikeFormDTO;
import com.exodia.hyke.exception.DuplicateRatingException;
import com.exodia.hyke.exception.HikeAlreadyExistsException;
import com.exodia.hyke.exception.HikeDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.UserReport;

import java.util.List;
import java.util.Optional;

/**
 * Manages Hikes.
 * @author jankovidakovic a_nakic
 */
public interface HikeService {

    /**
     * Finds all hikes with the matching publicity value.
     * @param publicity
     * @return List of hikes
     */
    List <Hike> findByPublicity (Boolean publicity);

    List<Hike> findAll();

    /**
     * Finds hike with specified id, if it exists.
     * @param id
     * @throws HikeDoesNotExistException if no hike matches the id
     * @return single matching hike
     */
    Hike findById (Long id);

    /**
     * Finds all hikes that have been created by the user with the specified id.
     * @param id of a user
     * @throws UserDoesNotExistException if no user matches id
     * @return list of matching hikes
     */
    List <Hike> findByCreatorId (Long id);

    /**
     * Creates a new hike.
     * @param hikeFormDTO
     * @throws HikeAlreadyExistsException if the same hike already exists in the database.
     */
    void createHike (HikeFormDTO hikeFormDTO);

    /**
     * Deletes a hike with a matching id.
     * @param id of a hike
     * @throws HikeDoesNotExistException if no hike matches id
     */
    void deleteHike (Long id);

    List<ReportDTO> getReports(Long hikeId);

    void deleteReport(Long hikeId, Long reportId);

    List<UserReport> getReportedHikes();

    /**
     * Creates a rating.
     * @param hikeId of the hike which is being rated
     * @param userId of the user who is rating
     * @param value
     * @throws UserDoesNotExistException if no user matches the id
     * @throws HikeDoesNotExistException if no hike matches the id
     * @throws DuplicateRatingException if the user already rated the hike
     */
    void rate (Long hikeId, Long userId, Integer value);

    /**
     * Crates a report.
     * @param hikeId of the hike which is being reported
     * @param userId of the user who is reporting
     * @param report description
     * @throws UserDoesNotExistException if no user matches the id
     * @throws HikeDoesNotExistException if no hike matches the id
     */
    ReportDTO report (Long hikeId, Long userId, String report);

    double getAverageRating(Long hikeId);
}
