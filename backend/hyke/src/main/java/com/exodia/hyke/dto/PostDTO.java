package com.exodia.hyke.dto;

import com.exodia.hyke.dto.minimal.BadgeMinimalDTO;
import com.exodia.hyke.dto.minimal.EventMinimalDTO;
import com.exodia.hyke.dto.minimal.HikeMinimalDTO;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.AcquiredBadge;
import com.exodia.hyke.model.Event;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.Photo;
import org.springframework.security.core.parameters.P;

import java.util.Date;

/**
 * GET /users/{userId}/news, dobiva se polje ovoga kao odgovor
 *  //TODO - discuss
 */
public class PostDTO implements Comparable <PostDTO>
{
    private Long id;
    private Date createdAt; //filter by this to fetch only most recent news, store last fetched timestamp
    private UserMinimalDTO creator;
    private String description;

    private HikeMinimalDTO hike;
    private EventMinimalDTO event;
    private PhotoDTO photo;
    private BadgeMinimalDTO badge;

    @Override
    public int compareTo (PostDTO postDTO)
    {
        if (postDTO.getCreatedAt () == null) {
            return 1;
        }

        return this.createdAt.compareTo (postDTO.getCreatedAt ());
    }

    public static <T> PostDTO getDTO (T post)
    {
        PostDTO postDTO = new PostDTO ();

        if (post instanceof Hike) {
            postDTO.setId (((Hike) post).getId ());
            postDTO.setCreatedAt (((Hike) post).getCreatedAt ());
            postDTO.setCreator (UserMinimalDTO.getDTO (((Hike) post).getCreator ()));
            postDTO.setDescription (((Hike) post).getDescription ());

            postDTO.setHike (HikeMinimalDTO.getDTO ((Hike) post));
        } else if (post instanceof Event) {
            postDTO.setId (((Event) post).getId ());
            postDTO.setCreatedAt (((Event) post).getCreatedAt ());
            postDTO.setCreator (UserMinimalDTO.getDTO (((Event) post).getCreator ()));
            postDTO.setDescription (((Event) post).getDescription ());

            postDTO.setEvent(EventMinimalDTO.getDTO ((Event) post));
        } else if (post instanceof AcquiredBadge) {
            postDTO.setId (((AcquiredBadge) post).getId ());
            postDTO.setCreatedAt (((AcquiredBadge) post).getCreatedAt ());
            postDTO.setCreator (UserMinimalDTO.getDTO (((AcquiredBadge) post).getCreator ()));
            postDTO.setDescription (((AcquiredBadge) post).getBadge ().getDescription ());

            postDTO.setBadge (BadgeMinimalDTO.getDTO ((AcquiredBadge) post));
        } else if (post instanceof Photo) {
            postDTO.setId (((Photo) post).getId ());
            postDTO.setCreatedAt (((Photo) post).getCreatedAt ());
            postDTO.setCreator (UserMinimalDTO.getDTO (((Photo) post).getCreator ()));
            postDTO.setDescription (((Photo) post).getDescription ());

            postDTO.setPhoto (PhotoDTO.getDTO ((Photo) post));
        }

        return postDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public UserMinimalDTO getCreator() {
        return creator;
    }

    public void setCreator(UserMinimalDTO creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HikeMinimalDTO getHike() {
        return hike;
    }

    public void setHike(HikeMinimalDTO hike) {
        this.hike = hike;
    }

    public EventMinimalDTO getEvent() {
        return event;
    }

    public void setEvent(EventMinimalDTO event) {
        this.event = event;
    }

    public PhotoDTO getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoDTO photo) {
        this.photo = photo;
    }

    public BadgeMinimalDTO getBadge() {
        return badge;
    }

    public void setBadge(BadgeMinimalDTO badge) {
        this.badge = badge;
    }
}
