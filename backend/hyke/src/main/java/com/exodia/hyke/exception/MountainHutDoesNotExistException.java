package com.exodia.hyke.exception;

/**
 * Exception that is thrown when a MountainHut, that should exist, does not exist in the database.
 * @author a_nakic
 */

public class MountainHutDoesNotExistException extends RuntimeException
{
    public MountainHutDoesNotExistException (String errorMessage)
    {
        super (errorMessage);
    }
}
