package com.exodia.hyke.dto.detailed;

import com.exodia.hyke.dto.minimal.LocationMinimalDTO;
import com.exodia.hyke.dto.minimal.MountainHutMinimalDTO;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HikeIntensity;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * GET /hikes/{hikeId}
 */
public class HikeDetailedDTO {

    private UserMinimalDTO creator;
    private Date createdAt;
    private String description;
    private LocationMinimalDTO location;
    private String name;
    private String hikeStart;
    private String hikeEnd;
    private Double hikeLength;
    private HikeIntensity hikeIntensity;
    private Boolean publicity;
    private Collection<MountainHutMinimalDTO> mountainHuts;

    public static HikeDetailedDTO getDTO(Hike hike) {
        HikeDetailedDTO dto = new HikeDetailedDTO();
        dto.setCreator(UserMinimalDTO.getDTO(hike.getCreator()));
        dto.setCreatedAt(hike.getCreatedAt());
        dto.setDescription(hike.getDescription());
        dto.setLocation(LocationMinimalDTO.getDTO(hike.getLocation()));
        dto.setName(hike.getName());
        dto.setHikeStart(hike.getHikeStart());
        dto.setHikeEnd(hike.getHikeEnd());
        dto.setHikeLength(hike.getHikeLength());
        dto.setHikeIntensity(hike.getIntensity());
        dto.setPublicity(hike.getPublicity());
        dto.setMountainHuts(hike.getMountainHuts()
                .stream()
                .map(MountainHutMinimalDTO::getDTO)
                .collect(Collectors.toList())
        );
        return dto;

    }

    public UserMinimalDTO getCreator() {
        return creator;
    }

    public void setCreator(UserMinimalDTO creator) {
        this.creator = creator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocationMinimalDTO getLocation() {
        return location;
    }

    public void setLocation(LocationMinimalDTO location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHikeStart() {
        return hikeStart;
    }

    public void setHikeStart(String hikeStart) {
        this.hikeStart = hikeStart;
    }

    public String getHikeEnd() {
        return hikeEnd;
    }

    public void setHikeEnd(String hikeEnd) {
        this.hikeEnd = hikeEnd;
    }

    public Double getHikeLength() {
        return hikeLength;
    }

    public void setHikeLength(Double hikeLength) {
        this.hikeLength = hikeLength;
    }

    public HikeIntensity getHikeIntensity() {
        return hikeIntensity;
    }

    public void setHikeIntensity(HikeIntensity hikeIntensity) {
        this.hikeIntensity = hikeIntensity;
    }

    public Boolean getPublicity() {
        return publicity;
    }

    public void setPublicity(Boolean publicity) {
        this.publicity = publicity;
    }

    public Collection<MountainHutMinimalDTO> getMountainHuts() {
        return mountainHuts;
    }

    public void setMountainHuts(Collection<MountainHutMinimalDTO> mountainHuts) {
        this.mountainHuts = mountainHuts;
    }
}
