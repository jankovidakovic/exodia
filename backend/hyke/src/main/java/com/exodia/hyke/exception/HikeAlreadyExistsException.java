package com.exodia.hyke.exception;

/**
 * Exception that is thrown when a hike, that should not exist, exists in the database.
 * @author a_nakic
 */

public class HikeAlreadyExistsException extends RuntimeException
{
    public HikeAlreadyExistsException (String errorMessage)
    {
        super (errorMessage);
    }
}
