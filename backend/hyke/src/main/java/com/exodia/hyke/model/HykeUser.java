package com.exodia.hyke.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Objects;

/**
 * Single user of the application. Can be uniquely identified by internal ID, username or email.
 * All user fields are required and cannot be left out. User can have different roles in the  application,
 * which provide him with access to different parts of the system.
 * @see Role
 * @author jankovidakovic
 */
@Entity
public class HykeUser {

    @Id
    @GeneratedValue
    private Long id; //unique ID of the entity instance

    @Column(unique = true)
    @NotNull
    private String username; //username which is public on the application

    @NotNull
    private String firstName; //user's first name

    @NotNull
    private String lastName; //user's last name

    @NotNull
    private Role role; //application role

    @Column(unique = true)
    private String email; //user's email

    @NotNull
    private String password; //user's password, which is never stored as plaintext

    @ManyToMany
    private Collection<HykeUser> friends;

    @ManyToMany
    private Collection<MountainHut> mountainHutsWishlist;

    @ManyToMany
    private Collection<MountainHut> visitedHuts; //CREATE NEW BADGES UPON BADGE FETCHING
        //the thing is, if it's a collection, does that mean that one mountain hut
        //can be saved multiple times? -> THAT WOULD BE EXTREMELY GUCCI

    @ManyToMany
    private Collection<MountainHut> ratedMountainHuts; //same problem (uniqueness?)
        //TODO - ^ this is not specified in zahtjev

    @ManyToMany
    private Collection<Hike> ratedHikes; //same uniqueness question

    @ManyToMany
    private Collection<Hike> hikesWishlist; //todo - turn into set

    //@OneToMany//jedan dom moze imati samo jednog dezurnog planinara
    //private Collection<MountainHut> duties; //todo - turn into set

    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<ArchivedHike> archivedHikes; //todo - change to set?

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HykeUser hykeUser = (HykeUser) o;
        return getUsername().equals(hykeUser.getUsername()) && getRole() == hykeUser.getRole() && getEmail().equals(hykeUser.getEmail()) && getPassword().equals(hykeUser.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getRole(), getEmail(), getPassword());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<HykeUser> getFriends() {
        return friends;
    }

    public void setFriends(Collection<HykeUser> friends) {
        this.friends = friends;
    }

    public Collection<MountainHut> getMountainHutsWishlist() {
        return mountainHutsWishlist;
    }

    public void setMountainHutsWishlist(Collection<MountainHut> mountainHutsWishlist) {
        this.mountainHutsWishlist = mountainHutsWishlist;
    }

    public Collection<Hike> getHikesWishlist() {
        return hikesWishlist;
    }

    public void setHikesWishlist(Collection<Hike> hikesWishlist) {
        this.hikesWishlist = hikesWishlist;
    }


    public Collection<ArchivedHike> getArchivedHikes() {
        return archivedHikes;
    }

    public void setArchivedHikes(Collection<ArchivedHike> archivedHikes) {
        this.archivedHikes = archivedHikes;
    }

    public Collection<MountainHut> getVisitedHuts() {
        return visitedHuts;
    }

    public void setVisitedHuts(Collection<MountainHut> visitedHuts) {
        this.visitedHuts = visitedHuts;
    }
}
