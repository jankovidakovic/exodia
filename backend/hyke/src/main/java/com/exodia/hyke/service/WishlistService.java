package com.exodia.hyke.service;

import com.exodia.hyke.dto.WishlistItemDTO;
import com.exodia.hyke.dto.WishlistUpdateDTO;

import java.util.List;

public interface WishlistService
{
    public List <WishlistItemDTO> fetchWishlist (Long userId);

    public void addHike (Long hikeId, Long userId);

    public void addMountainHut (Long mountainHutId, Long userId);

    public void removeHike (Long hikeId, Long userId);

    public void removeMountainHut (Long mountainHutId, Long userId);
}
