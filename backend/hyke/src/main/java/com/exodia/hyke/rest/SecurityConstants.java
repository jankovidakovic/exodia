package com.exodia.hyke.rest;

public class SecurityConstants {
    public static final String SECRET = "blablabla";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/register";
    public static final String LOGIN_URL = "/login";
    public static final String PUBLIC_HIKES = "/hikes/**";
    public static final String MOUNTAIN_HUTS = "/mountain-huts/**";
}