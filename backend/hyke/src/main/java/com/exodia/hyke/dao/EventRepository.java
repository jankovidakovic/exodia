package com.exodia.hyke.dao;

import com.exodia.hyke.model.Event;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HykeUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Repository that manages persistence of Events.
 * @author a_nakic
 */
public interface EventRepository extends JpaRepository<Event, Long>
{
    /**
     * Delete Event by Hike
     * @param hike
     */
    public void deleteByHike (Hike hike);
    
    /**
     * Find Event by creator
     * @param hykeUser
     */
	public List<Event> findByCreator(HykeUser hykeUser);

	public Optional<Event> findById(Long eventId);

}
