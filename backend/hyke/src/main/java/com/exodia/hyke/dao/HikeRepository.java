package com.exodia.hyke.dao;

import com.exodia.hyke.dto.detailed.HikeDetailedDTO;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.UserReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import com.exodia.hyke.model.HykeUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Repository that manages persistence of Hikes.
 * @author jankovidakovic a_nakic
 */
public interface HikeRepository extends JpaRepository<Hike, Long> {

    /**
     * Searches for all Hikes that are made to be public.
     * @return List of public hikes.
     */
    List<Hike> findByPublicityTrue();

    @Query("Select ur from UserReport ur where ur.hike = ?1")
    List<UserReport> getReports(Hike hike);

    @Query("Select ur from UserReport ur where ur.id = ?1")
    Optional<UserReport> findReportById(Long reportId);

    @Transactional
    @Modifying
    @Query("delete from UserReport ur where ur.id = ?1")
    void deleteReport(Long reportId);

    @Query("Select ur from UserReport ur")
    List<UserReport> getUserReports();
    List <Hike> findByPublicity (Boolean publicity);

    /**
     * Searches for all Hikes which have a specified creator.
     * @param creator
     * @return List of hikes.
     */
    List <Hike> findByCreator (HykeUser creator);

    List<Hike> findByCreatorAndPublicity(HykeUser creator, Boolean publicity);

    @Query("Select h from Hike h where h.name = ?1")
    Hike findByName(String name);

//    /**
//     * Checks if entries with specified hash exist.
//     * @param hash
//     * @return true if condition satisfied, else false
//     */
//    Boolean existsByHash (Integer hash);
}
