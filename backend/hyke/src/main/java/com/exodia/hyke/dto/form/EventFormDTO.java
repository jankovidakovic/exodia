package com.exodia.hyke.dto.form;

import com.exodia.hyke.dto.IdDTO;

import java.util.Date;

/**
 * POST /events, ovo se predaje u bodyju
 */
public class EventFormDTO {

    private IdDTO creator;
    private String description;
    private IdDTO hike;
    private Date startDateTime;
    private Date endDateTime;
    
	public IdDTO getCreator() {
		return creator;
	}
	public void setCreator(IdDTO creator) {
		this.creator = creator;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public IdDTO getHike() {
		return hike;
	}
	public void setHike(IdDTO hike) {
		this.hike = hike;
	}
	public Date getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}
	public Date getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

}
