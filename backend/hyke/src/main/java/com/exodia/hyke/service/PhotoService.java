package com.exodia.hyke.service;

import com.exodia.hyke.dto.form.PhotoFormDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Manages Photos.
 * @author a_nakic
 */
public interface PhotoService
{
    /**
     * Saves image.
     * @param image
     * @return location of saved image
     * @throws IOException
     */
    public String saveImage (MultipartFile image) throws IOException;

    /**
     * Creates a Post based on PhotoFormDTO and saves it.
     * @param photoFormDTO
     */
    public void uploadPhotoPost (PhotoFormDTO photoFormDTO);
}
