package com.exodia.hyke.dto.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.exodia.hyke.validation.MatchingPassword;
import com.exodia.hyke.validation.ValidEmail;

/**
 * Object which contains only the HykeUser information
 * relevant for the API access and successful account creation
 * @author Roko Grbelja
 */
@MatchingPassword

/**
 * POST /users/register, ovo se predaje u bodyju
 */
public class UserRegistrationDTO {
	
	@NotNull
	@NotEmpty
	private String firstName;
	
	@NotNull
	@NotEmpty
	private String lastName;
	
	@NotNull
	@NotEmpty
	private String username;
	
	@NotNull
	@NotEmpty
	@ValidEmail
	private String email;
	
	@NotNull
	@NotEmpty
	private String password;
	private String confirmPassword;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	
}
