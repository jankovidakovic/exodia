package com.exodia.hyke.dto;

import com.exodia.hyke.model.MountainHut;

public class MountainHutDTO {
	
	private Long id;
	private String locationName;
	private String name;
	private String description;
	
	public static MountainHutDTO getDTO(MountainHut mountainHut) {
		MountainHutDTO mountainHutDTO = new MountainHutDTO();
		mountainHutDTO.setName(mountainHut.getName());
		mountainHutDTO.setLocationName(mountainHut.getLocation().getName());
		mountainHutDTO.setDescription(mountainHut.getDescription());
		mountainHutDTO.setId(mountainHut.getId());
		return mountainHutDTO;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
		
}
