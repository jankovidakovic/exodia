package com.exodia.hyke.dto.minimal;

import com.exodia.hyke.model.HykeUser;

/**
 * GET /users, dobiva se polje ovoga
 */
public class UserMinimalDTO {

    private Long id;
    private String firstName;
    private String lastName;

    public static UserMinimalDTO getDTO(HykeUser hykeUser) {
        UserMinimalDTO dto = new UserMinimalDTO();
        if(hykeUser == null) {
            dto.setFirstName(null);
            dto.setLastName(null);
            dto.setId(null);
            return dto;
        }
        dto.setFirstName(hykeUser.getFirstName());
        dto.setLastName(hykeUser.getLastName());
        dto.setId(hykeUser.getId());
        return dto;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
