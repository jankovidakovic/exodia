package com.exodia.hyke.rest;

import com.exodia.hyke.dto.form.PhotoFormDTO;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Controller which takes care of the API endpoint for REST operations regarding Photos.
 * @author a_nakic
 */
@RestController
@RequestMapping("/photos")
public class PhotoController
{
    @Autowired
    private PhotoService photoService;

    /**
     * Used for uploading images.
     * @param image as multipartfile
     * @return path to the image
     */
    @PostMapping("/raw")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity <String> uploadImage (@RequestBody(required = false) MultipartFile image)
    {
        if (image == null) {
            return new ResponseEntity <String> ("No photo sent.", HttpStatus.OK);
        }

        try {
            return new ResponseEntity <String> (photoService.saveImage (image), HttpStatus.CREATED);
        } catch (IOException e) {
            return new ResponseEntity <String> ("IO error.", HttpStatus.CONFLICT);
        }
    }

    /**
     * Used for posting Photos with existing locations.
     * @param photoFormDTO
     * @return informational ResponseEntity
     */
    @PostMapping("/data")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity <String> postPhoto (@RequestBody(required = false) PhotoFormDTO photoFormDTO)
    {
        if (photoFormDTO == null) {
            return new ResponseEntity <String> ("No request.", HttpStatus.OK);
        }

        try {
            photoService.uploadPhotoPost(photoFormDTO);
        } catch (UserDoesNotExistException e) {
            return new ResponseEntity <String> ("User with id does not exist.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity <String> ("Success.", HttpStatus.OK);
    }
}