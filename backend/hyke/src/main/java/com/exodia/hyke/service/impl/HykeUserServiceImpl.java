package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.FriendRequestRepository;
import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.dto.ConfirmVisitDTO;
import com.exodia.hyke.dto.FriendRequestDTO;
import com.exodia.hyke.dto.detailed.UserDetailedDTO;
import com.exodia.hyke.dto.form.ChangePasswordDTO;
import com.exodia.hyke.dto.form.UpdateInfoFormDTO;
import com.exodia.hyke.dto.UserRoleDTO;
import com.exodia.hyke.dto.form.UserRegistrationDTO;
import com.exodia.hyke.dto.form.UserUpdateFormDTO;
import com.exodia.hyke.dto.minimal.EventMinimalDTO;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.*;
import com.exodia.hyke.service.HykeUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

@Service
public class HykeUserServiceImpl implements HykeUserService {

    @Autowired
    private HykeUserRepository hykeUserRepository;

    @Autowired
    private FriendRequestRepository friendRequestRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<HykeUser> listAll() {
        return hykeUserRepository.findAll();
    }

    @Override
    public HykeUser createHykeUser(HykeUser hykeUser) {
        return null;
    }

    @Override
    public HykeUser registerNewHykeUser(UserRegistrationDTO userDto) {
        return null;
    }

    @Override
    public void updateInfo(Long hykeUserId, UpdateInfoFormDTO updateInfoFormDTO) {

        //try to fetch the user with given ID
        HykeUser hykeUser = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow(); //throws NoSuchElementException

        HykeUser uniqueUser = hykeUserRepository.findByUsernameAndIdNot(
                updateInfoFormDTO.getUsername(),
                hykeUserId
        ); //check username unique constraint
        Assert.isNull(uniqueUser, "Odabrano korisničko ime je zauzeto.");

        uniqueUser = hykeUserRepository.findByEmailAndIdNot(
                updateInfoFormDTO.getEmail(),
                hykeUserId
        ); //check email unique constraint
        Assert.isNull(uniqueUser, "Odabrani e-mail je zauzet.");

        //TODO - validate email pattern on backend
        //TODO - validate password pattern on backend

        //update hyke user
        hykeUser.setUsername(updateInfoFormDTO.getUsername());
        hykeUser.setFirstName(updateInfoFormDTO.getFirstName());
        hykeUser.setLastName(updateInfoFormDTO.getLastName());
        hykeUser.setEmail(updateInfoFormDTO.getEmail());

        //persist the user
        hykeUserRepository.saveAndFlush(hykeUser); //TODO - catch exception?

    }

    @Override
    public void changePassword(Long hykeUserId, ChangePasswordDTO changePasswordDTO) {

        //try to fetch the user with given ID
        HykeUser hykeUser = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow(); //throws NoSuchElementException

        if (!passwordEncoder.matches(changePasswordDTO.getOldPassword(), hykeUser.getPassword())) {
            throw new IllegalArgumentException("Pogrešna lozinka.");
        }

        //assume password validation was done at the frontend
        hykeUser.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
        hykeUserRepository.saveAndFlush(hykeUser);
    }


    @Override
    public HykeUser getHykeUser(Long hykeUserId){
        HykeUser user = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow();

        return user;
    }

    @Override
    public List<AcquiredBadge> getUserBadges(Long hykeUserId){

        HykeUser hykeUser = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow(); //throws NoSuchElementException

        List<AcquiredBadge> badges = hykeUserRepository.getBadgesById(hykeUser);
        return badges;
    }

    @Override
    public void deleteUser(Long hykeUserId) {
        HykeUser user = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow();
        //removes data connected to the user from
        //all tables that contain it


        hykeUserRepository.setPostCreatorNull(user);
        hykeUserRepository.setDutyRequesterNull(user);
        hykeUserRepository.setReporterNull(user);
        hykeUserRepository.setInvitedUserNull(user);
        hykeUserRepository.setFriendRequestSenderNull(user);
        hykeUserRepository.setFriendRequestReceiverNull(user);
        hykeUserRepository.setFriendUserNull(hykeUserId);
        hykeUserRepository.setUserFriendNull(hykeUserId);
        hykeUserRepository.delete(user); //deletes user

    }
    public HykeUser getHykeUserWithUsername(String username) {
        HykeUser user = hykeUserRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchElementException();
        }
        return user;

    }


    public void changeRole(Long hykeUserId, UserRoleDTO userRoleDTO) {
        HykeUser hykeUser = hykeUserRepository
                .findById(hykeUserId)
                .orElseThrow(); //throws NoSuchElementException
        hykeUserRepository.changeRole(hykeUserId, userRoleDTO.getRole());
	}

    @Override
    public void sendFriendRequest(FriendRequestDTO frDTO) {
        HykeUser sender;
        HykeUser receiver;

        try {
            sender = hykeUserRepository
                    .findById(frDTO.getSender())
                    .orElseThrow();
        }catch(Exception e){
            throw new NoSuchElementException("You do not exist.");
        }
        try {
            receiver = hykeUserRepository
                    .findById(frDTO.getReceiver())
                    .orElseThrow();
        }catch(Exception e){
            throw new NoSuchElementException("User you are trying to befriend does not exist.");
        }

        if(sender.getFriends().contains(receiver))
            throw new NoSuchElementException("You are already friends with this person.");
        FriendRequest fr = new FriendRequest();
        fr.setSender(sender);
        fr.setReceiver(receiver);

        if(friendRequestRepository.checkIfExists(sender, receiver).size() != 0
        || friendRequestRepository.checkIfExists(receiver, sender).size() != 0)
            throw new NoSuchElementException("This friendship is already pending approval.");
        friendRequestRepository.saveAndFlush(fr);

    }

    @Override
    public List<UserMinimalDTO> getFriendRequests(Long hykeUserId) {
        HykeUser hu;
        try {
            hu = hykeUserRepository
                    .findById(hykeUserId)
                    .orElseThrow();
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("You do not exist");
        }
        List<FriendRequest> frs = friendRequestRepository.findByReceiver(hu);
        List<UserMinimalDTO> senders = new ArrayList<>();
        for(FriendRequest fr: frs){
            senders.add(UserMinimalDTO.getDTO(fr.getSender()));
        }

        return senders;
    }

    @Override
    public void replyToFriendRequest(Long hykeUserId, ConfirmVisitDTO cvDTO) {
        FriendRequest fr = new FriendRequest();
        HykeUser receiver;
        HykeUser sender;
        try{
            receiver = hykeUserRepository
                    .findById(hykeUserId)
                    .orElseThrow();
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("You do not exist.");
        }

        try{
            sender = hykeUserRepository
                    .findById(cvDTO.getUser().getId())
                    .orElseThrow();
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("The user whose friend " +
                    "request you are trying to answer does not exist.");
        }

        fr.setSender(sender);
        fr.setReceiver(receiver);

        if(friendRequestRepository.checkIfExists(sender, receiver).size() == 0)
            throw new NoSuchElementException("This user has not sent you a friend request.");

        friendRequestRepository.deleteFriendRequest(sender, receiver);
        if(cvDTO.getStatus() == Status.DECLINED) return;
        try {
            sender.getFriends().add(receiver);
        }catch(Exception e){
            throw new NoSuchElementException("You are already friends with this person.");
        }

        try {
            receiver.getFriends().add(sender);
        }catch(Exception e){
            throw new NoSuchElementException("You are already friends with this person.");
        }
        hykeUserRepository.saveAndFlush(sender);
        hykeUserRepository.saveAndFlush(receiver);
    }

    @Override
    public List<UserMinimalDTO> getFriends(Long id) {
        HykeUser hu = hykeUserRepository
                .findById(id)
                .orElseThrow();

        List<UserMinimalDTO> hutDTOs = new ArrayList<>();
        for(HykeUser u: hu.getFriends()){
            hutDTOs.add(UserMinimalDTO.getDTO(u));
        }
        return hutDTOs;
    }

    @Override
    public void removeFriend(Long id, FriendRequestDTO frDTO) {
        HykeUser friendshipEnder;
        HykeUser exFriend;
        try{
            friendshipEnder = hykeUserRepository
                    .findById(frDTO.getSender())
                    .orElseThrow();
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("You don't exist.");
        }

        try{
            exFriend = hykeUserRepository
                    .findById(frDTO.getReceiver())
                    .orElseThrow();
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("That friend doesn't exist.");
        }

        if(friendshipEnder.getFriends().contains(exFriend)){
            friendshipEnder.getFriends().remove(exFriend);
            exFriend.getFriends().remove(friendshipEnder);
            hykeUserRepository.saveAndFlush(friendshipEnder);
            hykeUserRepository.saveAndFlush(exFriend);
        }
        else{
            throw new NoSuchElementException("You are not friends with this person.");
        }

    }

    @Override
    public List<Invite> getMyInvites(Long id) {
        HykeUser invitedUser;
        try{
            invitedUser = hykeUserRepository
                    .findById(id)
                    .orElseThrow();
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("This user does not exist.");
        }

        return hykeUserRepository.getInvitesByInvitedUserAndStatus(invitedUser, Status.PENDING);
    }

    @Override
    public boolean checkFriendShip(Long user1_id, Long user2_id) {
        HykeUser user1;
        HykeUser user2;
        try{
            user1 = hykeUserRepository
                    .findById(user1_id)
                    .orElseThrow();
        }catch(NoSuchElementException ex){
            throw new NoSuchElementException("You don't exist.");
        }
        try{
            user2 = hykeUserRepository
                    .findById(user2_id)
                    .orElseThrow();
        }catch(NoSuchElementException ex){
            throw new NoSuchElementException("This person does not exist.");
        }
        return user1.getFriends().contains(user2);
    }
}
