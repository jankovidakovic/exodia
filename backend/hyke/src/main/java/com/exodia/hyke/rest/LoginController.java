package com.exodia.hyke.rest;

import com.exodia.hyke.dto.form.UserLoginDTO;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.service.UserLoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller which takes care of the API endpoint for REST operations regarding logging in.
 * @author a_nakic
 */

@RestController
@RequestMapping("/login")
public class LoginController
{

    @Autowired
    private UserLoginService userLoginService;

    /**
     * Checks if user with given UserLoginDTO can login and catches the UserDoesNotExistException.
     * @param userLoginDTO
     * @return appropriate ResponseEntity
     */
    @PostMapping("")
    public ResponseEntity <String> userLogin(@RequestBody UserLoginDTO userLoginDTO)
    {
        try {
            if (userLoginService.userLogin(userLoginDTO)) {
                return new ResponseEntity<String>("Success", HttpStatus.OK);
            } else {
                return new ResponseEntity <String> ("Failure", HttpStatus.UNAUTHORIZED);
            }
        } catch (UserDoesNotExistException e) {
            return new ResponseEntity <String> ("User does not exist", HttpStatus.NOT_FOUND);
        }
    }

}