package com.exodia.hyke.dto.detailed;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.exodia.hyke.dto.EventInviteDTO;
import com.exodia.hyke.dto.minimal.HikeMinimalDTO;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.Event;

public class EventDetailedDTO {
	
	private Long id;
    private UserMinimalDTO creator;
    private HikeMinimalDTO hike;
    private Date startDateTime;
    private Date endDateTime;
    private String description;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserMinimalDTO getCreator() {
		return creator;
	}

	public void setCreator(UserMinimalDTO creator) {
		this.creator = creator;
	}

	public HikeMinimalDTO getHike() {
		return hike;
	}

	public void setHike(HikeMinimalDTO hike) {
		this.hike = hike;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static EventDetailedDTO getDTO(Event event) {
    	EventDetailedDTO dto = new EventDetailedDTO();
    	dto.setCreator(UserMinimalDTO.getDTO(event.getCreator()));
    	dto.setDescription(event.getDescription());
    	dto.setEndDateTime(event.getEndDateTime());
    	dto.setStartDateTime(event.getStartDateTime());
    	dto.setHike(HikeMinimalDTO.getDTO(event.getHike()));
    	dto.setId(event.getId());

    	return dto;
    }
}
