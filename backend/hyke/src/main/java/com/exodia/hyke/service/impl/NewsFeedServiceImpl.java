package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.*;
import com.exodia.hyke.dto.PostDTO;
import com.exodia.hyke.model.AcquiredBadge;
import com.exodia.hyke.model.Event;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.Photo;
import com.exodia.hyke.service.NewsFeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class NewsFeedServiceImpl implements NewsFeedService
{
    @Autowired
    private HikeRepository hikeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private AcquiredBadgeRepository acquiredBadgeRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private HykeUserRepository userRepo;

    private Stream <Hike> fetchHikeByCreator (Long userId)
    {
        return hikeRepository
                .findByCreatorAndPublicity (userRepo.findById (userId).get (), true)
                .stream ();
    }

    private Stream <Event> fetchEventByCreator (Long userId)
    {
        return eventRepository
                .findByCreator (userRepo.findById (userId).get ())
                .stream ();
    }

    private Stream <AcquiredBadge> fetchBadgeByCreator (Long userId)
    {
        return acquiredBadgeRepository
                .findByCreator (userRepo.findById (userId).get ())
                .stream ();
    }

    private Stream <Photo> fetchPhotoByCreator (Long userId)
    {
        return photoRepository
                .findByCreator (userRepo.findById (userId).get ())
                .stream ();
    }

    @Override
    public List <PostDTO> fetchNewsFeedByCreator (Long userId)
    {

        return Stream.concat (
                fetchHikeByCreator (userId),
                Stream.concat (userRepo.findById (userId)
                                .get ()
                                .getFriends ()
                                .stream ()
                                .flatMap (x -> Stream.concat (fetchEventByCreator (x.getId ()),
                                        Stream.concat (fetchHikeByCreator (x.getId ()),
                                                Stream.concat (fetchBadgeByCreator (x.getId ()),
                                                fetchPhotoByCreator (x.getId ()))))),
                Stream.concat (fetchEventByCreator (userId),
                Stream.concat(fetchBadgeByCreator (userId),
                fetchPhotoByCreator (userId))))
        )
                .map (PostDTO::getDTO)
            .sorted(Comparator.comparing(PostDTO::getCreatedAt).reversed())
                .collect(Collectors.toList ());

    }
}
