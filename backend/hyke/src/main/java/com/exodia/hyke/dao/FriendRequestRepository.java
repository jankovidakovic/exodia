package com.exodia.hyke.dao;

import com.exodia.hyke.model.FriendRequest;
import com.exodia.hyke.model.HykeUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface FriendRequestRepository extends JpaRepository<FriendRequest, Long> {

    @Query("Select fr from FriendRequest fr where fr.receiver = ?1")
    List<FriendRequest> findByReceiver(HykeUser hykeUser);

    @Query("Select fr from FriendRequest fr where fr.receiver = ?2 and fr.sender = ?1")
    List<FriendRequest> checkIfExists(HykeUser sender, HykeUser receiver);

    @Modifying
    @Transactional
    @Query("Delete from FriendRequest fr where fr.sender = ?1 and fr.receiver = ?2")
    void deleteFriendRequest(HykeUser sender, HykeUser receiver);
}
