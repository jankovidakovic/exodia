package com.exodia.hyke.dto;


import com.exodia.hyke.dto.minimal.UserMinimalDTO;

public class RatingDTO {

    private UserMinimalDTO user;
    private Integer rating; //must be [1,5]

    public UserMinimalDTO getUser () {
        return user;
    }

    public void setUser (UserMinimalDTO user) {
        this.user = user;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
