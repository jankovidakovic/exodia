package com.exodia.hyke.dto.detailed;

import com.exodia.hyke.dto.MountainHutDTO;
import com.exodia.hyke.model.Location;

/**
 * GET /locations/{locationId}
 */
public class LocationDetailedDTO {
    private Long id;
    private String name;
    private String description;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static LocationDetailedDTO getDTO(Location location) {
		LocationDetailedDTO dto = new LocationDetailedDTO();
		dto.setName(location.getName());
		dto.setDescription(location.getDescription());
		dto.setId(location.getId());
		return dto;
	}
    
    
}
