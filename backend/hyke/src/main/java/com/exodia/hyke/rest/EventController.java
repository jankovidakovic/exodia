package com.exodia.hyke.rest;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exodia.hyke.dto.EventInviteDTO;
import com.exodia.hyke.dto.detailed.EventDetailedDTO;
import com.exodia.hyke.dto.form.EventFormDTO;
import com.exodia.hyke.dto.minimal.EventMinimalDTO;
import com.exodia.hyke.exception.EventDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.service.EventService;

@RestController
@RequestMapping("/events")
public class EventController {

	@Autowired
	private EventService eventService;

	@GetMapping("")
	@Secured({ "ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER" })
	public ResponseEntity<List<EventMinimalDTO>> listEvents(@RequestParam(required = false) Long creatorId) {

		if (creatorId != null) {
			try {
				return new ResponseEntity<List<EventMinimalDTO>>(eventService.findByCreatorId(creatorId).stream()
						.map(EventMinimalDTO::getDTO).collect(Collectors.toList()), HttpStatus.OK);

			} catch (UserDoesNotExistException e) {
				return new ResponseEntity<List<EventMinimalDTO>>(Collections.emptyList(), HttpStatus.NOT_FOUND);

			}
		}
		return new ResponseEntity<List<EventMinimalDTO>>((List<EventMinimalDTO>) null, HttpStatus.BAD_REQUEST);
	}

	@PostMapping("")
	@Secured({ "ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER" })
	public ResponseEntity<String> createNewEvent(@RequestBody EventFormDTO eventFormDTO) {
		try {
			eventService.createEvent(eventFormDTO);
			return new ResponseEntity<String>("Success.", HttpStatus.CREATED);
		} catch (IllegalArgumentException e) {
			return new ResponseEntity<String>("Invalid form submission.", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{eventId}")
	@Secured({ "ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER" })
	public ResponseEntity<EventDetailedDTO> getEventById(@PathVariable Long eventId) {
		try {
			return new ResponseEntity<EventDetailedDTO>(EventDetailedDTO.getDTO(eventService.findById(eventId)),
					HttpStatus.OK);

		} catch (EventDoesNotExistException e) {
			return new ResponseEntity<EventDetailedDTO>((EventDetailedDTO) null, HttpStatus.NOT_FOUND);

		}
	}

	// TODO
	@GetMapping("/{eventId}/invites")
	public ResponseEntity<List<EventInviteDTO>> getEventInvites(@PathVariable Long eventId) {
		try {
			return new ResponseEntity<List<EventInviteDTO>>(eventService.findEventInvites(eventId).stream()
					.map(EventInviteDTO::getDTO).collect(Collectors.toList()), HttpStatus.OK);
		} catch (EventDoesNotExistException e) {
			return new ResponseEntity<List<EventInviteDTO>>(Collections.emptyList(), HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/{eventId}/invites")
	public ResponseEntity<String> sendEventInvite(@RequestBody EventInviteDTO eventInviteDTO,
			@PathVariable Long eventId) {
		try {
			return new ResponseEntity<String>(eventService.sendInvite(eventInviteDTO, eventId), HttpStatus.OK);
		} catch (EventDoesNotExistException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/{eventId}/reply")
	public ResponseEntity<String> replyToInvite(@PathVariable("eventId") Long id, @RequestBody EventInviteDTO eiDTO) {
		try {
			eventService.replyToInvite(id, eiDTO);
			return new ResponseEntity<>("Successfully replied to the invite", HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

}
