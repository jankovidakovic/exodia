package com.exodia.hyke.rest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Exception handler for the controller layer. Handles exceptions which can
 * occur during data validation, and returns the appropriate responses for
 * the frontend to render.
 *
 * @author jankovidakovic
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler {

    /**
     * Handles {@code IllegalArgumentException}, which occurs during validation
     * at the service layer, and {@code NoSuchElementException}, which occurs
     * when some non-existing entity is requested (for example, a hyke user is requested
     * for an ID that does not exist.)
     *
     * @param e exception which is thrown by the method
     * @return 400 Bad Request, with the appropriate error message
     * @author jankovidakovic
     */
    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<?> handleIllegalArgument(Exception e) {
        Map<String, String> props = new HashMap<>();
        props.put("message", e.getMessage()); //TODO - change "no value present" error
        props.put("status", "400");
        props.put("error", "bad Request");
        return new ResponseEntity<>(props, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<?> handleNoSuchElement(Exception e) {
        return new ResponseEntity<>("Traženi resurs ne postoji.", HttpStatus.NOT_FOUND);
    }

    /**
     * Handles the {@code InvalidHykeUserException}, which occurs when an attempt is
     * made to update some hyke user with data that is not valid in the context of
     * the application (for example, empty username, or invalid email pattern)
     *
     * @param e exception which is thrown at the point of validation failure
     * @return 400 Bad Request response entity, containing the map of all fields
     * that failed the validation, and their corresponding messages
     * @author jankovidakovic
     */
    @ExceptionHandler(InvalidHykeUserException.class)
    protected ResponseEntity<?> handleInvalidHykeUser(InvalidHykeUserException e) {
        return new ResponseEntity<>(e.getErrorsMap(), HttpStatus.BAD_REQUEST);
    }

}
