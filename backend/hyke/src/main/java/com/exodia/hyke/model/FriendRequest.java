package com.exodia.hyke.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class FriendRequest {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private HykeUser sender;

    @ManyToOne
    private HykeUser receiver;

    //TODO - check mappings

    //ne treba cuvati status jer se friend requestovi cuvaju
    //samo dok su pending


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HykeUser getSender() {
        return sender;
    }

    public void setSender(HykeUser sender) {
        this.sender = sender;
    }

    public HykeUser getReceiver() {
        return receiver;
    }

    public void setReceiver(HykeUser receiver) {
        this.receiver = receiver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FriendRequest)) return false;
        FriendRequest that = (FriendRequest) o;
        return Objects.equals(sender, that.sender) &&
                Objects.equals(receiver, that.receiver);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, receiver);
    }
}
