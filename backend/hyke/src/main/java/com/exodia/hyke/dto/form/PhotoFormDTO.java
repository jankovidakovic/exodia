package com.exodia.hyke.dto.form;

import com.exodia.hyke.dto.PhotoDTO;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;

/**
 * POST /photos, ovo se predaje u bodyju
 */
public class PhotoFormDTO {

    private UserMinimalDTO user;

    //idea is to first send the POST request uploading the
    //photo, to which the appropriate controller will respond
    //with the location of the photo. Then, the response
    //is used to fill up this form on the frontend and
    //send it to another controller via another POST request.
    private PhotoDTO photo;

    public UserMinimalDTO getUser() {
        return user;
    }

    public void setUser(UserMinimalDTO user) {
        this.user = user;
    }

    public PhotoDTO getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoDTO photo) {
        this.photo = photo;
    }
}
