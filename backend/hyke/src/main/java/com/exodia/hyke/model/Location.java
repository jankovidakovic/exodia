package com.exodia.hyke.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Location that describes some geographical region that is popular for hiking, e.g.
 * some mountain range like Velebit or Dinarsko Gorje. Location can be identified by
 * its unique ID.
 * @author jankovidakovic
 */
@Entity
@Table(name = "hyke_location")
public class Location {

    @Id
    @GeneratedValue
    private Long id; //unique id by which the location can be identified

    @NotNull
    private String name; //name of the location

    @NotNull
    private String description; //description of the location

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return name.equals(location.name) &&
            description.equals(location.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }
}
