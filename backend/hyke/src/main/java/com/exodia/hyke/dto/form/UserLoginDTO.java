package com.exodia.hyke.dto.form;


/**
 * Data transfer object for the purpose of logging in.
 * Contains the username and password.
 * @author a_nakic
 */

/**
 * POST /users/login, ovo se predaje u bodyju
 */
public class UserLoginDTO
{
    private String username;

    private String password;

    public String getUsername ()
    {
        return username;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }
}
