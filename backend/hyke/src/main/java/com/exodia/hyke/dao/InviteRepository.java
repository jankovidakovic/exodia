package com.exodia.hyke.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exodia.hyke.model.Invite;

public interface InviteRepository extends JpaRepository<Invite, Long> {
	
	List<Invite> findByEventId(Long eventId);
	
	Optional<Invite> findByIdAndEventId(Long id, Long eventId);
}
