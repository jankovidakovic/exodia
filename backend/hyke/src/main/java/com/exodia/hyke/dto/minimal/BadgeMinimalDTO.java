package com.exodia.hyke.dto.minimal;

import com.exodia.hyke.model.AcquiredBadge;
import com.exodia.hyke.model.Badge;

import java.util.ArrayList;
import java.util.List;

public class BadgeMinimalDTO {

    private String fileSystemLocation;
    private String name;

    public static BadgeMinimalDTO getDTO (AcquiredBadge badge)
    {
        BadgeMinimalDTO badgeMinimalDTO = new BadgeMinimalDTO ();
        badgeMinimalDTO.setName(badge.getBadge().getName());
        badgeMinimalDTO.setFileSystemLocation(badge.getBadge().getFileSystemLocation());


        return badgeMinimalDTO;
    }



    public String getFileSystemLocation() {
        return fileSystemLocation;
    }

    public void setFileSystemLocation(String photoLocation) {
        this.fileSystemLocation = photoLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<BadgeMinimalDTO> getBadgeMinimalDTOS (List<AcquiredBadge> badges){
            List<BadgeMinimalDTO> list = new ArrayList<>();
            for(AcquiredBadge badge: badges){
                BadgeMinimalDTO badgeMinimalDTO = new BadgeMinimalDTO();
                badgeMinimalDTO.setName(badge.getBadge().getName());
                badgeMinimalDTO.setFileSystemLocation(badge.getBadge().getFileSystemLocation());


                list.add(badgeMinimalDTO);
            }

            return list;

    }
}
