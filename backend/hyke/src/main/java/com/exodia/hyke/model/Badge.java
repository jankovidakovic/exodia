package com.exodia.hyke.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
public class Badge { //TODO - hashCode, equals

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String fileSystemLocation;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private BadgeThreshold threshold;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileSystemLocation() {
        return fileSystemLocation;
    }

    public void setFileSystemLocation(String fileSystemLocation)
    {
        this.fileSystemLocation = fileSystemLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BadgeThreshold getThreshold() {
        return threshold;
    }

    public void setThreshold(BadgeThreshold threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Badge badge = (Badge) o;
        return fileSystemLocation.equals(badge.fileSystemLocation) && name.equals(badge.name) && description.equals(badge.description) && threshold == badge.threshold;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileSystemLocation, name, description, threshold);
    }
}
