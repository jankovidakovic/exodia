package com.exodia.hyke.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class DutyRequest {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private HykeUser dutyRequester;

    //@ManyToOne
    //private MountainHut requestedHut;

    //TODO - check mappings


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DutyRequest that = (DutyRequest) o;
        return getDutyRequester().equals(that.getDutyRequester());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDutyRequester());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HykeUser getDutyRequester() {
        return dutyRequester;
    }

    public void setDutyRequester(HykeUser dutyRequester) {
        this.dutyRequester = dutyRequester;
    }


}
