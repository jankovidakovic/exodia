package com.exodia.hyke.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class ArchivedHike {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Hike hike;

    @Column(nullable = false)
    private String archiveDescription;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArchivedHike that = (ArchivedHike) o;
        return getHike().equals(that.getHike()) && getArchiveDescription().equals(that.getArchiveDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHike(), getArchiveDescription());
    }

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date archivedAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date doneAt;

    public Date getArchivedAt() {
        return archivedAt;
    }

    public void setArchivedAt(Date archivedAt) {
        this.archivedAt = archivedAt;
    }

    public Date getDoneAt() {
        return doneAt;
    }

    public void setDoneAt(Date doneAt) {
        this.doneAt = doneAt;
    }

	public Hike getHike() {
		return hike;
	}

	public void setHike(Hike hike) {
		this.hike = hike;
	}

	public String getArchiveDescription() {
		return archiveDescription;
	}

	public void setArchiveDescription(String archiveDescription) {
		this.archiveDescription = archiveDescription;
	}
    
    
}
