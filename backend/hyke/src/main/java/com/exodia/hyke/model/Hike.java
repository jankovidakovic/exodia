package com.exodia.hyke.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Model of a hiking route. Can be uniquely identified by ID. Contains all
 * the necessary information about the route, such as its starting point, its
 * destination, shelters and hiking lodges along the way, as well as some general
 * description of the route, route's length and intensity.
 * @author jankovidakovic
 */
@Entity
@Table(name = "hike")
public class Hike extends Post { //TODO - hashCode, equals

    @ManyToOne
    @NotNull
    private Location location; //region in which the trip takes place

    @Column(nullable = false)
    private String name; //short name of the hike

    @Column(nullable = false)
    private String description; //TODO - getteri, setteri

    @Column(nullable = false)
    private String hikeStart; //description of the starting location

    @Column(nullable = false)
    private String hikeEnd; //description of the trip destination

    @Column(nullable = false)
    private Double hikeLength; //length of the trip in kilometers

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private HikeIntensity intensity; //physical intensity of the trip

    @Column(nullable = false)
    private Boolean publicity; //denotes whether trip is publically available or is private
    //TODO - implement equals for public hikes

    @ManyToMany
    private Set<MountainHut> mountainHuts; //huts along the path that the hiking trip follows

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getHikeStart() {
        return hikeStart;
    }

    public void setHikeStart(String hikeStart) {
        this.hikeStart = hikeStart;
    }

    public String getHikeEnd() {
        return hikeEnd;
    }

    public void setHikeEnd(String hikeEnd) {
        this.hikeEnd = hikeEnd;
    }

    public Double getHikeLength() {
        return hikeLength;
    }

    public void setHikeLength(Double hikeLength) {
        this.hikeLength = hikeLength;
    }

    public HikeIntensity getIntensity() {
        return intensity;
    }

    public void setIntensity(HikeIntensity intensity) {
        this.intensity = intensity;
    }

    public Boolean getPublicity() {
        return publicity;
    }

    public void setPublicity(Boolean publicity) {
        this.publicity = publicity;
    }

    public Set<MountainHut> getMountainHuts() {
        return mountainHuts;
    }

    public void setMountainHuts(Set<MountainHut> mountainHuts) {
        this.mountainHuts = mountainHuts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hike hike = (Hike) o;
        return location.equals(hike.location) &&
            name.equals(hike.name) &&
            description.equals(hike.description) &&
            hikeStart.equals(hike.hikeStart) &&
            hikeEnd.equals(hike.hikeEnd) &&
            hikeLength.equals(hike.hikeLength) &&
            intensity == hike.intensity &&
            publicity.equals(hike.publicity) &&
            mountainHuts.equals(hike.mountainHuts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, name, description, hikeStart, hikeEnd, hikeLength, intensity, publicity, mountainHuts);
    }
}
