package com.exodia.hyke.exception;

/**
 * Exception that is thrown when a Hike, that should exist, does not exist in the database.
 * @author a_nakic
 */

public class HikeDoesNotExistException extends RuntimeException
{
    public HikeDoesNotExistException (String errorMessage)
    {
        super (errorMessage);
    }
}
