package com.exodia.hyke.exception;

public class MountainHutAlreadyExistsException extends RuntimeException {
    public MountainHutAlreadyExistsException (String errorMessage) {
        super(errorMessage);
    }
}