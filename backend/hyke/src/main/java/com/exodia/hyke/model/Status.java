package com.exodia.hyke.model;

public enum Status {
    PENDING,
    ACCEPTED,
    DECLINED,
}
