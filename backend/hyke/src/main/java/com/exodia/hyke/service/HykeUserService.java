package com.exodia.hyke.service;

import java.util.List;
import java.util.NoSuchElementException;

import com.exodia.hyke.dto.ConfirmVisitDTO;
import com.exodia.hyke.dto.FriendRequestDTO;
import com.exodia.hyke.dto.form.ChangePasswordDTO;
import com.exodia.hyke.dto.form.UpdateInfoFormDTO;
import com.exodia.hyke.dto.UserRoleDTO;
import com.exodia.hyke.dto.form.UserRegistrationDTO;
import com.exodia.hyke.dto.form.UserUpdateFormDTO;
import com.exodia.hyke.dto.minimal.EventMinimalDTO;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.*;

/**
 * Service that manages all user-related operations.
 *
 * @author jankovidakovic
 */
public interface HykeUserService {

	/**
	 * Fetches all users that are defined within the application
	 *
	 * @return list of all defined hyke users.
     * @author jankovidakovic
	 */
	List<HykeUser> listAll();
	HykeUser createHykeUser(HykeUser hykeUser); //TODO - remove and integrate with other user services
	HykeUser registerNewHykeUser(UserRegistrationDTO userDto); //TODO - remove and integrate with other user services
	HykeUser getHykeUser(Long id);
	List<AcquiredBadge> getUserBadges(Long id);

	/**
	 * Updates some existing hyke user with the provided data.
	 *
	 * @param hykeUserId ID of user that is to be updated
	 * @throws NoSuchElementException if there is no user with provided ID.
	 * @throws IllegalArgumentException if new user data is invalid
	 * @author jankovidakovic
	 */
	void updateInfo(Long hykeUserId, UpdateInfoFormDTO updateInfoFormDTO);

	void changePassword(Long hykeUserId, ChangePasswordDTO changePasswordDTO);


	void deleteUser(Long hykeUserId);

	/**
	 * Fetches a hyke user with given username. Returns the user if such exists, or throws an exception if not.
	 * @param username username of the requested hyke user
	 * @return instance of hyke user with given username
	 * @throws NoSuchElementException if there is not hyke user with provided username
	 * @author jankovidakovic
	 */
	HykeUser getHykeUserWithUsername(String username);

	void changeRole(Long hykeUserId, UserRoleDTO userRoleDTO);
	void sendFriendRequest(FriendRequestDTO frDTO);
	List<UserMinimalDTO> getFriendRequests(Long hykeUserId);
	void replyToFriendRequest(Long hykeUserId, ConfirmVisitDTO cvDTO);
	List<UserMinimalDTO> getFriends(Long id);
	void removeFriend(Long id, FriendRequestDTO frDTO);
	List<Invite> getMyInvites(Long id);
	boolean checkFriendShip(Long user1_id, Long user2_id);
}
