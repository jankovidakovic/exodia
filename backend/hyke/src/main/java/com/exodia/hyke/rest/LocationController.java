package com.exodia.hyke.rest;

import com.exodia.hyke.dto.LocationDTO;
import com.exodia.hyke.exception.LocationAlreadyExistsException;
import com.exodia.hyke.exception.LocationDoesNotExistException;
import com.exodia.hyke.model.Location;
import com.exodia.hyke.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller which takes care of the API endpoint for REST operations regarding locations.
 * @author a_nakic
 */

@RestController
@RequestMapping("/locations")
public class LocationController
{
    @Autowired
    private LocationService locationService;

    /**
     * Creates a new location and catches the LocationAlreadyExistsException.
     * @param locationDTO
     * @return appropriate ResponseEntity
     */

    @PostMapping("")
    @Secured("ROLE_ADMIN")
    public ResponseEntity <String> createLocation (@RequestBody LocationDTO locationDTO)
    {
        try {
            locationService.createLocation (locationDTO);

            return new ResponseEntity <String> ("Success", HttpStatus.OK);

        } catch (LocationAlreadyExistsException e) {

            return new ResponseEntity <String> ("Location already exists", HttpStatus.CONFLICT);
        }
    }

    /**
     * Returns all available locations.
     * @return list of locations
     */

    @GetMapping("")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public List<Location> listLocations ()
    {
        return locationService.listLocations ();
    }

    /**
     * Updates a location and catches the LocationDoesNotExistException.
     * @param locationDTO
     * @return appropriate ResponseEntity
     */

    @PutMapping("")
    @Secured("ROLE_ADMIN")
    public ResponseEntity <String> updateLocation (@RequestBody LocationDTO locationDTO)
    {
        try {
            locationService.updateLocation (locationDTO);

            return new ResponseEntity <String> ("Success", HttpStatus.OK);

        } catch (LocationDoesNotExistException e) {

            return new ResponseEntity <String> ("Location does not exist", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes a location and catches the LocationDoesNotExistException.
     * @param location name
     * @return appropriate ResponseEntity
     */

    @DeleteMapping("")
    @Secured("ROLE_ADMIN")
    public ResponseEntity <String> deleteLocation (@RequestBody String name)
    {
        try {
            locationService.deleteLocation (name);

            return new ResponseEntity <String> ("Success", HttpStatus.OK);

        } catch (LocationDoesNotExistException e) {

            return new ResponseEntity <String> ("Location does not exist", HttpStatus.NOT_FOUND);
        }
    }
}
