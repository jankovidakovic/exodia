package com.exodia.hyke.dto.detailed;

import com.exodia.hyke.dto.MountainHutDTO;
import com.exodia.hyke.model.MountainHut;

/**
 * GET /mountain-huts/{mountainHutId}
 */
public class MountainHutDetailedDTO {

    private Long id;
    private LocationDetailedDTO location;
    private String name;
    private String description;
    private Boolean sleepover;
    private Boolean food;
    private Boolean water;
	private Boolean electricity;
    //add prenociste hrana voda struja
    
    public static MountainHutDetailedDTO getDTO(MountainHut mountainHut) {
		MountainHutDetailedDTO mountainHutDTO = new MountainHutDetailedDTO();
		mountainHutDTO.setName(mountainHut.getName());
		mountainHutDTO.setLocation(LocationDetailedDTO.getDTO(mountainHut.getLocation()));
		mountainHutDTO.setDescription(mountainHut.getDescription());
		mountainHutDTO.setId(mountainHut.getId());
		mountainHutDTO.setElectricity(mountainHut.getElectricity());
		mountainHutDTO.setFood(mountainHut.getFood());
		mountainHutDTO.setSleepover(mountainHut.getSleepover());
		mountainHutDTO.setWater(mountainHut.getWater());
		return mountainHutDTO;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public LocationDetailedDTO getLocation() {
		return location;
	}

	public void setLocation(LocationDetailedDTO location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getSleepover() {
		return sleepover;
	}

	public void setSleepover(Boolean sleepover) {
		this.sleepover = sleepover;
	}

	public Boolean getFood() {
		return food;
	}

	public void setFood(Boolean food) {
		this.food = food;
	}

	public Boolean getWater() {
		return water;
	}

	public void setWater(Boolean water) {
		this.water = water;
	}

	public Boolean getElectricity() {
		return electricity;
	}

	public void setElectricity(Boolean electricity) {
		this.electricity = electricity;
	}
}
