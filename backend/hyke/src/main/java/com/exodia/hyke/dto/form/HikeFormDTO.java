package com.exodia.hyke.dto.form;

import com.exodia.hyke.dto.IdDTO;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HikeIntensity;

import java.util.Collection;

/**
 * POST /hikes, ovo se predaje u bodyju
 */
public class HikeFormDTO {

    private Long creator;
    private String name;
    private String description;
    private Long location; //only ID is needed
    private String hikeStart;
    private String hikeEnd;
    private Double hikeLength;
    private HikeIntensity intensity;
    private Boolean publicity;
    private Collection<Long> mountainHuts;


    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLocation() {
        return location;
    }

    public void setLocation(Long location) {
        this.location = location;
    }

    public String getHikeStart() {
        return hikeStart;
    }

    public void setHikeStart(String hikeStart) {
        this.hikeStart = hikeStart;
    }

    public String getHikeEnd() {
        return hikeEnd;
    }

    public void setHikeEnd(String hikeEnd) {
        this.hikeEnd = hikeEnd;
    }

    public Double getHikeLength() {
        return hikeLength;
    }

    public void setHikeLength(Double hikeLength) {
        this.hikeLength = hikeLength;
    }

    public HikeIntensity getIntensity() {
        return intensity;
    }

    public void setIntensity(HikeIntensity intensity) {
        this.intensity = intensity;
    }

    public Boolean getPublicity() {
        return publicity;
    }

    public void setPublicity(Boolean publicity) {
        this.publicity = publicity;
    }

    public Collection<Long> getMountainHuts() {
        return mountainHuts;
    }

    public void setMountainHuts(Collection<Long> mountainHuts) {
        this.mountainHuts = mountainHuts;
    }
}
