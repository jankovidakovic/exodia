package com.exodia.hyke.exception;

/**
 * Exception that is thrown when a location, that should not exist, exists in the database.
 * @author a_nakic
 */

public class LocationAlreadyExistsException extends RuntimeException
{
    public LocationAlreadyExistsException (String errorMessage)
    {
        super (errorMessage);
    }
}
