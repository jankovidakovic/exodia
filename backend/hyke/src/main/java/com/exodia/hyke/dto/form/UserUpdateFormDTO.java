package com.exodia.hyke.dto.form;

import com.exodia.hyke.model.HykeUser;

/**
 * PUT /users/{userId}, ovo se predaje u bodyju
 */
public class UserUpdateFormDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static UserUpdateFormDTO getUserUpdateFormDTO(HykeUser user){
        UserUpdateFormDTO userUpdateFormDTO = new UserUpdateFormDTO();
        userUpdateFormDTO.setEmail(user.getEmail());
        userUpdateFormDTO.setFirstName(user.getFirstName());
        userUpdateFormDTO.setId(user.getId());
        userUpdateFormDTO.setLastName(user.getLastName());
        userUpdateFormDTO.setPassword(user.getPassword());
        userUpdateFormDTO.setUsername(user.getUsername());

        return userUpdateFormDTO;
    }
}
