package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.HikeRepository;
import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.dao.MountainHutRepository;
import com.exodia.hyke.dto.WishlistItemDTO;
import com.exodia.hyke.dto.WishlistUpdateDTO;
import com.exodia.hyke.exception.*;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.MountainHut;
import com.exodia.hyke.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class WishlistServiceImpl implements WishlistService
{
    @Autowired
    private HykeUserRepository userRepo;

    @Autowired
    private HikeRepository hikeRepository;

    @Autowired
    private MountainHutRepository mountainHutRepository;

    @Override
    public List <WishlistItemDTO> fetchWishlist (Long userId)
    {
        if (userRepo.findById (userId).isPresent ()) {
            return Stream.concat(
                userRepo.findById(userId).get().getMountainHutsWishlist()
                    .stream()
                    .map(mountainHut -> WishlistItemDTO.getDTO(mountainHut)),
                userRepo.findById(userId).get().getHikesWishlist()
                    .stream()
                    .map(hike -> WishlistItemDTO.getDTO(hike))
            ).collect(Collectors.toList());
        } else {
            throw new UserDoesNotExistException ("User does not exist.");
        }
    }

    @Override
    public void addHike (Long hikeId, Long userId)
    {
        if (userRepo.findById (userId).isPresent ()) {
            if (hikeRepository.findById (hikeId).isPresent()) {
                HykeUser user = userRepo.findById (userId).get ();
                Collection <Hike> hikes = user.getHikesWishlist ();
                Hike hike = hikeRepository.findById (hikeId).get ();

                if (hikes.contains (hike)) {
                    throw new HikeAlreadyExistsException ("Hike already exists in wishlist.");
                } else {
                    hikes.add (hike);
                    user.setHikesWishlist (hikes);
                    userRepo.save (user);
                }
            } else {
                throw new HikeDoesNotExistException ("Hike with id does not exist.");
            }
        } else {
            throw new UserDoesNotExistException ("User with id does not exist.");
        }
    }

    @Override
    public void addMountainHut (Long mountainHutId, Long userId)
    {
        if (userRepo.findById (userId).isPresent ()) {
            if (mountainHutRepository.findById (mountainHutId).isPresent()) {
                HykeUser user = userRepo.findById (userId).get ();
                Collection <MountainHut> mountainHuts = user.getMountainHutsWishlist ();
                MountainHut mountainHut = mountainHutRepository.findById (mountainHutId).get ();

                if (mountainHuts.contains (mountainHut)) {
                    throw new MountainHutAlreadyExistsException ("MountainHut already exists in wishlist");
                } else {
                    mountainHuts.add(mountainHutRepository.findById(mountainHutId).get());
                    user.setMountainHutsWishlist(mountainHuts);
                    userRepo.save(user);
                }
            } else {
                throw new MountainHutDoesNotExistException ("MountainHut with id does not exist.");
            }
        } else {
            throw new UserDoesNotExistException ("User with id does not exist.");
        }
    }

    @Override
    public void removeHike (Long hikeId, Long userId)
    {
        if (userRepo.findById (userId).isPresent ()) {
            if (hikeRepository.findById (hikeId).isPresent()) {
                HykeUser user = userRepo.findById (userId).get ();
                Collection <Hike> hikes = user.getHikesWishlist ();
                Hike hike = hikeRepository.findById (hikeId).get ();

                if (!hikes.contains (hike)) {
                    throw new HikeDoesNotExistException ("Hike does not exist in wishlist.");
                } else {
                    hikes.remove (hike);
                    user.setHikesWishlist (hikes);
                    userRepo.save (user);
                }
            } else {
                throw new HikeDoesNotExistException ("Hike with id does not exist.");
            }
        } else {
            throw new UserDoesNotExistException ("User with id does not exist.");
        }
    }

    @Override
    public void removeMountainHut (Long mountainHutId, Long userId)
    {
        if (userRepo.findById (userId).isPresent ()) {
            if (mountainHutRepository.findById (mountainHutId).isPresent()) {
                HykeUser user = userRepo.findById (userId).get ();
                Collection <MountainHut> mountainHuts = user.getMountainHutsWishlist ();
                MountainHut mountainHut = mountainHutRepository.findById (mountainHutId).get ();

                if (!mountainHuts.contains (mountainHut)) {
                    throw new MountainHutDoesNotExistException ("MountainHut does not exist in wishlist");
                } else {
                    mountainHuts.remove (mountainHutRepository.findById(mountainHutId).get());
                    user.setMountainHutsWishlist(mountainHuts);
                    userRepo.save(user);
                }
            } else {
                throw new MountainHutDoesNotExistException ("MountainHut with id does not exist.");
            }
        } else {
            throw new UserDoesNotExistException ("User with id does not exist.");
        }
    }
}
