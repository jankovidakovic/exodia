package com.exodia.hyke.service.impl;

import java.util.Collection;
import java.util.Optional;

import com.exodia.hyke.dao.HykeUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exodia.hyke.dao.HikeRepository;
import com.exodia.hyke.dto.ArchivedHikeDTO;
import com.exodia.hyke.exception.HikeDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.ArchivedHike;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.service.ArchiveService;

@Service
public class ArchiveServiceImpl implements ArchiveService {
	
	@Autowired
	HykeUserRepository userRepo;
	
	@Autowired
	HikeRepository hikeRepository;
	
	@Override
	public Collection<ArchivedHike> findByUserId(Long userId) {
		Optional<HykeUser> hykeUser = userRepo.findById(userId);
        if (hykeUser.isPresent()) {
            return hykeUser.get().getArchivedHikes();
        } else {
            throw new UserDoesNotExistException("User with specified id does not exist.");
        }

	}

	@Override
	public void addToArchive(ArchivedHikeDTO dto, Long userId) {
		
		if (userRepo.findById(userId).isPresent ()) {
			HykeUser user = userRepo.findById(userId).get();
			Collection<ArchivedHike> archivedHikes = user.getArchivedHikes();
			
			if (hikeRepository.findById(dto.getHikeMinimalDTO().getId()).isPresent()) {
	            Hike hike = hikeRepository.findById(dto.getHikeMinimalDTO().getId()).get();
	            
	            ArchivedHike archivedHike = new ArchivedHike();
	            archivedHike.setHike(hike);
	            archivedHike.setArchivedAt(dto.getArchivedAt());
	            archivedHike.setArchiveDescription(dto.getArchiveDescription());
	            archivedHike.setDoneAt(dto.getDoneAt());
	            archivedHikes.add(archivedHike);
	            
	            user.setArchivedHikes(archivedHikes);
	            userRepo.save(user);
	            
	        } else {
	        	throw new HikeDoesNotExistException("Hike with specified id does not exist");
	        }
			
		} else {
			throw new UserDoesNotExistException("User with specified id does not exist");
		}
   
	}
	
}
