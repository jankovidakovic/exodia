package com.exodia.hyke.dao;

import com.exodia.hyke.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Repository that manages locations
 * @atuhor a_nakic
 */

public interface LocationRepository extends JpaRepository <Location, Long>
{
    /**
     * Searches for a location with a given name
     * @return matching location
     */
    Location findByName (String name);

    /**
     * Searches for all locations
     * @return list of locations
     */
    List<Location> findAll ();

    /**
     * Searches for the id with the highest value
     * @return maximum id
     */
    @Query(value = "SELECT max(id) FROM hyke_location", nativeQuery = true)
    Long findMaxId ();
}
