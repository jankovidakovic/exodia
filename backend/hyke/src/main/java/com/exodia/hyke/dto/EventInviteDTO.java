package com.exodia.hyke.dto;


import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.Invite;
import com.exodia.hyke.model.Status;

import java.util.Objects;

/**
 * POST /invites, ovo se predaje u bodyju
 *
 * za odgovor na invite, radi se PUT /invites/{inviteId}, u bodyju je odgovor (oblik odgovora - TODO)
 */
public class EventInviteDTO {
	
    private UserMinimalDTO user;
    private Status status; //kad se šalje, ovo može biti null, a ionako će
                                //biti postavljeno u PENDING na backendu

    public UserMinimalDTO getUser() {
		return user;
	}

	public void setUser(UserMinimalDTO user) {
		this.user = user;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public static EventInviteDTO getDTO(Invite invite) {
    	EventInviteDTO dto = new EventInviteDTO();
    	dto.setUser(UserMinimalDTO.getDTO(invite.getInvitedUser()));
    	dto.setStatus(invite.getStatus());
    	return dto;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof EventInviteDTO)) return false;
		EventInviteDTO that = (EventInviteDTO) o;
		return Objects.equals(user, that.user);
	}

	@Override
	public int hashCode() {
		return Objects.hash(user);
	}
}
