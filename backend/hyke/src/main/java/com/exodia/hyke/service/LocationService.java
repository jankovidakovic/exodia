package com.exodia.hyke.service;

import com.exodia.hyke.dto.LocationDTO;
import com.exodia.hyke.exception.LocationAlreadyExistsException;
import com.exodia.hyke.model.Location;

import java.util.List;

/**
 * Manages locations.
 * @author a_nakic
 */

public interface LocationService
{
    /**
     * Creates a new location.
     * @param locationDTO
     * @return always 0
     * @throws LocationAlreadyExistsException if location already exists
     */

    int createLocation (LocationDTO location);

    /**
     * Returns all available locations.
     * @return list of locations
     */

    List<Location> listLocations ();

    /**
     * Updates an existing location.
     * @param locationDTO
     * @return always 0
     * @throws LocationDoesNotExistException if location does not exist
     */

    int updateLocation (LocationDTO location);

    /**
     * Deletes an existing location.
     * @param location name
     * @return always 0
     * @throws LocationDoesNotExistException if location does not exist
     */

    int deleteLocation (String name);
}
