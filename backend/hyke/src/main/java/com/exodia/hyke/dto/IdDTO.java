package com.exodia.hyke.dto;

/**
 * Kad se za neki objekt prenosi samo ID
 */
public class IdDTO {
    private Long id;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
}
