package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.LocationRepository;
import com.exodia.hyke.dto.LocationDTO;
import com.exodia.hyke.exception.LocationAlreadyExistsException;
import com.exodia.hyke.exception.LocationDoesNotExistException;
import com.exodia.hyke.model.Location;
import com.exodia.hyke.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService
{
    @Autowired
    private LocationRepository locationRepository;

    @Override
    public int createLocation (LocationDTO locationDTO)
    {
        if (locationRepository.findByName (locationDTO.getName ()) == null) {

            Location location = new Location ();

            location.setName (locationDTO.getName ());
            location.setDescription (locationDTO.getDescription ());

            locationRepository.saveAndFlush(location);

            return 0;
        } else {

            throw new LocationAlreadyExistsException ("Location with that name already exists.");
        }
    }

    @Override
    public List<Location> listLocations ()
    {
        return locationRepository.findAll ();
    }

    @Override
    public int updateLocation (LocationDTO locationDTO)
    {
        Location location = locationRepository.findByName (locationDTO.getName ());

        if (location != null) {

            location.setDescription (locationDTO.getDescription ());

            locationRepository.save (location);

            return 0;

        } else {

            throw new LocationDoesNotExistException ("Location with that name does not exist.");
        }
    }

    @Override
    public int deleteLocation (String name)
    {
        Location location = locationRepository.findByName (name);

        if (location != null) {

            locationRepository.delete (location);

            return 0;
        } else {

            throw new LocationDoesNotExistException ("Location with that name does not exist");
        }
    }
}
