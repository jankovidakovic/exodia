package com.exodia.hyke.service;

import java.util.Collection;

import com.exodia.hyke.dto.ArchivedHikeDTO;
import com.exodia.hyke.model.ArchivedHike;

public interface ArchiveService {

	Collection<ArchivedHike> findByUserId(Long userId);

	void addToArchive(ArchivedHikeDTO dto, Long userId);

}
