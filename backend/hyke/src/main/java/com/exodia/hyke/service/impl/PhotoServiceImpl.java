package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.dao.PhotoRepository;
import com.exodia.hyke.dto.form.PhotoFormDTO;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.Photo;
import com.exodia.hyke.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;

@Service
public class PhotoServiceImpl implements PhotoService
{
    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private HykeUserRepository userRepo;

    @Value("${image.path}")
    private String imagePath;

    @Override
    public String saveImage (MultipartFile image) throws IOException
    {
        if (ImageIO.read(image.getInputStream()) == null) {
            throw new IOException("Only images allowed!");
        }
        String img_name = Integer.toString (new Timestamp (System.currentTimeMillis ()).hashCode ()) + ".jpg";
        Path path = Paths.get (imagePath + img_name);

        image.transferTo (path);

        return img_name;
    }

    @Override
    public void uploadPhotoPost (PhotoFormDTO photoFormDTO)
    {
        Photo photo = new Photo ();

        photo.setDescription (photoFormDTO.getPhoto ().getDescription ());
        photo.setFilesystemLocation (photoFormDTO.getPhoto ().getLocation ());

        if (!userRepo.findById (photoFormDTO.getUser ().getId ()).isPresent ()) {
            throw new UserDoesNotExistException ("User with id does not exist.");
        }

        photo.setCreator(userRepo.findById(photoFormDTO.getUser().getId()).get());
        photo.setCreatedAt (new Date());

        photoRepository.save (photo);
    }
}
