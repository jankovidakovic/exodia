package com.exodia.hyke.exception;

/**
 * Exception that is thrown when a location, that should exist, does not exist in the database.
 * @author a_nakic
 */

public class LocationDoesNotExistException extends RuntimeException
{
    public LocationDoesNotExistException (String errorMessage)
    {
        super (errorMessage);
    }
}
