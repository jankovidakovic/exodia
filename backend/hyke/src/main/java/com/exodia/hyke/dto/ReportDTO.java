package com.exodia.hyke.dto;


import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.model.UserReport;

import java.util.ArrayList;
import java.util.List;

public class ReportDTO {

    private Long id;

    private UserMinimalDTO user;

    private String report;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserMinimalDTO getUser() {
        return user;
    }

    public void setUser(UserMinimalDTO user) {
        this.user = user;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public static List<ReportDTO> getReportDTOs(List<UserReport> reports){
        List<ReportDTO> reportDTOs = new ArrayList<>();
        for(UserReport ur: reports){
            ReportDTO reportDTO = new ReportDTO();
            reportDTO.setId(ur.getId());
            reportDTO.setReport(ur.getDescription());
            reportDTO.setUser(UserMinimalDTO.getDTO(ur.getReporter()));
            reportDTOs.add(reportDTO);
        }

        return reportDTOs;

    }

    public static ReportDTO getDTO(UserReport userReport) {
        ReportDTO dto = new ReportDTO();
        dto.setId(userReport.getId());
        dto.setReport(userReport.getDescription());
        dto.setUser(UserMinimalDTO.getDTO(userReport.getReporter()));
        return dto;
    }
}
