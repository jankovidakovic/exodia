package com.exodia.hyke.dao;

import com.exodia.hyke.model.Location;
import com.exodia.hyke.model.MountainHut;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages persistence of MountainHuts.
 * @author a_nakic
 */
public interface MountainHutRepository extends JpaRepository <MountainHut, Long>
{
    MountainHut findByName(String name);

    List<MountainHut> findByLocation (Location location);
}
