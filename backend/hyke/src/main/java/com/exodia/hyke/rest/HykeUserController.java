package com.exodia.hyke.rest;

import com.exodia.hyke.dto.*;
import com.exodia.hyke.dto.detailed.BadgeDetailedDTO;
import com.exodia.hyke.dto.detailed.UserDetailedDTO;
import com.exodia.hyke.dto.form.ChangePasswordDTO;
import com.exodia.hyke.dto.form.UpdateInfoFormDTO;
import com.exodia.hyke.dto.minimal.*;
import com.exodia.hyke.model.*;
import com.exodia.hyke.service.HykeUserService;
import com.sun.istack.NotNull;
import com.exodia.hyke.dto.minimal.UserMinimalDTO;
import com.exodia.hyke.exception.*;
import com.exodia.hyke.service.WishlistService;
import com.exodia.hyke.service.NewsFeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Controller which accepts the requests that deal directly with hyke users.
 *
 * @author jankovidakovic a_nakic
 */
@RestController
@RequestMapping("/users")
public class HykeUserController {

    @Autowired
    private HykeUserService hykeUserService;

    @Autowired
    private WishlistService wishlistService;

	@Autowired
    private NewsFeedService newsFeedService;

    /**
     * Updates the hyke user with the given ID, setting new values to match
     * the given update DTO. Validates the DTO and reports any errors.
     *
     * @param hykeUserId ID of the user that is to be updated. Provided as a path variable.
     * @param updateInfoFormDTO new data that the user should be updated with. Provided in the request body.
     * @return 200 OK if the user was updated successfully, 400 Bad Request if somethin went wrong.
     * @author jankovidakovic
     */
    @PutMapping("/{hykeUserId}/info")
    public ResponseEntity<String> updateInfo(
            @PathVariable("hykeUserId") @NotBlank Long hykeUserId,
            @RequestBody UpdateInfoFormDTO updateInfoFormDTO
            ) {
        try {
            hykeUserService.updateInfo(hykeUserId, updateInfoFormDTO);

            //TODO - validate that url parameter matches hyke user update id
            return new ResponseEntity<>("User updated successfully", HttpStatus.OK);

            //TODO - synchronize response entitites of DTO validation and service-layer validation
        }catch(NoSuchElementException e){
            return new ResponseEntity<>("User does not exist", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{hykeUserId}/password")
    public ResponseEntity<String> changePassword(
            @PathVariable("hykeUserId") @NotBlank Long hykeUserId,
            @RequestBody ChangePasswordDTO changePasswordDTO
            ) {
        try {
            hykeUserService.changePassword(hykeUserId, changePasswordDTO);

            return new ResponseEntity<>("Password successfully changed.", HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>("User does not exist.", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Fetches the specified user's badges.
     *
     * @return list containing user's badges.
     * @author daeyarn
     */

    @GetMapping("/{hykeUserId}/badges")
    public ResponseEntity<List<BadgeMinimalDTO>> getUserBadges(
            @PathVariable("hykeUserId") Long hykeUserId
            ){
        try{
            List<AcquiredBadge> badgesList = hykeUserService.getUserBadges(hykeUserId);
            List<BadgeMinimalDTO> badgeMinimalDTOList = BadgeMinimalDTO.getBadgeMinimalDTOS(badgesList);

            return new ResponseEntity<>(badgeMinimalDTOList, HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // FOR TESTING THE FRONTEND
    @GetMapping("/{hykeUserId}/badges-test")
    public ResponseEntity<List<BadgeDetailedDTO>> getBadgesTest(
            @PathVariable("hykeUserId") Long hykeUserId
    ) {
        List<BadgeDetailedDTO> badges = new ArrayList<>() {{
            add(new BadgeDetailedDTO("badge1", "description of badge1"));
            add(new BadgeDetailedDTO("badge2", "description of badge2"));
            add(new BadgeDetailedDTO("badge3", "description of badge3"));

        }};

        return new ResponseEntity<>(badges, HttpStatus.OK);
    }

    /**
     * Fetches the specified user's data.
     *
     * @return user's data.
     * @author daeyarn
     */

    @GetMapping("/{hykeUserId}")
    public ResponseEntity<UserDetailedDTO> getHykeUser(
            @PathVariable("hykeUserId") @NotBlank Long hykeUserId
            //@RequestBody IdDTO idDTO
            ) {
        try {
            HykeUser hu = hykeUserService.getHykeUser(hykeUserId);
            //boolean befriended = hykeUserService.checkFriendShip(idDTO.getId(), hykeUserId);
            UserDetailedDTO dto = UserDetailedDTO.getDTO(hu);
            //dto.setBefriended(befriended);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    /**
     * Deletes the specified user from the database;
     * deletes its username and email.
     * @return 200 OK if no errors emerge.
     * @author daeyarn
     */

    @DeleteMapping("/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable("userId") @NotNull Long id){
        try {
            hykeUserService.deleteUser(id);
            return new ResponseEntity<>("User successfully deleted.", HttpStatus.NO_CONTENT);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>("User does not exist.", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Fetches all hyke users.
     *
     * @return list containing all users.
     * @author jankovidakovic
     */

    @GetMapping("")
	@Secured({"ROLE_USER", "ROLE_DUTY", "ROLE_ADMIN"})
    public ResponseEntity<List<UserMinimalDTO>> getAllUsers() {
        return new ResponseEntity<>(hykeUserService.listAll()
                    .stream()
                    .map(UserMinimalDTO::getDTO)
                    .collect(Collectors.toList()), HttpStatus.OK);
    }

    /**
     * Fetches a list of mountain huts in which the
     * specified user is on duty.
     *
     * @return list containing mountain hut minimal DTOs.
     * @author daeyarn
     */
    /*@GetMapping("/{userId}/duties")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<List<MountainHutMinimalDTO>> getDuties(
        @PathVariable("userId") @NotNull Long id
    ) {
    try {
        List<MountainHut> huts = hykeUserService.getDuties(id);
        List<MountainHutMinimalDTO> hutsDTOs = new ArrayList<>();
        for (MountainHut hut : huts) {
            hutsDTOs.add(MountainHutMinimalDTO.getDTO(hut));
        }
        return new ResponseEntity<>(hutsDTOs, HttpStatus.OK);
    }catch(NoSuchElementException e){
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    }*/

    /**
     * Updates the role of a user.
     *
     * @return 200 OK if no errors emerge.
     * @author daeyarn
     */
    @PutMapping("/{userId}/role")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> changeUserRole(
            @PathVariable("userId") @NotNull Long id,
            @RequestBody UserRoleDTO userRoleDTO
            ) {
        try {
            hykeUserService.changeRole(id, userRoleDTO);
            return new ResponseEntity("Role successfully changed.", HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity("User does not exist", HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/{userId}/wishlist")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity <List <WishlistItemDTO>> fetchWishlist (@PathVariable Long userId)
    {
        try {
            return new ResponseEntity<List<WishlistItemDTO>>(wishlistService.fetchWishlist(userId), HttpStatus.OK);
        } catch (UserDoesNotExistException e) {
            return new ResponseEntity <List <WishlistItemDTO>> (Collections.emptyList(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{userId}/wishlist")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity <String> addToWishList (@PathVariable Long userId, @RequestBody WishlistUpdateDTO wishlistUpdateDTO)
    {
        try {
            System.out.println(wishlistUpdateDTO.getHikeId ());
            if (wishlistUpdateDTO.getHikeId () != null) {
                wishlistService.addHike (wishlistUpdateDTO.getHikeId (), userId);
            }

            if (wishlistUpdateDTO.getMountainHutId () != null) {
                wishlistService.addMountainHut (wishlistUpdateDTO.getMountainHutId (), userId);
            }

            return new ResponseEntity <String> ("Success.", HttpStatus.CREATED);

        } catch (UserDoesNotExistException e) {
            return new ResponseEntity <String> ("User does not exist.", HttpStatus.NOT_FOUND);
        } catch (HikeDoesNotExistException e) {
            return new ResponseEntity <String> ("Hike does not exist.", HttpStatus.NOT_FOUND);
        } catch (MountainHutDoesNotExistException e) {
            return new ResponseEntity <String> ("MountainHut does not exist.", HttpStatus.NOT_FOUND);
        } catch (HikeAlreadyExistsException e ) {
            return new ResponseEntity <String> ("Hike exists.", HttpStatus.CONFLICT);
        } catch (MountainHutAlreadyExistsException e) {
            return new ResponseEntity <String> ("MountainHut exists.", HttpStatus.CONFLICT);
        }
    }


    @DeleteMapping("/{userId}/wishlist")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity <String> removeFromWishList (@PathVariable Long userId, @RequestBody WishlistUpdateDTO wishlistUpdateDTO) {
        try {
            if (wishlistUpdateDTO.getHikeId() != null) {
                wishlistService.removeHike(wishlistUpdateDTO.getHikeId(), userId);
            }

            if (wishlistUpdateDTO.getMountainHutId() != null) {
                wishlistService.removeMountainHut(wishlistUpdateDTO.getMountainHutId(), userId);
            }

            return new ResponseEntity<String>("Success.", HttpStatus.OK);

        } catch (UserDoesNotExistException e) {
            return new ResponseEntity<String>("User does not exist.", HttpStatus.NOT_FOUND);
        } catch (HikeDoesNotExistException e) {
            return new ResponseEntity<String>("Hike does not exist.", HttpStatus.NOT_FOUND);
        } catch (MountainHutDoesNotExistException e) {
            return new ResponseEntity<String>("MountainHut does not exist.", HttpStatus.NOT_FOUND);
        }
    }

     /** Fetches posts by creator which are created after lastFetched.
     * @param userId of user
     * @return List of PostDTO
     */
    @GetMapping("/{userId}/news")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity <List <PostDTO>> fetchPostByCreator (
            @PathVariable Long userId)
    {
        return new ResponseEntity <List<PostDTO>> (newsFeedService.fetchNewsFeedByCreator (userId), HttpStatus.OK);
    }

    //for friends
    @PostMapping("/{userId}/add-friend")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity<String> addFriend(
            @RequestBody FriendRequestDTO frDTO
            ){
        try {
            hykeUserService.sendFriendRequest(frDTO);
            return new ResponseEntity<>("Friend request sent.", HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{userId}/friend-requests")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity<List<UserMinimalDTO>> getFriendRequests(
            @PathVariable("userId") Long id
    ){
            try{
                return new ResponseEntity<>(hykeUserService.getFriendRequests(id), HttpStatus.OK);
            }catch(NoSuchElementException e){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
    }

    @PostMapping("/{userId}/friend-requests/reply")
    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
    public ResponseEntity<String> replyToFriendRequests(
            @PathVariable("userId") Long id,
            @RequestBody ConfirmVisitDTO cvDTO
            ){
        try{
            hykeUserService.replyToFriendRequest(id, cvDTO);
            return new ResponseEntity<>("Successfully replied to the friend request.", HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{userId}/friends")
    public ResponseEntity<List<UserMinimalDTO>> getFriends(
            @PathVariable("userId") Long id
    ){
        try {
            return new ResponseEntity<>(hykeUserService.getFriends(id), HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{userId}/remove-friend")
    public ResponseEntity<String> removeFriend(
            @PathVariable("userId") Long id,
            @RequestBody FriendRequestDTO frDTO
    ){
        try{
            hykeUserService.removeFriend(id, frDTO);
            return new ResponseEntity<>("Friendship successfuly ended.", HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{userId}/invites")
    public ResponseEntity<List<EventMinimalDTO>> getMyInvites(
            @PathVariable("userId") Long id
            ){
        try{
            return new ResponseEntity<>(
                hykeUserService.getMyInvites(id)
                    .stream()
                    .map(invite -> invite.getEvent())
                    .map(EventMinimalDTO::getDTO)
                    .collect(Collectors.toList()), HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
