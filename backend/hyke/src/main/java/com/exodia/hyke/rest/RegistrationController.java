package com.exodia.hyke.rest;

import com.exodia.hyke.dto.form.UserRegistrationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exodia.hyke.exception.UserAlreadyExistException;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.service.UserRegistrationService;

import javax.validation.Valid;

@RestController
@RequestMapping("/register")
public class RegistrationController {
	
	@Autowired
	private UserRegistrationService userRegistrationService;
	
	@GetMapping("")
	public String register() {
		return "register";
	}

	@PostMapping("")
	public ResponseEntity<String> register(@Valid @RequestBody UserRegistrationDTO userDto) {
		try {
			HykeUser registered = userRegistrationService.registerNewHykeUser(userDto);
		} catch (UserAlreadyExistException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>("Account created", HttpStatus.CREATED);
	}
}
