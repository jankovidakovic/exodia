package com.exodia.hyke.exception;

public class MountainHutAlreadyExistException extends RuntimeException {
	public MountainHutAlreadyExistException (String errorMessage) {
		super(errorMessage);
	}
}
