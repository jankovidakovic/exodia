package com.exodia.hyke.dto;

/**
 * PUT /wishlist, ovo se predaje u bodyju
 */
public class WishlistUpdateDTO {

    private Long hikeId;         //either this must be null
    private Long mountainHutId;  //or this(frontend checks)

    public Long getHikeId () {
        return hikeId;
    }

    public void setHikeId (Long hikeId) {
        this.hikeId = hikeId;
    }

    public Long getMountainHutId() {
        return mountainHutId;
    }

    public void setMountainHutId(Long mountainHutId) {
        this.mountainHutId = mountainHutId;
    }

    /**
     * {
     *     id:
     *     user:
     *     hike:
     * }
     *
     * {
     *     id:
     *     user:
     *     mountainHut:
     * }
     */
}
