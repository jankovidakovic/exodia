package com.exodia.hyke.rest;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exodia.hyke.dto.ArchivedHikeDTO;
import com.exodia.hyke.exception.HikeDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.service.ArchiveService;

@RestController
@RequestMapping("users/{userId}/archived-hikes")
public class ArchiveController {
	
	@Autowired
	ArchiveService archiveService;
	
	@GetMapping("")
	@Secured({"ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER"})
	public ResponseEntity<List<ArchivedHikeDTO>> getArchivedHikes(@PathVariable Long userId) {
		try {
			return new ResponseEntity<List<ArchivedHikeDTO>>(archiveService
		            .findByUserId(userId)
		            .stream()
		            .map(ArchivedHikeDTO::getDTO)
		            .collect(Collectors.toList()), HttpStatus.OK);

		} catch (UserDoesNotExistException e) {
         	return new ResponseEntity<List<ArchivedHikeDTO>> (Collections.emptyList(), HttpStatus.NOT_FOUND);
	    }
	}
	
	@PostMapping("")
	@Secured({"ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER"})
	public ResponseEntity<String> addHikeToArchive(@PathVariable Long userId, @RequestBody ArchivedHikeDTO archivedHikeDTO) {
		try {
			archiveService.addToArchive(archivedHikeDTO, userId);
			return new ResponseEntity<String> ("Success", HttpStatus.OK);
			
		} catch (UserDoesNotExistException e) {
			 return new ResponseEntity <String> ("User does not exist.", HttpStatus.NOT_FOUND);
			 
		} catch (HikeDoesNotExistException e) {
			 return new ResponseEntity <String> ("Hike does not exist.", HttpStatus.NOT_FOUND);
			 
		}
	}
}
