package com.exodia.hyke.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.exodia.hyke.dao.*;
import com.exodia.hyke.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.exodia.hyke.service.DataLoaderService;

@Service
public class DataLoaderServiceImpl implements DataLoaderService {

    @Autowired
    private HykeUserRepository hykeUserRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private MountainHutRepository mountainHutRepository;

    @Autowired
    private HikeRepository hikeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private BadgeRepository badgeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    HykeUser user = new HykeUser();
    Location location = new Location();
    MountainHut mountainHut = new MountainHut();
    Hike hike = new Hike();
    Event event = new Event();

    @Override
    @PostConstruct
    public void loadData() {
        loadUsers();
        loadLocations();
        loadMountainHuts();
        loadHikes();
        loadEvents();
        loadBadges();
    }

    private void loadUsers() {
        user.setEmail("adminmail@gmail.com");
        user.setFirstName("Admin");
        user.setLastName("Adminović");
        user.setPassword(passwordEncoder.encode("adminpass"));
        user.setUsername("admin");
        user.setRole(Role.ADMIN);
        hykeUserRepository.save(user);

        HykeUser user1 = new HykeUser();
        user1.setEmail("ivan.horvat@email.com");
        user1.setFirstName("Ivan");
        user1.setLastName("Horvat");
        user1.setPassword(passwordEncoder.encode("ivanpass"));
        user1.setUsername("ihorvat");
        user1.setRole(Role.USER);
        hykeUserRepository.save(user1);

        HykeUser user2 = new HykeUser();
        user2.setEmail("marija.knezevic@email.com");
        user2.setFirstName("Marija");
        user2.setLastName("Knežević");
        user2.setPassword(passwordEncoder.encode("marijapass"));
        user2.setUsername("mknezevic");
        user2.setRole(Role.DUTY);
        hykeUserRepository.save(user2);

        HykeUser user3 = new HykeUser();
        user3.setEmail("jospi.kovačević@email.com");
        user3.setFirstName("Josip");
        user3.setLastName("Kovačević");
        user3.setPassword(passwordEncoder.encode("josippass"));
        user3.setUsername("jkovacevic");
        user3.setRole(Role.USER);
        hykeUserRepository.save(user3);

        HykeUser user4 = new HykeUser();
        user4.setEmail("ana.pavlovic@email.com");
        user4.setFirstName("Ana");
        user4.setLastName("Pavlovic");
        user4.setPassword(passwordEncoder.encode("anapass"));
        user4.setUsername("apavlovic");
        user4.setRole(Role.USER);
        hykeUserRepository.save(user4);

        HykeUser user5 = new HykeUser();
        user5.setEmail("stjepan.blazevic@email.com");
        user5.setFirstName("Stjepan");
        user5.setLastName("Balzevic");
        user5.setPassword(passwordEncoder.encode("stjepanpass"));
        user5.setUsername("sblazevic");
        user5.setRole(Role.USER);
        hykeUserRepository.save(user5);

        HykeUser user6 = new HykeUser();
        user6.setEmail("zeljko.bozic@email.com");
        user6.setFirstName("Željko");
        user6.setLastName("Božić");
        user6.setPassword(passwordEncoder.encode("bozicpass"));
        user6.setUsername("zbozic");
        user6.setRole(Role.DUTY);
        hykeUserRepository.save(user6);

        HykeUser user7 = new HykeUser();
        user7.setEmail("mirko.horvat@email.com");
        user7.setFirstName("Mirko");
        user7.setLastName("Horvat");
        user7.setPassword(passwordEncoder.encode("mirkopass"));
        user7.setUsername("hmirko");
        user7.setRole(Role.ADMIN);
        hykeUserRepository.save(user7);

        HykeUser user8 = new HykeUser();
        user8.setEmail("klara.medić@email.com");
        user8.setFirstName("Klara");
        user8.setLastName("Medić");
        user8.setPassword(passwordEncoder.encode("klarapass"));
        user8.setUsername("kmedić");
        user8.setRole(Role.USER);
        hykeUserRepository.save(user8);

        HykeUser user9 = new HykeUser();
        user9.setEmail("patrik.zvijezda@email.com");
        user9.setFirstName("Patrik");
        user9.setLastName("Zvijezda");
        user9.setPassword(passwordEncoder.encode("patrikpass"));
        user9.setUsername("patrik");
        user9.setRole(Role.USER);
        hykeUserRepository.save(user9);

        HykeUser user10 = new HykeUser();
        user10.setEmail("edgar.poe@email.com");
        user10.setFirstName("Edgar");
        user10.setLastName("Poe");
        user10.setPassword(passwordEncoder.encode("edgarpass"));
        user10.setUsername("epoe");
        user10.setRole(Role.DUTY);
        hykeUserRepository.save(user10);

        HykeUser user11 = new HykeUser();
        user11.setEmail("stjepan.bilić@email.com");
        user11.setFirstName("Stjepan");
        user11.setLastName("Bilić");
        user11.setPassword(passwordEncoder.encode("stjepanpass"));
        user11.setUsername("sbilic");
        user11.setRole(Role.USER);
        hykeUserRepository.save(user11);
    }

    private void loadLocations() {

        location.setName("Medvednica");
        location.setDescription("Popularna lokacija za izlete sjeverno od Zagreba. Sadrži područje" +
                "proglašeno parkom prirode. Svjetski poznata po skijaškim natjecanjima.");
        locationRepository.save(location);

        Location location1 = new Location();
        location1.setName("Klek");
        location1.setDescription("Planina šumovitog podnožja koja, gledajući s istoka, " +
                "izgleda kao usnuli div. Kolijevka je hrvatskog planinarstva kako je" +
                "upravo ovdje bila prva škola hrvatskih alpinista.");
        locationRepository.save(location1);

        Location location2 = new Location();
        location2.setName("Bjelolasica");
        location2.setDescription("Najviša planina Gorskog kotara. Prirodne ljepote čine " +
                "ovu lokaciju čestim odredištem biciklista i planinara.");
        locationRepository.save(location2);

        Location location3 = new Location();
        location3.setName("Učka");
        location3.setDescription("Najviša planina Istre. U njenom planinskom masivu " +
                "nalazi se i Park prirode Učka proglašen zaštićenim 1999. godine. " +
                "Na najvišem vrhu imena Vojak nalazi se toranj unutar kojeg je suvenirnica " +
                "parka prirode. ");
        locationRepository.save(location3);

        Location location4 = new Location();
        location4.setName("Papuk");
        location4.setDescription("Planina istočne Hrvatske koja se nalazi " +
                "sjeverno od Požeške kotline, Papuk je proglašen parkom prirode " +
                "1999. godine. Uz taj status, Papuk je od 2007. godine geopark, " +
                "i to prvi geopark u Hrvatskoj.");
        locationRepository.save(location4);

        Location location5= new Location();
        location5.setName("Kamešnica");
        location5.setDescription("Kao nastavak Dinarskog lanca, Kamešnica krasi granicu Hrvatske te " +
                "Bosne i Hercegovine. Prekrasna je planina za planinarenje u proljeće i jesen.");
        locationRepository.save(location5);

        Location location6= new Location();
        location6.setName("Psunj");
        location6.setDescription("Planina istočne Hrvatske koja se odlikuje kao najviša planina " +
                "Slavonije. Visoki dijelovi planine prepuni su izvora i prekrive šumom, " +
                "dok se na nižim dijelovima nalaze vinogradi i voćnjaci.");
        locationRepository.save(location6);

        Location location7= new Location();
        location7.setName("Dinara");
        location7.setDescription("Planina Dinarskog gorja koja čini prirodnu granicu između Bosne i " +
                "Hercegovine te Hrvatske. Područje je bogato raznolikim biljni te životnijskim svijetom. " );
        locationRepository.save(location7);

        Location location8= new Location();
        location8.setName("Kalnik");
        location8.setDescription("Gora sjeverozapadne Hrvatske koja se nalazi većim dijelom u " +
                "Varaždinskoj, a manjim dijelom u Koprivničko-križevačkoj županiji. Privlači planinare, " +
                "penjače i druge izletnike.");
        locationRepository.save(location8);

        Location location9= new Location();
        location9.setName("Žumberak");
        location9.setDescription("Gorje na sjeverozapadu Hrvatske, dijelom u Hrvatskoj, a dijelom u Sloveniji. " +
                "Najviši vrh gore je Sveta Gera. Veći dio gore zaštićen je " +
                "kao Park Prirode Žumberak-Samoborsko gorje.");
        locationRepository.save(location9);

        Location location10= new Location();
        location10.setName("Kozjak");
        location10.setDescription("Brdo koje sa sjeverne strane okružuje grad Kaštela. Ime Kozjak vjerojatno potječe " +
                "od grčkog tragos = koza, čemu svjedoči i ime obližnjeg Trogira. Najupečatljiviji dio, Greda od Kozjaka," +
                " najveća je stijena u Hrvatskoj.");
        locationRepository.save(location10);

        Location location11= new Location();
        location11.setName("Krndija");
        location11.setDescription("Gora u Slavoniji koja se nadovezuje na Papuk. Zajedno s Papukom i " +
                "Psunjem tvori najstarije dijelove tzv. Slavonskih planina, a ujedno i najstarije stijene " +
                "koje nalazimo u Hrvatskoj.");
        locationRepository.save(location11);

        Location location12= new Location();
        location12.setName("Snježnik");
        location12.setDescription("Najviša planina Hrvatskoj zagorja.");
        locationRepository.save(location12);

        Location location13= new Location();
        location13.setName("Ivanščica");
        location13.setDescription("Najviša planina Hrvatskog zagorja. Unatoč pitomom izgledu, u planini ima " +
                "velikih strmina, pa i golih stijena. Visoke i niske dijelove krase miješane šume.");
        locationRepository.save(location13);

        Location location14= new Location();
        location14.setName("Biokovo");
        location14.setDescription("Planinski masiv u Spitsko-dalmatinskoj županiji. Najviši vrh planine je" +
                " Sveti Jure, koji je ujedno treći najviši planinski vrh Hrvatske. Sjeverni dio planine " +
                "proglašen je Parkom prirode 1981. godine.");
        locationRepository.save(location14);


    }

    private void loadMountainHuts() {

        //medvednica

        mountainHut.setLocation(location);
        mountainHut.setName("Planinarski dom Risnjak");
        mountainHut.setDescription("Planinarski dom Risnjak lijepa je prizemnica s kuhinjom, " +
                "dvije blagovaonice te dvije spavaonice, a smještena je na malom šumskom proplanku " +
                "u području Pongračeva. Otvoreno vikendom.");
        mountainHut.setFood(true);
        mountainHut.setWater(true);
        mountainHut.setElectricity(false);
        mountainHut.setSleepover(false);
        mountainHutRepository.save(mountainHut);

        MountainHut mountainHut1 = new MountainHut();
        mountainHut1.setLocation(location);
        mountainHut1.setName("Planinarski dom Grafičar");
        mountainHut1.setDescription("Planinarski dom Grafičar izgradilo je 1954."
                + " godine planinarsko društvo Grafičar. U prizemlju je malena blagovaonica " +
                "i kuhinja, a na katu su spavaonice.Otvoreno stalno, osim ponedjeljkom.");
        mountainHut1.setFood(true);
        mountainHut1.setWater(true);
        mountainHut1.setElectricity(false);
        mountainHut1.setSleepover(true);
        mountainHutRepository.save(mountainHut1);

        MountainHut mountainHut2 = new MountainHut();
        mountainHut2.setLocation(location);
        mountainHut2.setName("Planinarski dom Runolist");
        mountainHut2.setDescription("Planinarski dom Runolist sagradilo je 1936."
                + " godine Planinarsko društvo Runolist prema projektu arhitekta Vladimira Streka. " +
                "U prizemlju su blagovaonica i mala školska dvorana, a na katovima spavaonice. Otvoreno stalno.");
        mountainHut2.setFood(true);
        mountainHut2.setWater(true);
        mountainHut2.setElectricity(false);
        mountainHut2.setSleepover(true);
        mountainHutRepository.save(mountainHut2);

        MountainHut mountainHut3 = new MountainHut();
        mountainHut3.setLocation(location);
        mountainHut3.setName("Puntijarka");
        mountainHut3.setDescription("Planinarski dom Ivan Pačkovski (ili poznatiji pod nazivom Puntijarka)"
                + " nalazi se na 957 m nadmorske visine");
        mountainHut3.setFood(true);
        mountainHut3.setWater(true);
        mountainHut3.setElectricity(true);
        mountainHut3.setSleepover(false);
        mountainHutRepository.save(mountainHut3);

        //klek
        MountainHut mountainHut5 = new MountainHut();
        mountainHut5.setLocation(locationRepository.findByName("Klek"));
        mountainHut5.setName("Planinarski dom Klek");
        mountainHut5.setDescription("Na hrptu Kleka, ovaj je dom na vrhu strmih šumovitih padina. U prizemlju su dvije blagovaonice, kuhinja i soba dežurnoga, " +
                "a u potkrovlju spavaonice. Nema stalnog domara. Otvoreno vikendom.");
        mountainHut5.setFood(false);
        mountainHut5.setWater(true);
        mountainHut5.setElectricity(false);
        mountainHut5.setSleepover(true);
        mountainHutRepository.save(mountainHut5);

        //bjelolasica
        MountainHut mountainHut6 = new MountainHut();
        mountainHut6.setLocation(locationRepository.findByName("Klek"));
        mountainHut6.setName("Planinarsko sklonište Jakob Mihelčić");
        mountainHut6.setDescription("Planinarsko sklonište drvena je kućica smještena pod " +
                "samim hrptom Bjelolasice. Unutrašnjost je mala blagovaonica s drvenim namještajem i dva stola. Otvoreno stalno.");
        mountainHut6.setFood(false);
        mountainHut6.setWater(false);
        mountainHut6.setElectricity(false);
        mountainHut6.setSleepover(true);
        mountainHutRepository.save(mountainHut6);

        //papuk
        MountainHut mountainHut4 = new MountainHut();
        mountainHut4.setLocation(locationRepository.findByName("Papuk"));
        mountainHut4.setName("Jankovac");
        mountainHut4.setDescription("Planinarski dom Jankovac solidna je zidana zgrada . Ima veliku" +
                " blagovaonicu, elektrificiran je, ima vodovod i 10 spavaonica. Objekt je luksuzno opremljen i nudi punu hotelsku i restoransku uslugu.");
        mountainHut4.setFood(true);
        mountainHut4.setWater(true);
        mountainHut4.setElectricity(true);
        mountainHut4.setSleepover(true);
        mountainHutRepository.save(mountainHut4);

        MountainHut mountainHut7 = new MountainHut();
        mountainHut7.setLocation(locationRepository.findByName("Papuk"));
        mountainHut7.setName("Planinarska kuća Lapjak");
        mountainHut7.setDescription("Planinarska kuća Lapjak nalazi se u Velikoj, iznad termalnog kupališta," +
                " na kraju doline rječice Dubočanke. Otvoreno i opskrbljeno po dogovoru s upravljačem.");
        mountainHut7.setFood(false);
        mountainHut7.setWater(false);
        mountainHut7.setElectricity(false);
        mountainHut7.setSleepover(true);
        mountainHutRepository.save(mountainHut7);

        MountainHut mountainHut8 = new MountainHut();
        mountainHut8.setLocation(locationRepository.findByName("Papuk"));
        mountainHut8.setName("Planinarska kuća Trišnjica");
        mountainHut8.setDescription("Planinarska kuća Trišnjica mala je zidana prizemnica. Kuća je otvorena i opskrbljena po dogovoru. " +
                "U potkrovlju ima 10 ležaja.");
        mountainHut8.setFood(false);
        mountainHut8.setWater(false);
        mountainHut8.setElectricity(false);
        mountainHut8.setSleepover(true);
        mountainHutRepository.save(mountainHut8);

        MountainHut mountainHut9 = new MountainHut();
        mountainHut9.setLocation(locationRepository.findByName("Papuk"));
        mountainHut9.setName("Planinarska kuća Crni vrh");
        mountainHut9.setDescription("Planinarsko sklonište Crni vrh je jednostavan maleni metalni kontejner postavljen 20 metara " +
                "od najviše točke na Crnom vrhu. Podržava noćenje za nekoliko osoba. Otvoreno stalno.");
        mountainHut9.setFood(false);
        mountainHut9.setWater(false);
        mountainHut9.setElectricity(false);
        mountainHut9.setSleepover(true);
        mountainHutRepository.save(mountainHut9);

        //učka
        //kamešnica

        //psunj
        MountainHut mountainHut10 = new MountainHut();
        mountainHut10.setLocation(locationRepository.findByName("Psunj"));
        mountainHut10.setName("Planinarski dom Omanovac");
        mountainHut10.setDescription("Planinarski dom Omanovac nalazi se na najvišoj točki gorske kose Omanovac iznad sela Šeovice. " +
                "Dom " +
                "je elektrificiran i ima vodovod. Otvoreno stalno osim ponedjeljkom.");
        mountainHut10.setFood(true);
        mountainHut10.setWater(true);
        mountainHut10.setElectricity(true);
        mountainHut10.setSleepover(true);
        mountainHutRepository.save(mountainHut10);

        MountainHut mountainHut11 = new MountainHut();
        mountainHut11.setLocation(locationRepository.findByName("Psunj"));
        mountainHut11.setName("Planinarska kuća Strmac");
        mountainHut11.setDescription("Planinarska kuća Strmac nalazi se na rubu livade i šume. Kuća služi kao polazište " +
                "za uspone prema Brezovu polju. Otvoreno i opskrbljeno po dogovoru.");
        mountainHut11.setFood(false);
        mountainHut11.setWater(false);
        mountainHut11.setElectricity(false);
        mountainHut11.setSleepover(true);
        mountainHutRepository.save(mountainHut11);

        //kalnik
        MountainHut mountainHut12 = new MountainHut();
        mountainHut12.setLocation(locationRepository.findByName("Kalnik"));
        mountainHut12.setName("Planinarski dom Kalnik");
        mountainHut12.setDescription("Planinarski dom Kalnik sagrađen je između vrha Vranilca i ruševina staroga grada Velikog Kalnika." +
                "Dom je velik, s dvije velike blagovaonice i kuhinjom u prizemlju te 11 spavaonica na katu. Nudi kompletnu restoransku uslugu.");
        mountainHut12.setFood(true);
        mountainHut12.setWater(true);
        mountainHut12.setElectricity(false);
        mountainHut12.setSleepover(true);
        mountainHutRepository.save(mountainHut12);

        //žumberak
        MountainHut mountainHut13 = new MountainHut();
        mountainHut13.setLocation(locationRepository.findByName("Žumberak"));
        mountainHut13.setName("Planinarska kuća Scout");
        mountainHut13.setDescription("Planinarska kuća Scout nalazi se u Noršićkom jarku. " +
                "Kuća sadrži dvije blagovaonice, malu kuhinja te spavaonice. Otvoren vikendom od 1.4. do 31.10., a zimi po dogovoru. Opskrbljeno po dogovoru.");
        mountainHut13.setFood(false);
        mountainHut13.setWater(false);
        mountainHut13.setElectricity(false);
        mountainHut13.setSleepover(true);
        mountainHutRepository.save(mountainHut13);

        MountainHut mountainHut14 = new MountainHut();
        mountainHut14.setLocation(locationRepository.findByName("Žumberak"));
        mountainHut14.setName("Planinarski dom Žitnica");
        mountainHut14.setDescription("Planinarski dom Žitnica na Japetiću nalazi se oko 1 km jugozapadno od vrha Japetića, na vrhu strme livade Žitnice." +
                " U zidanom prizemlju je blagovaonica i kuhinja, a na drvenom katu su spavaonice. Otvoreno vikendom.");
        mountainHut14.setFood(true);
        mountainHut14.setWater(true);
        mountainHut14.setElectricity(false);
        mountainHut14.setSleepover(true);
        mountainHutRepository.save(mountainHut14);

        MountainHut mountainHut15 = new MountainHut();
        mountainHut15.setLocation(locationRepository.findByName("Žumberak"));
        mountainHut15.setName("Planinarska kuća Vodice");
        mountainHut15.setDescription("Planinarska kuća Vodice nalazi se iznad Sošica. " +
                "To je jednostavna prizemnica s kuhinjom, blagovaonicom i dvije spavaonice. Otvoreno vikendom od 1. 5 do 31. 10., a zimi po dogovoru. " +
                "Opskrbljeno po dogovoru.");
        mountainHut15.setFood(false);
        mountainHut15.setWater(false);
        mountainHut15.setElectricity(false);
        mountainHut15.setSleepover(true);
        mountainHutRepository.save(mountainHut15);

        MountainHut mountainHut16 = new MountainHut();
        mountainHut16.setLocation(locationRepository.findByName("Žumberak"));
        mountainHut16.setName("Planinarski dom Željezničar");
        mountainHut16.setDescription("Planinarski dom Željezničar na Oštrcu nalazi se na malenom gorskom sedlu ispod vrha Oštrca." +
                " Sadrži su blagovaonicu, kuhinju, sanitarni čvor, pomoćnu prostoriju te spavaonice. " +
                " Otvoreno vikendom.");
        mountainHut16.setFood(true);
        mountainHut16.setWater(true);
        mountainHut16.setElectricity(false);
        mountainHut16.setSleepover(true);
        mountainHutRepository.save(mountainHut16);

        //kozjak
        MountainHut mountainHut17 = new MountainHut();
        mountainHut17.setLocation(locationRepository.findByName("Kozjak"));
        mountainHut17.setName("Planinarski dom Putalj");
        mountainHut17.setDescription("Planinarski dom Putalj smješten je na malom obronku podno južne stijene Kozjaka. " +
                "Jedan je od najvećih u Dalmaciji. Ima dvije blagovaonice, kuhinju te nekoliko spavaonica. " +
                "Otvoreno stalno.");
        mountainHut17.setFood(true);
        mountainHut17.setWater(true);
        mountainHut17.setElectricity(false);
        mountainHut17.setSleepover(true);
        mountainHutRepository.save(mountainHut17);

        MountainHut mountainHut18 = new MountainHut();
        mountainHut18.setLocation(locationRepository.findByName("Kozjak"));
        mountainHut18.setName("Planinarski dom Malačka");
        mountainHut18.setDescription("Planinarski dom Malačka zidana je kuća smještena na samome prijevoju. " +
                "Dom ima boravak, pet spavaonica s ukupno 57 ležaja, kuhinju, cisternu te sanitarni čvor. Otvoreno vikendom.");
        mountainHut18.setFood(true);
        mountainHut18.setWater(true);
        mountainHut18.setElectricity(false);
        mountainHut18.setSleepover(true);
        mountainHutRepository.save(mountainHut18);

        MountainHut mountainHut19 = new MountainHut();
        mountainHut19.setLocation(locationRepository.findByName("Kozjak"));
        mountainHut19.setName("Planinarska kuća Pod Koludrom");
        mountainHut19.setDescription("Planinarska kuća Pod Koludrom zidana je prizemnica. " +
                "Sastoji se od dvije prostorije " +
                "(dnevna velika prostorija i blagovaonica). Opskrbljeno i otvoreno po dogovoru.");
        mountainHut19.setFood(false);
        mountainHut19.setWater(false);
        mountainHut19.setElectricity(false);
        mountainHut19.setSleepover(false);
        mountainHutRepository.save(mountainHut19);

        MountainHut mountainHut20 = new MountainHut();
        mountainHut20.setLocation(locationRepository.findByName("Kozjak"));
        mountainHut20.setName("Planinarska kuća Česma");
        mountainHut20.setDescription("Planinarska kuća Česmina na Malački nalazi se na hrptu zapadnog dijela Kozjaka, neposredno " +
                "pokraj dalekovoda. To je zidana prizemnica s blagovaonicom za 30 osoba i spavaonicom. Otvoreno i opskrbljeno po dogovoru.");
        mountainHut20.setFood(false);
        mountainHut20.setWater(false);
        mountainHut20.setElectricity(false);
        mountainHut20.setSleepover(true);
        mountainHutRepository.save(mountainHut20);

        MountainHut mountainHut21 = new MountainHut();
        mountainHut21.setLocation(locationRepository.findByName("Kozjak"));
        mountainHut21.setName("Planinarsko sklonište Orlovo gnijezdo");
        mountainHut21.setDescription("Planinarsko sklonište Orlovo gnijezdo simpatična je kućica uzidana na rubu stijene Koludra. " +
                "Ima otvoreno ognjište, stol i klupe te se u njega može smjestiti 10 ljudi. Otvoreno je stalno.");
        mountainHut21.setFood(false);
        mountainHut21.setWater(false);
        mountainHut21.setElectricity(false);
        mountainHut21.setSleepover(false);
        mountainHutRepository.save(mountainHut21);

        //krndija
        MountainHut mountainHut22 = new MountainHut();
        mountainHut22.setLocation(locationRepository.findByName("Krndija"));
        mountainHut22.setName("Planinarska kuća Tivanovo");
        mountainHut22.setDescription("Planinarska kuća Tivanovo nalazi se na kraju sela Gazija, na sjevernom podnožju Krndije. " +
                "Raspolaže sa 20 kreveta s posteljinom te jednim pomoćnim krevetom, a pokraj kuće je teren pogodan i za kampiranje. " +
                "Otvoreno i opskrbljeno po dogovoru.");
        mountainHut22.setFood(false);
        mountainHut22.setWater(false);
        mountainHut22.setElectricity(false);
        mountainHut22.setSleepover(true);
        mountainHutRepository.save(mountainHut22);

        MountainHut mountainHut23 = new MountainHut();
        mountainHut23.setLocation(locationRepository.findByName("Krndija"));
        mountainHut23.setName("Planinarska kuća Šaševo");
        mountainHut23.setDescription("Planinarska kuća Šaševo zidana je šumarska prizemnica koje se nalazi se u šumovitoj dolini " +
                "na sjevernoj strani Krndije, u prijelaznom " +
                "području prema Papuku. Otvoreno po dogovoru.");
        mountainHut23.setFood(false);
        mountainHut23.setWater(false);
        mountainHut23.setElectricity(false);
        mountainHut23.setSleepover(true);
        mountainHutRepository.save(mountainHut23);

        MountainHut mountainHut24 = new MountainHut();
        mountainHut24.setLocation(locationRepository.findByName("Krndija"));
        mountainHut24.setName("Planinarska kuća Borovik");
        mountainHut24.setDescription("Planinarska kuća Borovik nalazi se uz makadamsku cestu prema Paučju, u dnu šumovite udoline, " +
                "malo izmaknuta od jezera Borovik. Uz kuću je malo igralište i nadstrešnica za odmor. Otvoreno po dogovoru.");
        mountainHut24.setFood(false);
        mountainHut24.setWater(false);
        mountainHut24.setElectricity(false);
        mountainHut24.setSleepover(true);
        mountainHutRepository.save(mountainHut24);

        MountainHut mountainHut25 = new MountainHut();
        mountainHut25.setLocation(locationRepository.findByName("Krndija"));
        mountainHut25.setName("Planinarska kuća Mlaka");
        mountainHut25.setDescription("Planinarska kuća Mlaka nalazi se na južnoj strani Krndije, iznad sela Mitrovca u blizini Kutjeva. " +
                "To je jednostavna drvena brvnara s blagovaonicom i spavaonicom u potkrovlju. Otvoreno i opskrbljeno po dogovoru.");
        mountainHut25.setFood(false);
        mountainHut25.setWater(false);
        mountainHut25.setElectricity(false);
        mountainHut25.setSleepover(true);
        mountainHutRepository.save(mountainHut25);

        //snježnik

        //dinara
        MountainHut mountainHut26 = new MountainHut();
        mountainHut26.setLocation(locationRepository.findByName("Dinara"));
        mountainHut26.setName("Planinarska kuća Brezovac");
        mountainHut26.setDescription("Planinarska kuća Brezovac nalazi se u šumi, na rubu prostranog dolca Brezovca. " +
                ". Planinarska kuća ima planinarsku blagovaonicu," +
                "te dvije spavaonice. Uz kuću je cisterna s vodom. Otvoreno po dogovoru.");
        mountainHut26.setFood(false);
        mountainHut26.setWater(false);
        mountainHut26.setElectricity(false);
        mountainHut26.setSleepover(true);
        mountainHutRepository.save(mountainHut26);

        MountainHut mountainHut27 = new MountainHut();
        mountainHut27.setLocation(locationRepository.findByName("Dinara"));
        mountainHut27.setName("Planinarsko sklonište Martinova košara");
        mountainHut27.setDescription("Planinarsko sklonište Martinova košara nalazi se u Donjim Torinama. To je stari pastirski " +
                "stan uređen kao jednostavno sklonište u koje se na noćenje može smjestiti desetak ljudi. Otvoreno stalno.");
        mountainHut27.setFood(false);
        mountainHut27.setWater(false);
        mountainHut27.setElectricity(false);
        mountainHut27.setSleepover(true);
        mountainHutRepository.save(mountainHut27);

        MountainHut mountainHut28 = new MountainHut();
        mountainHut28.setLocation(locationRepository.findByName("Dinara"));
        mountainHut28.setName("Planinarsko sklonište Pume");
        mountainHut28.setDescription("To je jednostavan zidani objekt s drvenim potkrovljem te natkrivenom blagovaonicom. " +
                "Kuća je stalno otvorena.");
        mountainHut28.setFood(false);
        mountainHut28.setWater(false);
        mountainHut28.setElectricity(false);
        mountainHut28.setSleepover(true);
        mountainHutRepository.save(mountainHut28);

        //ivančšcica
        MountainHut mountainHut29 = new MountainHut();
        mountainHut29.setLocation(locationRepository.findByName("Ivanščica"));
        mountainHut29.setName("Planinarski dom Pasarićeva kuća");
        mountainHut29.setDescription("Planinarski dom Pasarićeva kuća lijepa je zidana prizemnica s " +
                "dvije blagovaonice, kuhinjom, smočnicom i predsobljem. U potkrovlju su dvije spavaonice. " +
                "Ima grijanje u svim sobama, struju i pitku vodu. Otvoreno vikendom, blagdanom i po dogovoru.");
        mountainHut29.setFood(true);
        mountainHut29.setWater(true);
        mountainHut29.setElectricity(true);
        mountainHut29.setSleepover(true);
        mountainHutRepository.save(mountainHut29);

        //biokovo
        MountainHut mountainHut30 = new MountainHut();
        mountainHut30.setLocation(locationRepository.findByName("Biokovo"));
        mountainHut30.setName("Planinarska kuća Bukovac");
        mountainHut30.setDescription("Planinarska kuća Bukovac malena je zidana prizemnica u šumovitoj udolini na sjeverozapadnom " +
                "dijelu Biokova. Kuća ima plinsko kuhalo i agregat za el. energiju te cisternu s pitkom vodom. Otvoreno po dogovoru.");
        mountainHut30.setFood(false);
        mountainHut30.setWater(true);
        mountainHut30.setElectricity(true);
        mountainHut30.setSleepover(true);
        mountainHutRepository.save(mountainHut30);

        MountainHut mountainHut31 = new MountainHut();
        mountainHut31.setLocation(locationRepository.findByName("Biokovo"));
        mountainHut31.setName("Planinarska kuća Slobodan Ravlić");
        mountainHut31.setDescription("Planinarska kuća Slobodan Ravlić na Lokvi zidana je prizemnica. " +
                "Poznata je kao lugarnica na Lokvi, a nazvana je po makarskom planinaru koji je 1981. poginuo na Biokovu. " +
                "Otvoreno po dogovoru, a po ljeti su dežurstva vikendom.");
        mountainHut31.setFood(false);
        mountainHut31.setWater(false);
        mountainHut31.setElectricity(false);
        mountainHut31.setSleepover(true);
        mountainHutRepository.save(mountainHut31);



    }
    
    private void loadHikes() {
        //medvednica
        hike.setLocation(location);
        hike.setName("Kratki izlet za početak godine");
        hike.setDescription("Otvaramo novogodišnje planinarenje ovim izletom.");
        hike.setHikeStart("Mihaljevac");
        hike.setHikeEnd("Mihaljevac");
        hike.setHikeLength(5.5);
        hike.setIntensity(HikeIntensity.MODERATE);
        hike.setPublicity(true);
        hike.setCreator(user);
        hike.setCreatedAt(new Date(1610188338000l));
        hikeRepository.save(hike);

        Hike hike1 = new Hike();
        hike1.setLocation(locationRepository.findByName("Medvednica"));
        hike1.setName("Šestine - PD Grafičar");
        hike1.setDescription("Izleti u organizaciji Vikendom na Medvednicu besplatni su,"
        		+ " otvoreni za sve, nema članarine i nije potrebno prethodno se najavljivati.");
        hike1.setHikeStart("Šestine");
        hike1.setHikeEnd("Grafičar");
        hike1.setHikeLength(6.2);
        hike1.setIntensity(HikeIntensity.MODERATE);
        hike1.setPublicity(true);
        hike1.setCreator(user);
        hike1.setCreatedAt(new Date(1610188338000l - 1279693l));
        Set<MountainHut> huts = new HashSet<>();
        huts.add(mountainHutRepository.findByName("Planinarski dom Grafičar"));
        hike1.setMountainHuts(huts);
        hikeRepository.save(hike1);

        Hike hike2 = new Hike();
        hike2.setLocation(locationRepository.findByName("Medvednica"));
        hike2.setName("MEDVEDNICA: pl. dom Runolist, Sljeme, pl.dom Puntijarka");
        hike2.setDescription("PLANINARSKO DRUŠTVO PINKLEC SVETA NEDELJA "
        		+ "organizira jednodnevni planinarski izlet");
        hike2.setHikeStart("Bliznec");
        hike2.setHikeEnd("Puntijarka");
        hike2.setHikeLength(10.0);
        hike2.setIntensity(HikeIntensity.LOW);
        hike2.setPublicity(true);
        hike2.setCreator(user);
        hike2.setCreatedAt(new Date(1610025647000l));
        Set<MountainHut> huts1 = new HashSet<>();
        huts1.add(mountainHutRepository.findByName("Planinarski dom Runolist"));
        huts1.add(mountainHutRepository.findByName("Planinarski dom Puntijarka"));
        hike1.setMountainHuts(huts1);
        hikeRepository.save(hike2);


        //krndija
        Hike hike3 = new Hike();
        hike3.setLocation(locationRepository.findByName("Krndija"));
        hike3.setName("Moj prvi izlet");
        hike3.setDescription("Ovo je moj prvi izlet. Nadam se da će vam se svidjeti. " +
                "Početak je u Orahovici i nakon nekoliko dana vraćamo se natrag.");
        hike3.setHikeStart("Orahovica");
        hike3.setHikeEnd("Orahovica");
        hike3.setHikeLength(50.0);
        hike3.setIntensity(HikeIntensity.HIGH);
        hike3.setPublicity(true);
        hike3.setCreator(hykeUserRepository.findByUsername("kmedić"));
        hike3.setCreatedAt(new Date());
        hikeRepository.save(hike3);
        Set<MountainHut> huts3 = new HashSet<>();
        huts3.add(mountainHutRepository.findByName("Planinarska kuća Šaševo"));
        huts3.add(mountainHutRepository.findByName("Planinarska kuća Borovik"));
        huts3.add(mountainHutRepository.findByName("Planinarska kuća Mlaka"));
        hike3.setMountainHuts(huts3);
        hikeRepository.save(hike3);

        Hike hike4 = new Hike();
        hike4.setLocation(locationRepository.findByName("Krndija"));
        hike4.setName("Izlet- Borovik");
        hike4.setDescription("Kratki izlet do doma Borovik.");
        hike4.setHikeStart("Kutjevo");
        hike4.setHikeEnd("Planinarska kuća Borovik.");
        hike4.setHikeLength(8.0);
        hike4.setIntensity(HikeIntensity.MODERATE);
        hike4.setPublicity(true);
        hike4.setCreator(hykeUserRepository.findByUsername("hmirko"));
        hike4.setCreatedAt(new Date(1610025647000l + 21444212l));
        Set<MountainHut> huts4 = new HashSet<>();
        huts4.add(mountainHutRepository.findByName("Planinarska kuća Borovik"));
        hike4.setMountainHuts(huts4);
        hikeRepository.save(hike4);

        //klek
        Hike hike5 = new Hike();
        hike5.setLocation(locationRepository.findByName("Klek"));
        hike5.setName("Lagana šetnja");
        hike5.setDescription("Ovo je moj prvi odlazak na ovo mjesto. Mislila sam se malo prošetati " +
                "da vidim kako mi se sviđa. Izlet traje koliko god želite, dok vam ne dosadi. ");
        hike5.setHikeStart("Ogulin");
        hike5.setHikeEnd("Ogulin");
        hike5.setHikeLength(1.0);
        hike5.setIntensity(HikeIntensity.LOW);
        hike5.setPublicity(true);
        hike5.setCreator(hykeUserRepository.findByUsername("apavlovic"));
        hike5.setCreatedAt(new Date(1610352013000l));
        hikeRepository.save(hike5);


        //papuk
        Hike hike6 = new Hike();
        hike6.setLocation(locationRepository.findByName("Papuk"));
        hike6.setName("Uspon do Jankovca");
        hike6.setDescription("Bio sam jednom na Papuku kao mali, pa organiziram ovaj izlet kao " +
                "povratak nakon dosta godina. Prekrasno mjesto.");
        hike6.setHikeStart("Voćin");
        hike6.setHikeEnd("Dom Jankovac");
        hike6.setHikeLength(43.0);
        hike6.setIntensity(HikeIntensity.MODERATE);
        hike6.setPublicity(true);
        hike6.setCreator(hykeUserRepository.findByUsername("sbilic"));
        hike6.setCreatedAt(new Date(1609762261000l));
        Set<MountainHut> huts6 = new HashSet<>();
        huts6.add(mountainHutRepository.findByName("Planinarski dom Jankovac"));
        hike6.setMountainHuts(huts6);
        hikeRepository.save(hike6);

        Hike hike7 = new Hike();
        hike7.setLocation(locationRepository.findByName("Papuk"));
        hike7.setName("Park prirode Papuk");
        hike7.setDescription("Ovaj izlet vodi do parka prirode.");
        hike7.setHikeStart("Kutjevo");
        hike7.setHikeEnd("Park prirode Papuk");
        hike7.setHikeLength(18.0);
        hike7.setIntensity(HikeIntensity.MODERATE);
        hike7.setPublicity(true);
        hike7.setCreator(hykeUserRepository.findByUsername("patrik"));
        hike7.setCreatedAt(new Date(1609762261000l + 124321467l));
        hikeRepository.save(hike7);

        Hike hike8 = new Hike();
        hike8.setLocation(locationRepository.findByName("Papuk"));
        hike8.setName("Pregled moguće rute");
        hike8.setDescription("Prije nego napravim javni izlet, zanima me kako će izgledati. " +
                "Ukoliko mi se svidi, napravit ću javni izlet po ovome.");
        hike8.setHikeStart("Mikleuš");
        hike8.setHikeEnd("Kutjevo");
        hike8.setHikeLength(65.0);
        hike8.setIntensity(HikeIntensity.VERY_HIGH);
        hike8.setPublicity(false);
        hike8.setCreator(hykeUserRepository.findByUsername("zbozic"));
        hike8.setCreatedAt(new Date(1610646898000l));
        Set<MountainHut> huts8 = new HashSet<>();
        huts8.add(mountainHutRepository.findByName("Planinarski dom Jankovac"));
        huts8.add(mountainHutRepository.findByName("Planinarski dom Lapjak"));
        huts8.add(mountainHutRepository.findByName("Planinarski dom Trišnjica"));
        huts8.add(mountainHutRepository.findByName("Planinarski dom Crni vrh"));
        hike8.setMountainHuts(huts8);
        hikeRepository.save(hike8);

        //psunj
        Hike hike9 = new Hike();
        hike9.setLocation(locationRepository.findByName("Psunj"));
        hike9.setName("Moja šetnjica");
        hike9.setDescription("Šetnja do Omanovca na klopu.");
        hike9.setHikeStart("podnožje Psunja");
        hike9.setHikeEnd("Omanovac");
        hike9.setHikeLength(1.0);
        hike9.setIntensity(HikeIntensity.LOW);
        hike9.setPublicity(false);
        hike9.setCreator(hykeUserRepository.findByUsername("zbozic"));
        hike9.setCreatedAt(new Date(1610646898000l - 1223124l));
        Set<MountainHut> huts9 = new HashSet<>();
        huts9.add(mountainHutRepository.findByName("Planinarski dom Omanovac"));
        hike9.setMountainHuts(huts9);
        hikeRepository.save(hike9);


        //primjer izleta s netočnim informacijama: domovi koji nisu na tom području
        Hike hike10 = new Hike();
        hike10.setLocation(locationRepository.findByName("Psunj"));
        hike10.setName("Obilazak domova");
        hike10.setDescription("Cilj je obići dom Omanovac i Strmac na putu do Lipika.");
        hike10.setHikeStart("Pakrac");
        hike10.setHikeEnd("Lipik");
        hike10.setHikeLength(90.0);
        hike10.setIntensity(HikeIntensity.MODERATE);
        hike10.setPublicity(true);
        hike10.setCreator(hykeUserRepository.findByUsername("jkovacevic"));
        hike10.setCreatedAt(new Date(1610616919000l));
        Set<MountainHut> huts10 = new HashSet<>();
        huts10.add(mountainHutRepository.findByName("Planinarski dom Omanovac"));
        huts10.add(mountainHutRepository.findByName("Planinarski dom Grafičar"));
        huts10.add(mountainHutRepository.findByName("Planinarski dom Risnjak"));
        huts10.add(mountainHutRepository.findByName("Planinarska kuća Strmac"));
        huts10.add(mountainHutRepository.findByName("Planinarski dom Klek"));
        hike10.setMountainHuts(huts10);
        hikeRepository.save(hike10);

        //kalnik
        Hike hike11 = new Hike();
        hike11.setLocation(locationRepository.findByName("Kalnik"));
        hike11.setName("Škola u prirodi");
        hike11.setDescription("Profesori srednje škole organiziraju izlet do Kalnika, " +
                "gdje će se održati predavanje o Starom gradu Kalnik. Svi su pozvani.");
        hike11.setHikeStart("crkva sv. Roka u Križevcima");
        hike11.setHikeEnd("Planinarski dom Kalnik");
        hike11.setHikeLength(21.0);
        hike11.setIntensity(HikeIntensity.MODERATE);
        hike11.setPublicity(true);
        hike11.setCreator(hykeUserRepository.findByUsername("mknezevic"));
        hike11.setCreatedAt(new Date(1609668241000l + 1241112l + 123142l));
        Set<MountainHut> huts11 = new HashSet<>();
        huts11.add(mountainHutRepository.findByName("Planinarski dom Kalnik"));
        hike11.setMountainHuts(huts11);
        hikeRepository.save(hike11);

        //žumberak
        Hike hike12 = new Hike();
        hike12.setLocation(locationRepository.findByName("Žumberak"));
        hike12.setName("Odmor od grada");
        hike12.setDescription("Promislite o odlasku na ovaj izlet ako ste umorni " +
                "od svakidašnjeg života u gradu.");
        hike12.setHikeStart("Samobor");
        hike12.setHikeEnd("Samobor");
        hike12.setHikeLength(90.0);
        hike12.setIntensity(HikeIntensity.HIGH);
        hike12.setPublicity(true);
        hike12.setCreator(hykeUserRepository.findByUsername("mknezevic"));
        hike12.setCreatedAt(new Date(1609668241000l + 1241112l));
        Set<MountainHut> huts12 = new HashSet<>();
        huts12.add(mountainHutRepository.findByName("Planinarska kuća Vodice"));
        huts12.add(mountainHutRepository.findByName("Planinarska kuća Željezničar"));
        huts12.add(mountainHutRepository.findByName("Planinarska kuća Žitnica"));
        huts12.add(mountainHutRepository.findByName("Planinarska kuća Scout"));
        hike12.setMountainHuts(huts12);
        hikeRepository.save(hike12);

        //kozjak
        Hike hike13 = new Hike();
        hike13.setLocation(locationRepository.findByName("Kozjak"));
        hike13.setName("Izlet do Orlovog gnijezda");
        hike13.setDescription("HPD Ante Bedalov organizira izlet do planinarskog skloništa. " +
                "Učesnici sami plaćaju obrok na vrhu.");
        hike13.setHikeStart("Planinarska kuća Pod Koludrom");
        hike13.setHikeEnd("Planinarsko sklonište Orlovo gnijezdo");
        hike13.setHikeLength(0.5);
        hike13.setIntensity(HikeIntensity.LOW);
        hike13.setPublicity(true);
        hike13.setCreator(hykeUserRepository.findByUsername("patrik"));
        hike13.setCreatedAt(new Date(1609668241000l));
        Set<MountainHut> huts13 = new HashSet<>();
        huts13.add(mountainHutRepository.findByName("Planinarska kuća Pod Koludrom"));
        huts13.add(mountainHutRepository.findByName("Planinarsko sklonište Orlovo gnijezdo"));
        hike13.setMountainHuts(huts13);
        hikeRepository.save(hike13);

        Hike hike14 = new Hike();
        hike14.setLocation(locationRepository.findByName("Kozjak"));
        hike14.setName("Početnička ruta");
        hike14.setDescription("Ruta za početnike koja obuhvaća obilazak nekoliko domova.");
        hike14.setHikeStart("Planinarska kuća Pod Koludrom");
        hike14.setHikeEnd("Planinarska kuća Orlovo gnijezdo");
        hike14.setHikeLength(0.8);
        hike14.setIntensity(HikeIntensity.LOW);
        hike14.setPublicity(true);
        hike14.setCreator(hykeUserRepository.findByUsername("hmirko"));
        hike14.setCreatedAt(new Date(1609661622000l));
        Set<MountainHut> huts14 = new HashSet<>();
        huts14.add(mountainHutRepository.findByName("Planinarska kuća Pod Koludrom"));
        huts14.add(mountainHutRepository.findByName("Planinarsko sklonište Orlovo gnijezdo"));
        huts14.add(mountainHutRepository.findByName("Planinarska kuća Česmina"));
        hike14.setMountainHuts(huts14);
        hikeRepository.save(hike14);
    }

    private void loadEvents() {
        event.setHike(hike);
        event.setStartDateTime(new Date(1611126000000l));
        event.setEndDateTime(new Date(1611176400000l));
        event.setDescription("Posjet Medvednici na jedan dan.");
        event.setCreator(user);
        event.setCreatedAt(new Date());
        eventRepository.save(event);

        Event event1 = new Event();
        event1.setHike(hike);
        event1.setStartDateTime(new Date(1611136800000l));
        event1.setEndDateTime(new Date(1611136800000l + 2134412l));
        event1.setDescription("Jednodnevno druženje. Sve informacije zadane u nastavku.");
        event1.setCreator(hykeUserRepository.findByUsername("patrik"));
        event1.setCreatedAt(new Date());
        eventRepository.save(event1);

        Event event2 = new Event();
        event2.setHike(hikeRepository.findByName("Obilazak domova"));
        event2.setStartDateTime(new Date(1613296800000l - 93244451l));
        event2.setEndDateTime(new Date(1613296800000l));
        event2.setDescription("Upoznajmo lokalnu planinarsku zajednicu posjećujući neke od domova.");
        event2.setCreator(hykeUserRepository.findByUsername("epoe"));
        event2.setCreatedAt(new Date());
        eventRepository.save(event2);

        Event event3 = new Event();
        event3.setHike(hikeRepository.findByName("Početnička ruta"));
        event3.setStartDateTime(new Date(1614759000000l));
        event3.setEndDateTime(new Date(1614759000000l + 2142432l));
        event3.setDescription("Događaj s izletom za početnike.");
        event3.setCreator(hykeUserRepository.findByUsername("hmirko"));
        event3.setCreatedAt(new Date());
        eventRepository.save(event3);

        Event event4 = new Event();
        event4.setHike(hikeRepository.findByName("Odmor od grada"));
        event4.setStartDateTime(new Date(1612030500000l));
        event4.setEndDateTime(new Date(1612030500000l + 9882149l));
        event4.setDescription("Pođimo u prirodu odmoriti se od svakodnevice.");
        event4.setCreator(hykeUserRepository.findByUsername("kmedic"));
        event4.setCreatedAt(new Date());
        eventRepository.save(event4);

        Event event5 = new Event();
        event5.setHike(hikeRepository.findByName("Škola u prirodi"));
        event5.setStartDateTime(new Date(1618732200000l));
        event5.setEndDateTime(new Date(1618732200000l + 827482l));
        event5.setDescription("Pridružite nam se i saznajte nešto novo.");
        event5.setCreator(hykeUserRepository.findByUsername("kmedic"));
        event5.setCreatedAt(new Date());
        eventRepository.save(event5);

        Event event6 = new Event();
        event6.setHike(hikeRepository.findByName("Odmor od grada"));
        event6.setStartDateTime(new Date(1614594000000l));
        event6.setEndDateTime(new Date(1614594000000l + 4299291l));
        event6.setDescription("Moja verzija ovog izleta.");
        event6.setCreator(hykeUserRepository.findByUsername("ihorvat"));
        event6.setCreatedAt(new Date());
        eventRepository.save(event6);

        Event event7 = new Event();
        event7.setHike(hikeRepository.findByName("Uspon do Jankovca"));
        event7.setStartDateTime(new Date(1613550600000l));
        event7.setEndDateTime(new Date(1613550600000l + 13589201l));
        event7.setDescription("Pridružite se kako bismo se zajedno lijepo proveli.");
        event7.setCreator(hykeUserRepository.findByUsername("ihorvat"));
        event7.setCreatedAt(new Date());
        eventRepository.save(event7);

        Event event8 = new Event();
        event8.setHike(hikeRepository.findByName("Šestine - PD Grafičar"));
        event8.setStartDateTime(new Date(1610966400000l));
        event8.setEndDateTime(new Date(1610966400000l + 2419229l));
        event8.setDescription("Tradicijski izlet kojeg održavamo početkom svake godine.");
        event8.setCreator(hykeUserRepository.findByUsername("mknezevic"));
        event8.setCreatedAt(new Date());
        eventRepository.save(event8);

        Event event9 = new Event();
        event9.setHike(hikeRepository.findByName("Lagana šetnja"));
        event9.setStartDateTime(new Date(1611471600000l));
        event9.setEndDateTime(new Date(1611514800000l));
        event9.setDescription("Odmor za dušu bar na jedan dan.");
        event9.setCreator(hykeUserRepository.findByUsername("ihorvat"));
        event9.setCreatedAt(new Date());
        eventRepository.save(event9);



    }

    @Value("${badge.path}")
    private String badgePath;

    private void loadBadges() {
        Badge noviceBadge = new Badge();
        noviceBadge.setThreshold(BadgeThreshold.NOVICE);
        noviceBadge.setName("PRVI POSJET");
        noviceBadge.setDescription("Bedž koji se osvaja za prvi posjet planinarskom domu.");
        noviceBadge.setFileSystemLocation("novice.png"); //TODO - fix in production
        badgeRepository.save(noviceBadge);

        Badge beginnerBadge = new Badge();
        beginnerBadge.setThreshold(BadgeThreshold.BEGINNER);
        beginnerBadge.setName("PET");
        beginnerBadge.setDescription("Bedž koji se osvaja za ukupno pet posjeta planinarskim domovima.");
        beginnerBadge.setFileSystemLocation("beginner.png"); //TODO - fix in production
        badgeRepository.save(beginnerBadge);

        Badge intermediateBadge = new Badge();
        intermediateBadge.setThreshold(BadgeThreshold.INTERMEDIATE);
        intermediateBadge.setName("DESET");
        intermediateBadge.setDescription("Bedž koji se osvaja za ukupno deset posjeta planinarskim domovima.");
        intermediateBadge.setFileSystemLocation("intermediate.png"); //TODO - fix in production
        badgeRepository.save(intermediateBadge);

        Badge expiriencedBadge = new Badge();
        expiriencedBadge.setThreshold(BadgeThreshold.EXPIRIENCED);
        expiriencedBadge.setName("STO");
        expiriencedBadge.setDescription("Bedž koji se osvaja za ukupno sto posjeta planinarskim domovima.");
        expiriencedBadge.setFileSystemLocation("expirienced.png"); //TODO - fix in production
        badgeRepository.save(expiriencedBadge);
    }

}
