package com.exodia.hyke.service.impl;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.exodia.hyke.dao.HykeUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.exodia.hyke.dao.EventRepository;
import com.exodia.hyke.dao.HikeRepository;
import com.exodia.hyke.dao.InviteRepository;
import com.exodia.hyke.dto.EventInviteDTO;
import com.exodia.hyke.dto.form.EventFormDTO;
import com.exodia.hyke.exception.EventDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.Event;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Invite;
import com.exodia.hyke.model.Status;
import com.exodia.hyke.service.EventService;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	EventRepository eventRepository;

	@Autowired
	InviteRepository inviteRepository;

	@Autowired
	HykeUserRepository userRepo;

	@Autowired
	HikeRepository hikeRepo;

	@Override
	public Event findById(Long id) {
		Optional<Event> event = eventRepository.findById(id);
		if (event.isPresent()) {
			return event.get();
		} else {
			throw new EventDoesNotExistException("Event with specified id does not exist.");
		}
	}

	@Override
	public List<Event> findByCreatorId(Long creatorId) {
		Optional<HykeUser> hykeUser = userRepo.findById(creatorId);
		if (hykeUser.isPresent()) {
			return eventRepository.findByCreator(hykeUser.get());
		} else {
			throw new UserDoesNotExistException("User with specified id does not exist.");
		}
	}

	@Override
	public void createEvent(EventFormDTO dto) {
		Event event = new Event();

		event.setCreator(userRepo.findById(dto.getCreator().getId()).get());
		event.setStartDateTime(dto.getStartDateTime());
		event.setEndDateTime(dto.getEndDateTime());
		event.setDescription(dto.getDescription());
		event.setHike(hikeRepo.findById(dto.getHike().getId()).get());
		event.setCreatedAt(new Date());

		validate(event);
		eventRepository.save(event);
	}

	private void validate(Event event) {
		Assert.notNull(event.getStartDateTime(), "A starting date and time must be specified.");
		Assert.notNull(event.getEndDateTime(), "An ending date and time must be specified.");
		Assert.notNull(event.getDescription(), "An event must have a description.");
		Assert.notNull(event.getHike(), "An event must have a specified hike.");
	}

	@Override
	public List<Invite> findEventInvites(Long eventId) {
		Optional<Event> event = eventRepository.findById(eventId);
		if (event.isPresent()) {
			return inviteRepository.findByEventId(eventId);
		} else {
			throw new EventDoesNotExistException("Event with specified id does not exist.");
		}
	}

	@Override
	public String sendInvite(EventInviteDTO dto, Long eventId) {
		Optional<Event> event = eventRepository.findById(eventId);
		if (event.isPresent()) {
			Invite invite = new Invite();
			invite.setInvitedUser(userRepo.findById(dto.getUser().getId()).get());
			invite.setStatus(Status.PENDING);
			invite.setEvent(event.get());
			inviteRepository.save(invite);
			event.get().getInvites().add(invite);
			eventRepository.save(event.get());
			return "Invite sent to user " + dto.getUser().getFirstName() + " " + dto.getUser().getFirstName();
		} else {
			throw new EventDoesNotExistException("Event with specified id does not exist.");
		}
	}

	@Override
	public void replyToInvite(Long eventId, EventInviteDTO eiDTO) {
		Event e;
		try {
			e = eventRepository.findById(eventId).orElseThrow();
		} catch (NoSuchElementException ex) {
			throw new NoSuchElementException("This event does not exist.");
		}

		boolean exists = false;
		for (Invite i : e.getInvites()) {
			if (i.getInvitedUser().getId() == eiDTO.getUser().getId()) {
				exists = true;
				break;
			}
		}
		if (!exists)
			throw new NoSuchElementException("You are not invited to this event.");

		if (eiDTO.getStatus() == Status.ACCEPTED) {
			for (Invite i : e.getInvites()) {
				if (i.getInvitedUser().getId() == eiDTO.getUser().getId()) {
					i.setStatus(Status.ACCEPTED);
					inviteRepository.save(i);
					eventRepository.saveAndFlush(e);
					break;
				}
			}

		} else if (eiDTO.getStatus() == Status.DECLINED) {
			for (Invite i : e.getInvites()) {
				if (i.getInvitedUser().getId() == eiDTO.getUser().getId()) {
					i.setStatus(Status.DECLINED);
					inviteRepository.save(i);
					eventRepository.saveAndFlush(e);
					break;
				}
			}
		}
	}
}
