package com.exodia.hyke.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class UserReport {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private HykeUser reporter;

    @ManyToOne
    private Hike hike;

    @Column(nullable = false)
    private String description;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HykeUser getReporter() {
        return reporter;
    }

    public void setReporter(HykeUser reporter) {
        this.reporter = reporter;
    }

    public Hike getHike() {
        return hike;
    }

    public void setHike(Hike hike) {
        this.hike = hike;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserReport)) return false;
        UserReport that = (UserReport) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(reporter, that.reporter) &&
                Objects.equals(hike, that.hike) &&
                Objects.equals(description, that.description) &&
                Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reporter, hike, description, createdAt);
    }
}
