package com.exodia.hyke.service;

import com.exodia.hyke.model.Badge;
import com.exodia.hyke.model.HykeUser;

import java.util.List;

public interface BadgeService {

    void generateBadges(HykeUser hykeUser);

}
