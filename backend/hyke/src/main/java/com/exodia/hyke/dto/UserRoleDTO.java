package com.exodia.hyke.dto;

import com.exodia.hyke.model.Role;

public class UserRoleDTO {

    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
