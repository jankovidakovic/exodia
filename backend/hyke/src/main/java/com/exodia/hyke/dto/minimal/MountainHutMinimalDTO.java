package com.exodia.hyke.dto.minimal;

import com.exodia.hyke.model.MountainHut;

/**
 * GET /mountain-huts, dobiva se polje ovoga
 */
public class MountainHutMinimalDTO {

    private Long id;
    private LocationMinimalDTO location;
    private String name;
    //todo - dodaj stvari po kojima se može filtrirati
        //prenociste, struja, voda, hrana

    public static MountainHutMinimalDTO getDTO(MountainHut mountainHut) {
        MountainHutMinimalDTO dto = new MountainHutMinimalDTO();
        dto.setId(mountainHut.getId());
        dto.setLocation(LocationMinimalDTO.getDTO(mountainHut.getLocation()));
        dto.setName(mountainHut.getName());
        return dto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocationMinimalDTO getLocation() {
        return location;
    }

    public void setLocation(LocationMinimalDTO location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
