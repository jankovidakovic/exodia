package com.exodia.hyke.rest;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.exodia.hyke.dao.HykeUserRepository;
import com.exodia.hyke.model.HykeUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.NoSuchElementException;

import static com.exodia.hyke.rest.SecurityConstants.*;
import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    private HykeUserRepository repository;


    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, HykeUserRepository repository) {
        this.authenticationManager = authenticationManager;
        this.repository = repository;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            HykeUser creds = new ObjectMapper()
                    .readValue(req.getInputStream(), HykeUser.class);

            HykeUser u = repository
                    .findByUsername(creds
                            .getUsername());
            if(u == null) throw new NoSuchElementException("That username is not present.");

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            commaSeparatedStringToAuthorityList(
                                    repository
                                            .findByUsername(creds
                                                    .getUsername())
                                            .toString()
                            )

                    )
            );
        } catch (Exception e) {
            if(e instanceof IOException)
            throw new RuntimeException(e);
            else if(e instanceof NoSuchElementException) //throw new NoSuchElementException("User not existent.");
                res.setStatus(404);
                else if(e instanceof  AuthenticationException) //throw new NoSuchElementException("Failed to login.");
                res.setStatus(401);
        }

        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        HykeUser authUser = repository.findByUsername(((HykeUserDetails) auth.getPrincipal()).getUsername());
        String token = JWT.create()
                .withSubject(((HykeUserDetails) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withClaim("rol", auth.getAuthorities().toString())
                .withClaim("id",
                        repository.findByUsername(
                                ((HykeUserDetails)auth.getPrincipal()).getUsername()).getId()
                )
                .withClaim("username", authUser.getUsername())
            .withClaim("firstName", authUser.getFirstName())
            .withClaim("lastName", authUser.getLastName())
            .withClaim("email", authUser.getEmail())
            .withClaim("role", authUser.getRole().toString()) //TODO - sus
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }

}