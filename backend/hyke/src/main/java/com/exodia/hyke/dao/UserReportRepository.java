package com.exodia.hyke.dao;

import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.UserReport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages persistence of Events.
 * @author a_nakic
 */
public interface UserReportRepository extends JpaRepository <UserReport, Long>
{
    /**
     * Delete entries which have a matching Hike.
     * @param hike
     * @return number of deleted entries
     */
    public void deleteByHike (Hike hike);
}
