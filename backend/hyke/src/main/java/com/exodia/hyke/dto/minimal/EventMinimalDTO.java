package com.exodia.hyke.dto.minimal;

import com.exodia.hyke.model.Event;

import java.util.Date;

import com.exodia.hyke.model.Event;

/**
 * GET /events, dobiva se polje ovoga
 */
public class EventMinimalDTO {

    private Long id;
    private UserMinimalDTO creator;
    private HikeMinimalDTO hike;
    private Date startDateTime;

    public static EventMinimalDTO getDTO (Event event)
    {
        EventMinimalDTO eventMinimalDTO = new EventMinimalDTO ();

        eventMinimalDTO.setId (event.getId ());
        eventMinimalDTO.setCreator (UserMinimalDTO.getDTO (event.getCreator ()));
        eventMinimalDTO.setHike (HikeMinimalDTO.getDTO (event.getHike ()));
        eventMinimalDTO.setStartDateTime (event.getStartDateTime ());

        return eventMinimalDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserMinimalDTO getCreator() {
        return creator;
    }

    public void setCreator(UserMinimalDTO creator) {
        this.creator = creator;
    }

    public HikeMinimalDTO getHike() {
        return hike;
    }

    public void setHike(HikeMinimalDTO hike) {
        this.hike = hike;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }
}
