package com.exodia.hyke.dao;

import com.exodia.hyke.model.AcquiredBadge;
import com.exodia.hyke.model.HykeUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages persistence of AcquiredBadges.
 * @author a_nakic
 */
public interface AcquiredBadgeRepository extends JpaRepository <AcquiredBadge, Long>
{
    /**
     * Finds all AcquiredBadges created by specified user.
     * @param hykeUser
     * @return List of AcquiredBadges
     */
    public List<AcquiredBadge> findByCreator (HykeUser hykeUser);
}
