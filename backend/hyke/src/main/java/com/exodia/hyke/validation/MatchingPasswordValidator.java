package com.exodia.hyke.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.exodia.hyke.dto.form.UserRegistrationDTO;

/**
 * Validates that the user has matched desired password in the password confirmation field
 * Applied to the entire UserRegistrationDTO object
 */

public class MatchingPasswordValidator implements ConstraintValidator<MatchingPassword, Object> {

	@Override
	public void initialize(MatchingPassword constraintAnnotation) {
	}
	
	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context) {
		UserRegistrationDTO user = (UserRegistrationDTO) obj;
		return user.getPassword().equals(user.getConfirmPassword());
	}
}
