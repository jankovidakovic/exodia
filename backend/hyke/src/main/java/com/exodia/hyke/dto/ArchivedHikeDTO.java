package com.exodia.hyke.dto;

import java.util.Date;

import com.exodia.hyke.dto.minimal.HikeMinimalDTO;
import com.exodia.hyke.model.ArchivedHike;

public class ArchivedHikeDTO {
	private HikeMinimalDTO hikeMinimalDTO;
	private String archiveDescription;
	private Date archivedAt;
	private Date doneAt;
	
	public HikeMinimalDTO getHikeMinimalDTO() {
		return hikeMinimalDTO;
	}
	public void setHikeMinimalDTO(HikeMinimalDTO hikeMinimalDTO) {
		this.hikeMinimalDTO = hikeMinimalDTO;
	}
	public String getArchiveDescription() {
		return archiveDescription;
	}
	public void setArchiveDescription(String archiveDescription) {
		this.archiveDescription = archiveDescription;
	}
	public Date getArchivedAt() {
		return archivedAt;
	}
	public void setArchivedAt(Date archivedAt) {
		this.archivedAt = archivedAt;
	}
	public Date getDoneAt() {
		return doneAt;
	}
	public void setDoneAt(Date doneAt) {
		this.doneAt = doneAt;
	}
	
	public static ArchivedHikeDTO getDTO(ArchivedHike hike) {
		ArchivedHikeDTO dto = new ArchivedHikeDTO();
		dto.setArchivedAt(hike.getArchivedAt());
		dto.setArchiveDescription(hike.getArchiveDescription());
		dto.setDoneAt(hike.getDoneAt());
		dto.setHikeMinimalDTO(HikeMinimalDTO.getDTO(hike.getHike()));
		return dto;
	}
}
