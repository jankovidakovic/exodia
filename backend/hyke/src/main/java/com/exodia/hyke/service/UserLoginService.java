package com.exodia.hyke.service;

import com.exodia.hyke.dto.form.UserLoginDTO;
import org.springframework.stereotype.Service;

/**
 * Manages logins.
 * @author a_nakic
 */

@Service
public interface UserLoginService
{
    /**
     * Checks if user represented with the given UserLoginDTO exists.
     * Checks if the the passwords match.
     * @param userLoginDTO
     * @return true if passwords match or else false
     * @throws UserDoesNotExistException if user does not exist
     */
    boolean userLogin (UserLoginDTO userLoginDTO);
}
