package com.exodia.hyke.dto;

/**
 * Data transfer object for managing locations.
 * Contains the name and description.
 * @author a_nakic
 */

public class LocationDTO
{
    private String name;

    private String description;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
