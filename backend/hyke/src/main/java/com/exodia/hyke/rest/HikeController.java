package com.exodia.hyke.rest;

import com.exodia.hyke.dto.ReportDTO;
import com.exodia.hyke.dto.minimal.HikeMinimalDTO;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.UserReport;
import com.exodia.hyke.dto.RatingDTO;
import com.exodia.hyke.dto.ReportDTO;
import com.exodia.hyke.dto.detailed.HikeDetailedDTO;
import com.exodia.hyke.dto.form.HikeFormDTO;
import com.exodia.hyke.dto.minimal.HikeMinimalDTO;
import com.exodia.hyke.exception.DuplicateRatingException;
import com.exodia.hyke.exception.HikeAlreadyExistsException;
import com.exodia.hyke.exception.HikeDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.service.HikeService;
import com.sun.net.httpserver.HttpsConfigurator;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Controller which takes care of the API endpoint for REST operations regarding hikes.
 * @author jankovidakovic a_nakic
 */
@RestController
@RequestMapping("/hikes")
public class HikeController{

   @Autowired
   private HikeService hikeService; //dependency injection by Spring Boot

   /**
    * Fetches hikes which either have a matching publicity or matching creator.
    * @param creatorId user id
    * @param publicity
    * @return list of hikes that satisfy the either condition
    */
   @GetMapping("")
   public ResponseEntity <List <HikeMinimalDTO>> listHikes (@RequestParam(required = false) Boolean publicity, @RequestParam(required = false) Long creatorId)
   {
      if (publicity != null) { //TODO - refactor so that it fetches all hikes
         try {
            return new ResponseEntity <List<HikeMinimalDTO>>(hikeService
                .findByPublicity (publicity)
                .stream ()
                .map (HikeMinimalDTO::getDTO)
                .collect (Collectors.toList()), HttpStatus.OK);
         } catch (NullPointerException e) {
            int i = 0;
            i += 2;
         }
      }

      if (creatorId != null) {
         try {
            return new ResponseEntity <List<HikeMinimalDTO>>(hikeService
                    .findByCreatorId (creatorId)
                    .stream ()
                    .map (HikeMinimalDTO::getDTO)
                    .collect (Collectors.toList()), HttpStatus.OK);

         } catch (UserDoesNotExistException e) {
            return new ResponseEntity <List <HikeMinimalDTO>> (Collections.emptyList (), HttpStatus.NOT_FOUND);
         }
      }

      return new ResponseEntity<List<HikeMinimalDTO>>(
          hikeService.findAll()
              .stream()
              .map(HikeMinimalDTO::getDTO)
              .collect(Collectors.toList()),
          HttpStatus.OK);
      //return new ResponseEntity <List<HikeMinimalDTO>> ((List <HikeMinimalDTO>) null, HttpStatus.BAD_REQUEST);
   }

   /**
    * Fetches hike that has a matching id.
    * @param hikeId
    * @return response entity with the hike
    */
   @GetMapping("/{hikeId}")
   //@Secured({"ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER"})
   public ResponseEntity <HikeDetailedDTO> getHikeById (@PathVariable Long hikeId)
   {
      try {
         return new ResponseEntity <HikeDetailedDTO> (HikeDetailedDTO.getDTO (hikeService.findById (hikeId)), HttpStatus.OK);

      } catch (UserDoesNotExistException e) {
         return new ResponseEntity <HikeDetailedDTO> ((HikeDetailedDTO) null, HttpStatus.NOT_FOUND);
      }
   }

   /**
    * Creates a new hike.
    * @param hikeFormDTO dto on which the new hike is based on
    * @return informational response entity
    */
   @PostMapping("")
   @Secured({"ROLE_ADMIN", "ROLE_DUTY", "ROLE_USER"})
   public ResponseEntity <String> createNewHike (@RequestBody HikeFormDTO hikeFormDTO)
   {
      try {
         hikeService.createHike (hikeFormDTO);

         return new ResponseEntity <String> ("Success.", HttpStatus.CREATED);

      } catch (HikeAlreadyExistsException e) {
         return new ResponseEntity <String> ("Hike already exists.", HttpStatus.CONFLICT);
      }
   }



   /**
    * Fetches reports for the specified hike.
    * @return list of reports, represented by their corresponding DTOs
    * @author daeyarn
    */

   @Secured("ROLE_ADMIN")
   @GetMapping("/{hikeId}/reports")
   public ResponseEntity<List<ReportDTO>> getHikeReports(
      @PathVariable("hikeId") Long id
   ){
      try {
         return new ResponseEntity<>(hikeService.getReports(id), HttpStatus.OK);
      }catch(NoSuchElementException e){
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
   }

   @DeleteMapping("/{hikeId}/reports/{reportId}")
   @Secured("ROLE_ADMIN")
   public ResponseEntity<String> deleteReport(
      @PathVariable("hikeId") Long hikeId,
      @PathVariable("reportId") Long reportId
   ){
      try {
         hikeService.deleteReport(hikeId, reportId);
         return new ResponseEntity<>("Report successfully deleted.", HttpStatus.OK);
      }catch(NoSuchElementException e){
         return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
      }

   }

   @GetMapping("/reported")
   @Secured("ROLE_ADMIN")
   public ResponseEntity<List<HikeMinimalDTO>> getReportedHikes(){
      List<UserReport> reportedHikes = hikeService.getReportedHikes();
      List<HikeMinimalDTO> hikeMinimalDTOS = new ArrayList<>();
      for(UserReport ur: reportedHikes){
         hikeMinimalDTOS.add(HikeMinimalDTO
                              .getDTO(
                              ur.getHike()
         ));
      }
      return new ResponseEntity<>(hikeMinimalDTOS, HttpStatus.OK);

   }
   /**
    * Deletes a hike that matches the given id.
    * @param hikeId
    * @return informational response entity
    */
   @DeleteMapping("/{hikeId}")
   @Secured({"ROLE_ADMIN", "ROLE_USER"})
   public ResponseEntity <String> deleteHike (@PathVariable Long hikeId)
   {
      try {
         hikeService.deleteHike (hikeId);

         return new ResponseEntity <String> ("Success.", HttpStatus.OK);

      } catch (HikeDoesNotExistException e) {
         return new ResponseEntity <String> ("Hike does not exist.", HttpStatus.NOT_FOUND);
      }
   }

   /**
    * Creates a rating.
    * @param hikeId
    * @param ratingDTO
    * @return informational response entity
    */
   @PostMapping("/{hikeId}/ratings")
   @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
   public ResponseEntity <String> rateHike (@PathVariable Long hikeId, @RequestBody RatingDTO ratingDTO)
   {
      try {
         hikeService.rate (hikeId, ratingDTO.getUser ().getId (), ratingDTO.getRating ());

         return new ResponseEntity <String> ("Success.", HttpStatus.OK);

      } catch (UserDoesNotExistException e) {
         return new ResponseEntity <String> ("User does not exist.", HttpStatus.NOT_FOUND);

      } catch (HikeDoesNotExistException e) {
         return new ResponseEntity<String>("Hike does not exist.", HttpStatus.NOT_FOUND);

      } catch (DuplicateRatingException e) {
         return new ResponseEntity <String> ("Already rated.", HttpStatus.CONFLICT);
      }
   }

   /**
    * Creates a report.
    * @param hikeId
    * @param reportDTO
    * @return informational response entity
    */
   @PostMapping("/{hikeId}/reports")
   @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
   public ResponseEntity <ReportDTO> reportHike (@PathVariable Long hikeId, @RequestBody ReportDTO reportDTO)
   {
         hikeService.report (hikeId, reportDTO.getUser ().getId (), reportDTO.getReport ());

         return new ResponseEntity <ReportDTO> (reportDTO, HttpStatus.CREATED);

         /*
      } catch (UserDoesNotExistException e) {
         return new ResponseEntity <String> ("User does not exist.", HttpStatus.NOT_FOUND);

      } catch (HikeDoesNotExistException e) {
         return new ResponseEntity<String>("Hike does not exist.", HttpStatus.NOT_FOUND);


          */
   }

   @GetMapping("/{hikeId}/average-rating")
   public ResponseEntity<Double> getAverageRating(@PathVariable Long hikeId) {
      return new ResponseEntity<>(hikeService.getAverageRating(hikeId), HttpStatus.OK);
   }

}
