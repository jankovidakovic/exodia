package com.exodia.hyke.dto;

/**
 * POST /friend-requests, ovo se predaje u bodyju
 *
 * odgovor na zahtjev: PUT /friend-requests/{friend-request-id} , u bodyju odgovor (oblik - TODO)
 */
public class FriendRequestDTO {
    private Long sender;
    private Long receiver;

    //TODO - zahtjev kad se geta

    public Long getSender() {
        return sender;
    }

    public void setSender(Long sender) {
        this.sender = sender;
    }

    public Long getReceiver() {
        return receiver;
    }

    public void setReceiver(Long receiver) {
        this.receiver = receiver;
    }
}
