package com.exodia.hyke.model;

public enum BadgeThreshold {
    NOVICE,
    BEGINNER,
    INTERMEDIATE,
    EXPIRIENCED,
}
