package com.exodia.hyke.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

@Entity
@Table(name="event_invite")
public class Invite {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private HykeUser invitedUser;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "event_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Event event;
    
    @Column(nullable=false)
    private Status status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invite invite = (Invite) o;
        return getInvitedUser().equals(invite.getInvitedUser()) && getEvent().equals(invite.getEvent()) && getStatus() == invite.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInvitedUser(), getEvent(), getStatus());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HykeUser getInvitedUser() {
        return invitedUser;
    }

    public void setInvitedUser(HykeUser invitedUser) {
        this.invitedUser = invitedUser;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
    
}
