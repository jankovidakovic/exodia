package com.exodia.hyke.exception;

/**
 * Exception that is thrown when more than one rating from the same user and hike exist in the database.
 * @author a_nakic
 */

public class DuplicateRatingException extends RuntimeException
{
    public DuplicateRatingException (String errorMessage)
    {
        super (errorMessage);
    }
}
