package com.exodia.hyke.dao;

import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages persistence of Ratings.
 * @author a_nakic
 */
public interface RatingRepository extends JpaRepository <Rating, Long>
{
//    /**
//     * Checks if entries with specified hash exist.
//     * @param hash
//     * @return true if condition satisfied, else false
//     */
//    public Boolean existsByHash (Integer hash);

    /**
     * Delete entries which have a matching Hike.
     * @param hike
     * @return number of deleted entries
     */
    public Long deleteByHike (Hike hike);

    List<Rating> findByHikeIs(Hike hike);

}
