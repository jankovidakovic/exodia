package com.exodia.hyke.dto.minimal;

import com.exodia.hyke.model.Location;

/**
 * GET /locations, dobiva se polje ovoga
 */
public class LocationMinimalDTO {

    private Long id;
    private String name;

    public static LocationMinimalDTO getDTO(Location location) {
        LocationMinimalDTO dto = new LocationMinimalDTO();
        dto.setId(location.getId());
        dto.setName(location.getName());
        return dto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
