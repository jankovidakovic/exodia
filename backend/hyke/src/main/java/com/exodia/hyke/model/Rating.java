package com.exodia.hyke.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Rating
{
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private HykeUser user;

    @ManyToOne
    private Hike hike;

    @Column(nullable = false)
    private Integer rating;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return getUser().equals(rating.getUser()) && getHike().equals(rating.getHike());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getHike());
    }

    public Long getId ()
    {
        return this.id;
    }

    public HykeUser getUser ()
    {
        return this.user;
    }

    public Hike getHike ()
    {
        return this.hike;
    }

    public Integer getRating () { return this.rating; }

    public void setId (Long id)
    {
        this.id = id;
    }

    public void setUser (HykeUser user)
    {
        this.user = user;
    }

    public void setHike (Hike hike)
    {
        this.hike = hike;
    }

    public void setRating (Integer rating)
    {
        this.rating = rating;
    }
}
