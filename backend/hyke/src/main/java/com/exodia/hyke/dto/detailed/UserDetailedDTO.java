package com.exodia.hyke.dto.detailed;

import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Role;

/**
 * GET /users/{userId}
 */
public class UserDetailedDTO {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private boolean befriended;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isBefriended() {
        return befriended;
    }

    public void setBefriended(boolean befriended) {
        this.befriended = befriended;
    }

    public static UserDetailedDTO getDTO(HykeUser user){
        UserDetailedDTO userDetailedDTO = new UserDetailedDTO();
        userDetailedDTO.setId(user.getId());
        userDetailedDTO.setFirstName(user.getFirstName());
        userDetailedDTO.setLastName(user.getLastName());
        userDetailedDTO.setUsername(user.getUsername());
        userDetailedDTO.setEmail(user.getEmail());
        userDetailedDTO.setRole(user.getRole());
        userDetailedDTO.setBefriended(false);

        return userDetailedDTO;

    }
}
