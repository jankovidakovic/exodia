package com.exodia.hyke.service.impl;

import com.exodia.hyke.dao.HikeRepository;
import com.exodia.hyke.dto.ReportDTO;
import com.exodia.hyke.model.Hike;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.UserReport;
import com.exodia.hyke.dao.*;
import com.exodia.hyke.dto.form.HikeFormDTO;
import com.exodia.hyke.exception.DuplicateRatingException;
import com.exodia.hyke.exception.HikeAlreadyExistsException;
import com.exodia.hyke.exception.HikeDoesNotExistException;
import com.exodia.hyke.exception.UserDoesNotExistException;
import com.exodia.hyke.model.*;
import com.exodia.hyke.service.HikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class HikeServiceImpl implements HikeService {

    @Autowired
    private HikeRepository hikeRepository;

    @Autowired
    UserReportRepository userReportRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    ArchivedHikeRepository archivedHikeRepository;

    @Autowired
    HykeUserRepository userRepo;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    MountainHutRepository mountainHutRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Override
    public List <Hike> findByPublicity (Boolean publicity)
    {
        return hikeRepository.findByPublicity (publicity);
    }

    @Override
    public List<Hike> findAll() {
        return hikeRepository.findAll();
    }

    @Override
    public Hike findById(Long id)
    {
        Optional <Hike> hike = hikeRepository.findById (id);

        if (hike.isPresent ()) {

            return hike.get ();
        } else {

            throw new HikeDoesNotExistException ("Hike with specified id does not exist.");
        }
    }

    @Override
    public List <Hike> findByCreatorId (Long id)
    {
        Optional <HykeUser> hykeUser = userRepo.findById (id);

        if (hykeUser.isPresent ()) {

            return hikeRepository.findByCreator (hykeUser.get ());
        } else {

            throw new UserDoesNotExistException ("User with specified id does not exist.");
        }
    }

    @Override
    public void createHike(HikeFormDTO hikeFormDTO)
    {
        Hike hike = new Hike ();

        hike.setCreator (userRepo.findById (hikeFormDTO.getCreator ()).get ());
        hike.setCreatedAt(new Date());
        hike.setName (hikeFormDTO.getName ());
        hike.setDescription (hikeFormDTO.getDescription ());
        hike.setLocation (locationRepository.findById (hikeFormDTO.getLocation ()).get ());
        hike.setHikeStart (hikeFormDTO.getHikeStart ());
        hike.setHikeEnd (hikeFormDTO.getHikeEnd ());
        hike.setHikeLength (hikeFormDTO.getHikeLength ());
        hike.setIntensity (hikeFormDTO.getIntensity ());
        hike.setPublicity (hikeFormDTO.getPublicity ());
        hike.setMountainHuts (hikeFormDTO.getMountainHuts ()
                .stream ()
                .map (x -> mountainHutRepository.findById (x).get ())
                .collect (Collectors.toSet ()));

        validate (hike);

        /*
        if (hikeRepository.existsByHash (hike.getHash ())) {
            throw new HikeAlreadyExistsException ("Double hike entry.");
        }

         */

        hikeRepository.save(hike);
    }

    private void validate(Hike hike)
    {
        Assert.notNull(hike, "Hike object must be given.");
        Assert.notNull(hike.getCreator(), "Hike must have a user creator.");
        Assert.notNull(hike.getDescription(), "Hike must have a description.");
        Assert.notNull(hike.getHikeEnd(), "Hike must have a defined ending destination.");
        Assert.notNull(hike.getHikeStart(), "Hike must have a defined starting position.");
        Assert.notNull(hike.getIntensity(), "Hike must have a defined intensity.");
        Assert.notNull(hike.getHikeLength(), "Hike must have a defined length.");
        Assert.notNull(hike.getLocation(), "Hike must have a location.");
        Assert.notNull(hike.getName(), "Hike must have a name.");
    }

    public List<ReportDTO> getReports(Long hikeId){

        Hike hike = hikeRepository
                .findById(hikeId)
                .orElseThrow(); //throws NoSuchElementException

        List<ReportDTO> reports = ReportDTO.getReportDTOs(
                        hikeRepository
                        .getReports(hike)
        );

        return reports;
    }

    @Override
    public void deleteReport(Long hikeId, Long reportId) {
        try {
            Hike hike = hikeRepository
                    .findById(hikeId)
                    .orElseThrow(); //throws NoSuchElementException
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("This hike does not exist.");
        }

        try {
            UserReport userReport = hikeRepository
                    .findReportById(reportId)
                    .orElseThrow(); //throws NoSuchElementException
        }catch(NoSuchElementException e){
            throw new NoSuchElementException("This report does not exist.");
        }

        hikeRepository.deleteReport(reportId);
    }

    @Override
    public List<UserReport> getReportedHikes() {
        return hikeRepository.getUserReports();
	}

    @Override
    public void deleteHike (Long id)
    {
        Optional <Hike> hike = hikeRepository.findById (id);

        if (hike.isPresent ()) {

            userReportRepository.deleteByHike (hike.get ());
            eventRepository.deleteByHike (hike.get ());
            archivedHikeRepository.deleteByHike (hike.get ());
            ratingRepository.deleteByHike (hike.get ());

            hikeRepository.deleteById (id);

        } else {

            throw new HikeDoesNotExistException ("Hike with specified id does not exist.");
        }
    }

    @Override
    public void rate (Long hikeId, Long userId, Integer value)
    {
        Rating rating = new Rating ();

        Optional <HykeUser> hykeUser = userRepo.findById (userId);

        if (hykeUser.isPresent ()) {

            rating.setUser (hykeUser.get ());
        } else {

            throw new UserDoesNotExistException ("User with specified id does not exist.");
        }

        Optional <Hike> hike = hikeRepository.findById(hikeId);

        if (hike.isPresent ()) {

            rating.setHike (hike.get ());
        } else {

            throw new HikeDoesNotExistException ("Hike with specified id does not exist.");
        }

        rating.setRating (value);
        //rating.setHash ();

        /*
        if (ratingRepository.existsByHash (rating.getHash ())) {
            throw new DuplicateRatingException ("Can not rate twice.");
        }

         */

        ratingRepository.save (rating);
    }

    @Override
    public ReportDTO report (Long hikeId, Long userId, String report)
    {
        UserReport userReport = new UserReport ();

        Optional <HykeUser> hykeUser = userRepo.findById (userId);

        if (hykeUser.isPresent ()) {
            userReport.setReporter (hykeUser.get ());

        } else {
            throw new UserDoesNotExistException ("User with specified id does not exist.");
        }

        Optional <Hike> hike = hikeRepository.findById(hikeId);

        if (hike.isPresent ()) {
            userReport.setHike (hike.get ());

        } else {
            throw new HikeDoesNotExistException ("Hike with specified id does not exist.");
        }

        userReport.setDescription (report);

        userReportRepository.save (userReport);

        return ReportDTO.getDTO(userReport);
    }

    @Override
    public double getAverageRating(Long hikeId) {
        Hike hike = hikeRepository.findById(hikeId).orElseThrow();
        return ratingRepository.findByHikeIs(hike).stream().mapToInt(Rating::getRating).average().getAsDouble();
    }
}
