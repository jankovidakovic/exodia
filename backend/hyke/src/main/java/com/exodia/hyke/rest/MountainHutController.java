package com.exodia.hyke.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import com.exodia.hyke.dto.IdDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import com.exodia.hyke.dto.ConfirmVisitDTO;
import com.exodia.hyke.dto.MountainHutDTO;
import com.exodia.hyke.dto.detailed.MountainHutDetailedDTO;
import com.exodia.hyke.dto.minimal.MountainHutMinimalDTO;
import com.exodia.hyke.exception.MountainHutAlreadyExistException;
import com.exodia.hyke.exception.MountainHutDoesNotExistException;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Status;
import com.exodia.hyke.service.MountainHutService;

/**
 * Controller which takes care of the API endpoint for REST operations regarding mountain huts.
 * @author daeyarn
 */

@RestController
@RequestMapping("/mountain-huts")
public class MountainHutController {

    @Autowired
    MountainHutService mountainHutService;


    /**
     * Fetches users who have requested a visit confirmation
     * at the specified hut.
     * @return list of requests in the form of ConfirmVisitDTO
     * @author daeyarn
     */

    @GetMapping("/{mountainHutId}/visits")
    @Secured({"ROLE_ADMIN", "ROLE_DUTY"})
    public ResponseEntity<List<ConfirmVisitDTO>> getVisitRequests(
            @PathVariable("mountainHutId") @NotNull Long id
    ){
        try {
            List<HykeUser> requesters = mountainHutService.getVisitRequestors(id);
            List<ConfirmVisitDTO> confirmVisitDTOS = new ArrayList<>();
            for (HykeUser user : requesters) {
                confirmVisitDTOS.add(ConfirmVisitDTO.getDTO(user));
            }

            return new ResponseEntity(confirmVisitDTOS, HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{mountainHutId}/request-visit")
    @Secured({"ROLE_USER", "ROLE_DUTY", "ROLE_ADMIN"})
    public ResponseEntity<String> requestVisit(@PathVariable("mountainHutId") @NotNull Long mountainHutId,
                                               @RequestBody IdDTO idDTO) {
        long id = idDTO.getId();
        mountainHutService.addRequestor(mountainHutId, id);
        return new ResponseEntity<String> ("Zahtjev uspješno poslan", HttpStatus.CREATED);
    }


    /**
     * Processes the visit request at the specified hut
     * for the user specified in the request's body.
     * @return void
     * @author daeyarn
     */

    @PostMapping("/{mountainHutId}/visits")
    @Secured({"ROLE_ADMIN", "ROLE_DUTY"})
    public ResponseEntity<String> replyToVisitRequest(
        @PathVariable("mountainHutId") @NotNull Long id,
        @RequestBody ConfirmVisitDTO confirmVisitDTO
    ){

        try{
            if (confirmVisitDTO.getStatus() == Status.ACCEPTED)
                mountainHutService.approveVisitRequest(id, confirmVisitDTO.getUser().getId());
            else if (confirmVisitDTO.getStatus() == Status.DECLINED)
                mountainHutService.removeRequestor(id, confirmVisitDTO.getUser().getId());
            return new ResponseEntity<>("Successfuly replied to the request.", HttpStatus.OK);
        }catch(NoSuchElementException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
	
	@GetMapping("")
	public ResponseEntity <List <MountainHutDetailedDTO>> listMountainHuts (@RequestParam(required = false) Long locationId)
    {
	    if (locationId != null) {
	        try {
	            return new ResponseEntity <List<MountainHutDetailedDTO>> (mountainHutService.fetchMountainHutsByLocation (locationId), HttpStatus.OK);
            } catch (MountainHutDoesNotExistException e) {
                return new ResponseEntity <List<MountainHutDetailedDTO>> (Collections.EMPTY_LIST, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity <List <MountainHutDetailedDTO>> (mountainHutService
                    .listAll()
                    .stream()
                    .map(mountainHut -> MountainHutDetailedDTO.getDTO(mountainHut))
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
    }
	
	@GetMapping("/{id}")
    //@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_DUTY"})
	public ResponseEntity<MountainHutDetailedDTO> viewMountainHutDetails(@PathVariable(value="id") String id) {
		MountainHutDetailedDTO dto = MountainHutDetailedDTO.getDTO(mountainHutService.fetch(Long.parseLong(id)));
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@PostMapping("")
    @Secured("ROLE_ADMIN")
	public ResponseEntity<String> createMountainHut(@RequestBody MountainHutDTO mountainHutDTO) {
		try {
			mountainHutService.createMountainHut(mountainHutDTO);
			return new ResponseEntity <String> ("Success", HttpStatus.OK); 
		} catch (MountainHutAlreadyExistException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}
	}
	
	@PutMapping("")
    @Secured("ROLE_ADMIN")
    public ResponseEntity <String> updateMountainHut (@RequestBody MountainHutDetailedDTO mountainHutDTO) {
        try {
            mountainHutService.updateMountainHut(mountainHutDTO);
            return new ResponseEntity <String> ("Success", HttpStatus.OK);
        } catch (MountainHutDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
	
	@DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity <String> deleteMountainHut (@RequestBody String name) {
        try {
            mountainHutService.deleteMountainHut(name);
            return new ResponseEntity <String> ("Success", HttpStatus.OK);
        } catch (MountainHutDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

}
