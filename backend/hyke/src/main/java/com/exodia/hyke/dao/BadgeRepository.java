package com.exodia.hyke.dao;

import com.exodia.hyke.model.Badge;
import com.exodia.hyke.model.BadgeThreshold;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadgeRepository extends JpaRepository<Badge, Long> {
    Badge findByThreshold(BadgeThreshold badgeThreshold);
}
