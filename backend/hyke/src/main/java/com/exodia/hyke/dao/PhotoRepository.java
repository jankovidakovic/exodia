package com.exodia.hyke.dao;

import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages persistence of Photos.
 * @author a_nakic
 */
public interface PhotoRepository extends JpaRepository <Photo, Long>
{
    /**
     * Finds all Photos created by specified user.
     * @param hykeUser
     * @return List of Photos
     */
    public List <Photo> findByCreator (HykeUser hykeUser);
}
