package com.exodia.hyke.service;

import java.util.List;
import java.util.Optional;

import com.exodia.hyke.dto.MountainHutDTO;
import com.exodia.hyke.dto.detailed.MountainHutDetailedDTO;
import com.exodia.hyke.dto.minimal.MountainHutMinimalDTO;
import com.exodia.hyke.model.HykeUser;
import com.exodia.hyke.model.MountainHut;

public interface MountainHutService {
	List <MountainHutDetailedDTO> fetchMountainHutsByLocation (Long locationId);
	List<MountainHut> listAll();
	Optional<MountainHut> findById(Long id);
	MountainHut fetch(Long id);	
	MountainHut createMountainHut(MountainHutDTO mountainHutDTO);
	void updateMountainHut(MountainHutDetailedDTO dto);
	void deleteMountainHut(String name);
    List<HykeUser> getVisitRequestors(Long hutId);
    void removeRequestor(Long hutId, Long hykeUserId);
    void addRequestor(Long hutId, Long hykeUserId);
    void approveVisitRequest(Long hutId, Long hykeUserId);

}
