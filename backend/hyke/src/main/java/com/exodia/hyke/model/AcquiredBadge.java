package com.exodia.hyke.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class AcquiredBadge extends Post { //TODO - hashCode, equals

    @ManyToOne //single badge can be acquired by multiple users
    private Badge badge;

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AcquiredBadge)) return false;
        AcquiredBadge that = (AcquiredBadge) o;
        return Objects.equals(badge, that.badge);
    }

    @Override
    public int hashCode() {
        return badge.hashCode();
    }
}
