curl --location --request POST 'localhost:8080/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username" : "user1",
    "firstName" : "firstName1",
    "lastName" : "lastName1",
    "email" : "email1@gmail.com",
    "password" : "password1",
    "confirmPassword" : "password1"
}'

curl --location --request POST 'localhost:8080/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username" : "user2",
    "firstName" : "firstName2",
    "lastName" : "lastName2",
    "email" : "email2@gmail.com",
    "password" : "password2",
    "confirmPassword" : "password2"
}'

curl --location --request POST 'localhost:8080/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username" : "user3",
    "firstName" : "firstName3",
    "lastName" : "lastName3",
    "email" : "email3@gmail.com",
    "password" : "password3",
    "confirmPassword" : "password3"
}'
